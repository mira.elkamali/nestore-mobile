import {Component, NgZone, ViewChild} from '@angular/core';
import {AlertController, NavController, Platform, ToastController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {IamClientProvider} from '../providers/iam-client/iam-client';

import {TabsPage} from '../pages/tabs/tabs';
import {LocalNotifications} from '@ionic-native/local-notifications';
import {GlobalvarProvider} from './globalprovider';
import {FCM} from '@ionic-native/fcm';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from '@ionic/storage';
import {MessageProvider} from '../chat/services/message-provider';
import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import localeNL from '@angular/common/locales/nl';
import localeIT from '@angular/common/locales/it';
import localeEN from '@angular/common/locales/en-GB';
import {APIService} from '../chat/services/api';
import {LoginPage} from '../pages/login/login';
import {AppVersion} from '@ionic-native/app-version';
import {AppMinimize} from "@ionic-native/app-minimize/ngx";
import {WOTService} from "../providers/WOTService";
import {LogsService} from "../providers/logsService";
import {Util} from "../util";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('nav') nav: NavController;

  rootPage: any;
  loaded = false;
  logged = false;
  hasConnection = false;
  game = null;
  tuto = false;
  langLoaded = false;
  questionnaireLoaded = false;
  versionPopUp = false;
  lastversion;

  constructor(public appVersion: AppVersion,
              public iamClient: IamClientProvider,
              private storage: Storage,
              private translate: TranslateService,
              private fcm: FCM,
              private globalvarProvider: GlobalvarProvider,
              public platform: Platform,
              private statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public localNotifications: LocalNotifications,
              public messageProvider: MessageProvider,
              public alertController: AlertController,
              public api: APIService,
              public ngZone: NgZone,
              public appMinimize: AppMinimize,
              public toast: ToastController,
              public wot : WOTService,
              public logs : LogsService
  ) {


    this.translate.onLangChange.subscribe(()=>{
        console.log('lang:ok');
      },
      ()=>{
        console.log('lang:nok')
      });



    // means that platform is ready
    this.storage.ready().then(async() => {
      try {
        console.log('versions')
        this.globalvarProvider.version = await this.appVersion.getVersionNumber();
        console.log(this.globalvarProvider.version);
        this.api.getAPKVersion().then((lastversion)=>{
          this.lastversion = lastversion.version;
          if(Util.compareVersion(this.globalvarProvider.version,lastversion.version) === -1) {
            this.versionPopUp = true;
          }
        });
      }
      catch (e) {
        // no version info (ionic serve)
      }

      this.splashScreen.hide();

      this.checkConnection();

    });

    this.globalvarProvider.loginEvent.subscribe(r => {
      this.init();
    });

    this.globalvarProvider.logoutEvent.subscribe(r => {
      this.logged = false;
      this.messageProvider.clearMessages();
      this.checkAuthentification();
    });

    this.localNotifications.on('click').subscribe(notification => {
      this.globalvarProvider.openChat(true);
      this.localNotifications.cancelAll();
      /*this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#f78000');
      this.statusBar.styleLightContent();*/
    });

    document.addEventListener("pause", () => {

      if(this.globalvarProvider.isChatOpen) {
        this.logs.stopAndSend('chat');
      }
      else {
        if (this.globalvarProvider.active_tab === 0) {
          this.logs.stopAndSend('home');
        } else if (this.globalvarProvider.active_tab === 1) {
          this.logs.stopAndSend('activity');
        } else if (this.globalvarProvider.active_tab === 2) {
          this.logs.stopAndSend('charts');
        } else if (this.globalvarProvider.active_tab === 3) {
          this.logs.stopAndSend('profile');
        }
      }

    });

    document.addEventListener("resume", ()=> {
      if(this.globalvarProvider.isChatOpen) {
        this.logs.start('chat');
      }
      else {
        if (this.globalvarProvider.active_tab === 0) {
          this.logs.start('home');
        } else if (this.globalvarProvider.active_tab === 1) {
          this.logs.start('activity');
        } else if (this.globalvarProvider.active_tab === 2) {
          this.logs.start('charts');
        } else if (this.globalvarProvider.active_tab === 3) {
          this.logs.start('profile');
        }
      }

    });


  }

  checkConnection(){

    this.hasConnection = true;//this.network.type!=='none';

    if(this.hasConnection) {
      this.checkAuthentification();
    }
    else {
      this.loaded = true;
      this.globalvarProvider.error = true;
    }
  }

  async presentAlert(message) {
    let okText = await this.translate.get('ok').toPromise().then((res)=> {return res});

    const alert = await this.alertController.create({
      message: message,
      enableBackdropDismiss: false,

      buttons: [{
        text: okText
      }],
    });
    await alert.present();
  }



  isFirstPage(){
    let c = this.nav.getAllChildNavs()[0];
    if(c) {
      console.log(c.getSelected().index);
    }
    return false;
  }

  private checkAuthentification(){
    console.log('checkauthentication')
    this.loaded = false;
    this.globalvarProvider.error = false;
    this.iamClient.checkAuth().then(check => {

      if (check) {

        this.iamClient.getUser().then(user => {

          this.globalvarProvider.setId(user.sub);

          this.storage.get('lastRefresh').then(refreshDate => {
            let todaysDate = new Date();

            if (refreshDate && refreshDate == todaysDate.setHours(0, 0, 0, 0)) {

              this.init();
            } else {

              this.iamClient.updateToken().then(() => {

                this.init();
              }, error => {
                this.startLoginPage();
              });
            }
          });
        }).catch((error)=> {
            console.log(error);
            if(error.type!==3) {
              //this.globalvarProvider.logoutEvent.next();
              this.startLoginPage();
            }
            else {
              this.loaded = true;
              this.globalvarProvider.error = true;
            }
          }
        );

      } else {
        this.startLoginPage();
      }
    });
  }

  private startLoginPage(){
    let appLanguage;
    let phoneLanguage = navigator.language.split("-", 1)[0];
    if (phoneLanguage == "nl" || phoneLanguage == "es" || phoneLanguage == "it") {
      appLanguage = phoneLanguage;
    } else if (phoneLanguage === "ca") {
      appLanguage = "es";
    } else {
      appLanguage = "en";
    }

    this.translate.reloadLang(appLanguage).subscribe((c)=>{
      this.translate.use(appLanguage).subscribe((c)=> {
        this.langLoaded = true;
      });
      this.openLoginPage();
    },(e)=>{
      console.log(e);
      console.error('error lang');
      this.loaded = true;
      this.globalvarProvider.error = true;
    });
  }

 private openLoginPage(){
   this.rootPage = LoginPage;
   this.loaded = true;
   this.globalvarProvider.error = false;
 }

  private init() {

    this.loaded = false;
    this.logged = true;
    this.globalvarProvider.error = false;

    this.initLanguage();

    this.registerLocales();

    this.addNotificationEvent();

    this.initBackButton();

    this.initStatusBar();

    this.addPlatformEvents();

    this.addRefreshTokenEvent();

    this.registerBroadcastReceiver();

    if(this.storage.get('currentPhysicalActivity').then(activity=>{
      this.globalvarProvider.startedActivity = activity;
    }))

      document.addEventListener("resume", ()=>{
        this.api.initUser().then(
          (res) => {
            console.log(res);
            this.ngZone.run(() => {
              this.localNotifications.cancelAll();
                this.globalvarProvider.profile = res;
              this.treatPendingMessages();
              this.globalvarProvider.startQuestionnaireEvent.next();
            });
          });

      }, false);




    this.addTokenEvent().then(()=>
    {
      this.langLoaded = false;
      console.log('gettoken');

      this.api.initUser().then(
        (res) => {
          this.globalvarProvider.profile = res;
          //this.globalvarProvider.profile.dss_profile.working_memory['questionnaires_done'].push('PATH');
          //delete this.globalvarProvider.profile.dss_profile.working_memory.pathways;
          this.ngZone.run(() =>
            this.loaded = true
          );
          this.initLanguage2(this.globalvarProvider.profile.lang);
          this.rootPage = TabsPage;
          this.storage.get('logs').then((res) => {
            if (!res) {
              res = [];
            }
            this.storage.set('logs', res);

          });
          this.wot.ping();
          if(this.globalvarProvider.profile.stage!=='baseline'){
            this.treatPendingMessages();
          }
        },
        (err)=>{
          console.log('error DSS');
          this.loaded = true;
          this.globalvarProvider.error = err;

        }
      )
    });
  }

  private registerLocales(){
    registerLocaleData(localeES, 'es');
    registerLocaleData(localeEN, 'en');
    registerLocaleData(localeIT, 'it');
    registerLocaleData(localeNL, 'nl');
  }

  private registerBroadcastReceiver(){
    let self = this;
    if((<any>window).plugins){ // useful when we test without deploying
      (<any>window).plugins.intentShim.registerBroadcastReceiver({
          filterActions: [
            'institute.humantech.nestore.action.WOT_ACK',
            'institute.humantech.nestore.action.WOT_ERROR',
            'institute.humantech.nestore.action.WOT_SUCCESS',
          ]
        },
        function(intent) {
          self.wot.WotMessage.next(intent)
        }
      );
    }
  }

  private initBackButton() {
    this.platform.registerBackButtonAction(() => {
      console.log('back');
      if(this.globalvarProvider.showTuto){
        return;
      }
      if(this.globalvarProvider.selectedPathway!==null){
        this.globalvarProvider.selectedPathway = null;
      }
      else if (this.globalvarProvider.selectedDomain && this.globalvarProvider.canBack) {
        this.globalvarProvider.selectedDomain = false;
        this.game = null;
        this.initStatusBar();
      }
      else if (this.globalvarProvider.isChatOpen) {
        this.globalvarProvider.openChat(false);
        this.initStatusBar();
      }
      else if (this.globalvarProvider.selectedDomain && !this.globalvarProvider.canBack) {
        this.globalvarProvider.back.next();
      }
      else if(this.globalvarProvider.isActivityOpen){
        this.globalvarProvider.isActivityOpen=false
      }
      else if(this.globalvarProvider.blur){
        this.globalvarProvider.blur = !this.globalvarProvider.blur;
      }
      else if(this.nav.getAllChildNavs()[0].getSelected().index){
        this.globalvarProvider.selectedTab = 0;
        this.nav.getActiveChildNavs()[0].select(0);
      }
      else if(!this.globalvarProvider.quit){
        this.globalvarProvider.quit = true;
      }
      else {
        this.appMinimize.minimize();
      }
    });
  }

  private initLanguage2(val) {
    this.translate.use(val).subscribe((m)=>{
      this.langLoaded = true;
    },(e)=>{
      console.log('lang : error');
    });
    this.globalvarProvider.lang = val;
  }

  private initLanguage() {
    this.storage.get('lang').then((val) => {
      this.translate.addLangs(['en', 'es','nl']);
      this.translate.setDefaultLang('en');

      let lang;
      switch(val) {
        case 2: lang = 'it'; break;
        case 3: lang = 'es'; break;
        case 4: lang = 'nl'; break;
        default: lang = 'en';
      }
      console.log("select language ", val, lang);
      this.translate.use(lang).subscribe(()=>{
        console.log('lang : set');

      },(e)=>{
        console.log('lang : error');
      })
      ;
      this.globalvarProvider.lang = val;


      this.translate.onLangChange.subscribe((event) => {
        this.translate.get('notification_title').subscribe(res => {
          console.log(res);
          this.globalvarProvider.textNotificationTitle = res;
        })
      });
    });

  }

  private initStatusBar() {
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByName('white');
    this.statusBar.styleDefault();
  }

  private addPlatformEvents() {
    this.platform.resume.subscribe(() => {
      this.globalvarProvider.setIsSleeping(false);
      if (this.globalvarProvider.isChatOpen) {
        //this.localNotifications.cancelAll(); // TODO: Delete only notification related with chat.
      }
    });

    this.platform.pause.subscribe(() => {
      this.globalvarProvider.setIsSleeping(true);
    });
  }

  private addTokenEvent() {
    return new Promise((resolve, reject) =>
    {
      this.fcm.getToken().then(token => {
        this.globalvarProvider.setToken(token);
        resolve();
      }).catch(error=>{
        resolve()//reject(); //in order to test without cordova
      });
    });
  }

  private addRefreshTokenEvent() {
    this.fcm.onTokenRefresh().subscribe(token => {
      this.globalvarProvider.setToken(token);
    });
  }

  private addNotificationEvent() {
    this.fcm.onNotification().subscribe(data => {
      let body = JSON.parse(data.body);

      if (!data.wasTapped) {

        if(body && body.partialProfile){
          Util.setProfile(this.globalvarProvider.profile, body.partialProfile)
        }

        if(body && body.action){
          this.ngZone.run(()=> {
            if(body.action=='pathway-selection') {
              this.globalvarProvider.profile['dss_profile']['working_memory']['questionnaires_done'].push('PATH');
              console.log(this.globalvarProvider.profile);
            }
            this.globalvarProvider.blur = false;
          });
        }
        else if(body && body.progress){
          console.log('PROGRESS');
          console.log(body.progress);
          this.ngZone.run(()=> {
            this.globalvarProvider.profile.progress = body.progress;
          });
        }
        else if(body && body.profile_change){
          this.api.initUser().then(
            (res) => {
              this.ngZone.run(() => {
                this.globalvarProvider.profile = res;
                this.initLanguage2(this.globalvarProvider.profile.lang);
              });
            });
        }
        else if(body && body.profile){
          console.log('message profile');
          this.globalvarProvider.profile= body;
        }
        else if(body && body.untreatedMessage){
          this.presentAlert("Untreated Message received :"+body.untreatedMessage).then() //TODO: REMOVE ONCE ALL MESSAGES ARE CORRECTLY TREATED
        }
        else {
          if(body.type==='activities'){
            this.globalvarProvider.reloadProposedActivities();
          }
          console.log(data.body);
          this.api.deletePendingMessages();

          if(this.globalvarProvider.profile.conversation && this.globalvarProvider.profile.conversation.app) {
            this.globalvarProvider.profile.conversation.app.lastMessage = body;
          }

          this.platform.zone.run(() => {
            if(body.mode!=='InQueue') {
              this.messageProvider.addMessage(body, null)
            }
          });
        }
      }
    });
  }

  private treatPendingMessages(){
    if(this.globalvarProvider.profile && this.globalvarProvider.profile.conversation && this.globalvarProvider.profile.conversation.app && this.globalvarProvider.profile.conversation.app.pendingMessages){
      for(let message of this.globalvarProvider.profile.conversation.app.pendingMessages) {
          if (!this.messageProvider.messages.find((m) => m.id === message.id)) {
            this.messageProvider.addMessage(message, null, false);
          }
      }
    }
    this.api.deletePendingMessages().then();
  }

  async presentToast(message) {
    const toast = await this.toast.create({
      message: message,
      duration: 10000
    });
    toast.present();
  }

  quit(){
    this.platform.exitApp();
  }

  goPlayStore(){
    if((<any>window).plugins){ // useful when we test without deploying
      (<any>window).plugins.intentShim.startActivity({
          //https://github.com/darryncampbell/plugin-intent-api-exerciser/blob/master/www/js/index.js#L120
          action: (<any>window).plugins.intentShim.ACTION_VIEW,
          url:"market://details?id=institute.humantech.nestore"

    },
        function() {},
        function() {}
      );
    }
    this.versionPopUp = false;
  }

}
