import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from 'rxjs/Rx';
import { ENVIRONMENT } from "../environement.config";
import {Storage} from "@ionic/storage";
import {APIService} from "../chat/services/api";
import {LogsService} from "../providers/logsService";

@Injectable()
export class GlobalvarProvider {
  public _isChatOpen : boolean = false;
  public chatCreated = false;

  public environment = ENVIRONMENT;
  public onChatOpen = new EventEmitter();

  public active_tab:any;
  private isSleeping = false;
  public lang;
  public id;
  public changeTab = new EventEmitter();
  public isActivityOpen = false;
  public chatPhoto;
  public firstname;
  public token;
  public textNotificationTitle;

  public selectedTab;

  public startedActivity;
  public lastTestDate;

  public startQuestionnaireEvent = new Subject();

  public loginEvent = new Subject();
  public logoutEvent = new Subject();

  public profile;
  public waterUpdate = new Subject();

  public startGame = new Subject();

  public reloadActivitiesSubject = new Subject();
  public reloadChartSubject = new Subject();

  public reloadProposedActivitiesSubject = new Subject();
  public proposedActivitiesLoaded;

  public blur = false;
  public tuto;
  public selectedPathway = null;

  public selectedDomain = null;

  public selectedGame = null;

  public currentGame = null;
  public quit = null;

  public canBack = true;

  public showTuto = 0;
  public showTip = false;
  public tutoTitle;
  public tutorials: any = {};
  public tutorialDescription;
  public tutorialTitle;
  public tutorialCurrent;
  public tutorialSubject;

  public tutorialC;

  public error;
  public internalError;

  public message;
  public currentQuestionnaire;

  public click2weeks = false;
  public processLoading = false;

  public version;

  back = new Subject();


  get isChatOpen(): boolean {
    return this._isChatOpen;
  }

  constructor(private storage: Storage, private logs: LogsService){
    this.active_tab=null;
  }

  reloadActivities(){
    this.reloadActivitiesSubject.next();
  }

  reloadChart(options){
    this.reloadChartSubject.next(options);
  }

  reloadProposedActivities(){
    this.reloadProposedActivitiesSubject.next();
  }

  openChat(value = true){
      this.chatCreated = true;
      if(value){
        this.logs.start('chat');
        if(this.active_tab==0){
          this.logs.stopAndSend('home')
        }
        else if(this.active_tab==1){
          this.logs.stopAndSend('activity')
        }
        else if(this.active_tab==2) {
          this.logs.stopAndSend('charts')
        }
        else if(this.active_tab==3) {
          this.logs.stopAndSend('profile')
        }
      }
      else if(this.isChatOpen) {
        this.logs.stopAndSend('chat');
        if(this.active_tab==0){
          this.logs.start('home')
        }
        else if(this.active_tab==1){
          this.logs.start('activity')
        }
        else if(this.active_tab==2) {
          this.logs.stopAndSend('charts')
        }
        else if(this.active_tab==3) {
          this.logs.stopAndSend('profile')
        }
      }
      this._isChatOpen = value;
  }

  beginGame(game, taskId=null, messageId=null, messageIndex=null){
    if(this.profile.stage == 'baseline'){

    }
    this.selectedDomain='cognitive';
    this.currentGame = {name:game, taskId: taskId, messageId:messageId, messageIndex:messageIndex};
  }

  setActiveTab(val){
    this.selectedTab=val;
    console.log(val);
    if(this.active_tab===0 && val!==0){
      this.logs.stopAndSend('home')
    }
    if(this.active_tab===1 && val!==1){
      this.logs.stopAndSend('activity')
    }
    if(this.active_tab===2 && val!==2){
      this.logs.stopAndSend('charts')
    }
    if(this.active_tab===3 && val!==3){
      this.logs.stopAndSend('profile')
    }
    this.active_tab=val;
    this.changeTab.next(val);
    if(val===0){
      this.logs.start('home')
    }
    if(val===1){
      this.logs.start('activity')
    }
    if(val===2){
      this.logs.start('charts')
    }
    if(val===3){
      this.logs.start('profile')
    }
  }

  setIsSleeping(val){
    this.isSleeping=val;
  }
  getIsSleeping(){
    return this.isSleeping;
  }
  setId(val){
    this.id=val;
  }
  getId(){
    return this.id;
  }
  getToken(){
    return this.token;
  }
  setToken(token){
    this.token=token;
  }

  nextTip() {
    this.tutorials[this.tutorialCurrent] = {'activated': false};
    let currentList = this.list.find(x=>x.name===this.tutorialC).flow;
    let el = currentList.findIndex(x => x.name === this.tutorialCurrent);
    if(currentList[el].actionAfter){
      currentList[el].actionAfter();
    }
    if(this.tutorialSubject) {
      this.tutorialSubject.next(true);
    }
    if (el >= 0 && el + 1 < currentList.length) {
      this.addTutorial(this.tutorialC,currentList[el + 1].name);
    }
    else{
      this.showTuto=0;
    }
  }

  copyElement(e, left, position, subject, name){
    console.log(position);
    let c = document.getElementById('canvas');
    while (c.firstChild) {
      c.removeChild(c.firstChild);
    }
    console.log(e.style);
    let cln = <HTMLElement>e.cloneNode(true);
    this.tutorialCurrent = name;
    this.tutorialSubject = subject;

    cln.addEventListener('click', ()=>{
      this.nextTip();
      });
    let divOffset = this.offset(e);
    console.log(divOffset);
    cln.style.position = 'absolute';
    cln.style.top = divOffset.top+"px";
    if(left) {
      cln.style.left = divOffset.left + "px";
    }
    console.log(e.clientWidth);
    console.log(e.getBoundingClientRect());
    cln.style.width =  e.clientWidth+'px';
    cln.style.height =  e.clientHeight+'px';

    //workaround to avoid to open file chooser in tutoNutrition
    if(cln.children[2] && cln.children[2]['htmlFor']){
      cln.children[2]['htmlFor'] = '';
    }


    c.appendChild(cln);

    let c2 = document.getElementById('tutorialWindowText');
    console.log(c.offsetHeight);
    console.log(divOffset.top);
    if(position === 'bottom') {
      console.log('bottom');
      c2.style.bottom = null;
      c2.style.top = divOffset.top + e.clientHeight + 'px';
    }
    else if(position==='top'){
      console.log('top');
      c2.style.top = null;
      c2.style.bottom = c.clientHeight - divOffset.top + 'px';
    }
    else if(position==='under') {
      let values3 = c.getElementsByClassName('tutoIcon')[0].getBoundingClientRect();
      c2.style.top = values3.top + 30 + 'px';
      if(values3.top + 30 < 10){
        c2.style.top = 10 + 'px';
      }
    }
    else if(position==='above') {
      let values3 = c.getElementsByClassName('tutoIcon')[0].getBoundingClientRect();
      c2.style.top = null;
      c2.style.bottom = c.clientHeight - values3.top - 40 + 'px';
    }
  }

  offset(el) {
    let rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }

  list = [
    {name:'nutrition',
      flow:[
          {name:'chatBack', action:()=>{this.setActiveTab(0);this.openChat(true)}},
          {name:'activities'},
          {name:'nutrition_select', actionAfter:()=>{this.remove()}},
          {name:'activities_window', action:()=>{this.openChat(false);this.setActiveTab(1)}},
          {name:'date_back'},
          {name:'timeframe_switcher', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'charts'},
          {name:'nutrition_select2', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'energy', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'progress', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'nutrients', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'water', actionAfter:()=>{this.showTuto=0;this.remove()}},
          {name:'plus_button',actionAfter:()=>{this.remove()}},
          {name:'glass', actionAfter:()=>{this.remove()}},
          {name:'selectGlass'},
          {name:'selectGlass2'},
          {name:'saveWater', actionAfter:()=>{this.showTuto=0;this.remove()}}
        ]
    },
    {name:'meal_registration',
      flow:[
        {name:'chatBack', action:()=>{this.setActiveTab(0);this.openChat(true)}},
        {name:'plus_button2', actionAfter:()=>{this.remove();}},
        {name:'nutrition_icon'},
        {name:'camera'},
        {name:'gallery'},
        {name:'gallery2'},
        {name:'image'},
        {name:'dish_choice'},
        {name:'add'},
        {name:'nutritional_infos'},
        {name:'sizes'},
        {name:'showmore'},
        {name:'saveMeal'},
        {name:'feedback'},
        {name:'image2'},
        {name:'scroller', actionAfter:()=>{this.remove()}},
        {name:'combo'},
        {name:'ingredients'},
        {name:'comboModfiy', actionAfter:()=>{this.remove()}},
        {name:'ingredients_remove'},
        {name:'ingredients_add'},
        {name:'ingredients_add2'},
        {name:'add2'},
        {name:'nutritional_infos2'},
        {name:'sizes2'},
        {name:'showmore2'},
        {name:'saveMeal2'},
        {name:'feedback2', actionAfter:()=>{this.remove();this.showTuto=0}},
      ]
    }
  ];

  treatTutorial(name, e=null, subject=null, left=true, position='top', description=null, title=null){
    this.tutoTitle = name;
    if(this.tutorials && this.tutorials[name] && this.tutorials[name].activated){
      if(!this.tutorials) {
        this.tutorials = {};
      }
      this.tutorials[name] = {'activated': false};
      this.tutorialDescription = description;
      this.tutorialTitle = title;
      this.storage.set('tutorials', this.tutorials);
      if(e) {
        this.showTuto=1;
        this.showTip = true;
        this.copyElement(e, left, position, subject, name);

        let currentList = this.list.find(x=>x.name===this.tutorialC).flow;
        let el = currentList.findIndex(x => x.name === this.tutorialCurrent);
        console.log(currentList);
        console.log(el);
        if(currentList[el].action){
          currentList[el].action();
        }
      }
    }
    else{
      if(subject) {
        subject.next();
      }
    }
  }

  addTutorial(tutoName, tipName=null){
    if(!this.tutorials){
      this.tutorials={};
    }
    this.tutorialC = tutoName;
    let currentList = this.list.find(x=>x.name===tutoName).flow;
    if(tipName){
      this.tutorials[tipName] = {activated: true};
    }
    else if(currentList[0]) {
      let currentTip = currentList[0];
      this.tutorials[currentTip.name] = {activated: true};
    }
    this.storage.set('tutorials', this.tutorials);
  }

  remove(){
    this.showTip=false;
    let c = document.getElementById('canvas');
    while (c.firstChild) {
      c.removeChild(c.firstChild);
    }
  }

}
