import {HammerGestureConfig} from '@angular/platform-browser';
declare var Hammer: any;

// This allows to scroll on elements that implements gesture detection in a scroll view
export class HammerConfig extends HammerGestureConfig {
  buildHammer(element: HTMLElement) {
    let mc = new Hammer(element, {
      touchAction: "pan-y",
      recognizers: [
      [Hammer.Pan,{ direction: Hammer.DIRECTION_HORIZONTAL }],
      [Hammer.Press]
      ]
    });
    return mc;
  }
}
