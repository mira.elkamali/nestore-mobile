import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule, HAMMER_GESTURE_CONFIG } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler, IonicPageModule, IonicGestureConfig } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { Keyboard } from "@ionic-native/keyboard";
import { SplashScreen } from "@ionic-native/splash-screen";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ChartsModule } from "ng2-charts";
import { SpeechRecognition } from "@ionic-native/speech-recognition";
import { TextToSpeech } from "@ionic-native/text-to-speech";
import { Camera } from "@ionic-native/camera";
import { IonicImageViewerModule } from "ionic-img-viewer";
import { AutoCompleteModule } from "ionic2-auto-complete";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { FCM } from "@ionic-native/fcm";
import { Calendar } from "@ionic-native/calendar";
import { DatePicker } from "@ionic-native/date-picker";
import { Network } from "@ionic-native/network";
import { NativeStorage } from "@ionic-native/native-storage";

import { MyApp } from "./app.component";
import { GlobalvarProvider } from "./globalprovider";

import { HomePage } from "../pages/home/home";
import { TabsPage } from "../pages/tabs/tabs";
import { UserPage } from "../pages/user/user";

import { NutritionPage } from "../pages/nutrition/nutrition";
import { NutritionSelectPage } from "../pages/nutrition-select/nutrition-select";
import { MealDetailPage } from "../pages/meal-detail/meal-detail";

import { ChatWindowComponent } from "../chat/chat-window/chat-window.component";

import { UsersService } from "../chat/user/users.service";
import { APIService } from "../chat/services/api";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from "@ngx-translate/core";
import { IonicStorageModule } from "@ionic/storage";
import { HttpModule } from "@angular/http";
import { LoginPage } from "../pages/login/login";
import { MessageProvider } from "../chat/services/message-provider";
import { IamClientProvider } from "../providers/iam-client/iam-client";
import { AppVersion } from "@ionic-native/app-version";

// import {
//   MqttModule,
//   MqttService
// } from 'angular2-mqtt';
import { ComponentsModule } from "../components/components.module";
import { MyActivityPage } from "../pages/my-activity/my-activity";
import { ChartsPage } from "../pages/charts/charts";
import { DataProvider } from "../providers/data/data";
import { ApiProvider } from "../providers/api/api";
import { DateFormatPipe } from "../providers/helpers";
import { SubmitDishPage } from "../pages/submit-dish/submit-dish";
import { CognitivePage } from "../pages/cognitive/cognitive";
import { MemoryTest } from "../pages/cognitive/memoryTest/memoryTest";
import { SocialPage } from "../pages/social/social";
import { ProposedActivitiesPage } from "../pages/proposed-activities/proposed-activities";
import { PhysicalPage } from "../pages/physical/physical";
import { NBackGameComponent } from "../pages/cognitive/nBackTest/nBack";
import { HammerConfig } from "./HammerConfig";
import { WaterIntakePage } from "../pages/water-intake/water-intake";
import { LoginRegisterProvider } from "../pages/login/login-register/login-register";
import { RegisterPage } from "../pages/login/register/register";
import { RecaptchaModule } from "ng-recaptcha";
import { DigitSpanBackwardsComponent } from "../pages/cognitive/DigitSpanBackwards/digitSpanBackwards";
import { GooglePlus } from "@ionic-native/google-plus";
import { RegisterPageModule } from "../pages/login/register/register.module";
import { ClickOutsideModule } from "ng-click-outside";
import { WheelSelector } from "@ionic-native/wheel-selector";
import {AppMinimize} from "@ionic-native/app-minimize/ngx";

import { HTTP } from "@ionic-native/http";
import {Ng2ImgMaxModule} from "ng2-img-max";
import {MyMissingTranslationHandler} from "./MyMissingTranslationHandler";
import {WOTService} from "../providers/WOTService";
import {TipsWelcomeComponent} from "../tips/tips-welcome.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LogsService} from "../providers/logsService";
import {LoadDirective} from "../pipes/LoadDirective";
import {Insomnia} from "@ionic-native/insomnia";
import {ENVIRONMENT} from "../environement.config";
import {EmailComposer} from "@ionic-native/email-composer";
import {Rooms} from "../pages/rooms/rooms";
import {RoundPipe} from "../pipes/RoundPipe";

export const MQTT_SERVICE_OPTIONS = {
  hostname: "broker.mqttdashboard.com",
  port: 8000,
  path: "",
  protocol: "mqtt"
};

// export function mqttServiceFactory() {
//   return new MqttService(MQTT_SERVICE_OPTIONS);
// }
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, ENVIRONMENT.COACH_SERVER + 'translation/mobile/');
  //return new TranslateHttpLoader(http, 'http://160.98.116.120:5000/translation/mobile/');
}

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    MyActivityPage,
    ChartsPage,
    UserPage,
    NutritionPage,
    SocialPage,
    PhysicalPage,
    ProposedActivitiesPage,
    NutritionSelectPage,
    MealDetailPage,
    ChatWindowComponent,
    SubmitDishPage,
    CognitivePage,
    LoginPage,
    WaterIntakePage,
    MemoryTest,
    NBackGameComponent,
    DigitSpanBackwardsComponent,
    DateFormatPipe,
    TipsWelcomeComponent,
    Rooms,
    RoundPipe,
  ],
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: MyMissingTranslationHandler},

      useDefaultLang: false
    }),
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule, // TODO: remove this Module, use HttpClientModule instead
    //MqttModule.forRoot({
    //  provide: MqttService,
    //  useFactory: mqttServiceFactory
    //}),
    IonicModule.forRoot(MyApp),
    ChartsModule,
    IonicImageViewerModule,
    AutoCompleteModule,
    IonicStorageModule.forRoot(),
    ComponentsModule,
    IonicPageModule.forChild(LoginPage),
    RecaptchaModule.forRoot(),
    RegisterPageModule,
    ClickOutsideModule,
    Ng2ImgMaxModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    MyActivityPage,
    ChartsPage,
    UserPage,
    LoginPage,
    ChatWindowComponent,
    NutritionPage,
    NutritionSelectPage,
    MealDetailPage,
    SubmitDishPage
  ],
  exports: [
    DateFormatPipe
  ],
  providers: [
    AppVersion,
    LoginRegisterProvider,
    Keyboard,
    StatusBar,
    SplashScreen,
    APIService,
    WOTService,
    UsersService,
    //SpeechRecognition,
    TextToSpeech,
    GlobalvarProvider,
    Camera,
    LocalNotifications,
    FCM,
    HttpClientModule,
    Calendar,
    DatePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    IamClientProvider,
    Network,
    MessageProvider,
    DataProvider,
    ApiProvider,
    DateFormatPipe,
    NativeStorage,
    //GooglePlus,
    AppMinimize,
    WheelSelector,
    HTTP,
    LogsService,
    Insomnia,
    EmailComposer,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig
    }
  ]
})
export class AppModule {}
