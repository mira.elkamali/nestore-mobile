import {ChangeDetectorRef, Injectable} from '@angular/core';
import { Storage } from '@ionic/storage';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Message} from '../message/message.model';
import {APIService} from './api';
import {NativeStorage} from '@ionic-native/native-storage';
import {Subject} from "rxjs";
import {Util} from "../../util";
const AsyncLock = require('async-lock');
let lock = new AsyncLock();

const STORAGE_KEY = 'messages';

@Injectable()
export class MessageProvider {
  errorSentence = "";
  wait1Sentence = "";
  lastMessageId;

  messages = [];
  allMessages = [];
  messagesChange = new Subject();
  messagesLoaded = false;
  numberShown = 20;
  unreadMessages = 0;
  translatePipe;


  constructor(public translate: TranslateService,
              private storage: Storage,
              private globalProvider : GlobalvarProvider,
              private api: APIService,
              private nativeStorage: NativeStorage,
  ) {


    translate.onLangChange.subscribe(()=> {
      translate.get('example_sentence-error').toPromise().then((res) => {
        this.errorSentence = res;
      });
      translate.get('example_sentence-wait1').toPromise().then((res) => {
        this.wait1Sentence = res;
      });
    });
    this.loadMessage();
  }

  showMore(){
    this.numberShown = this.numberShown+30;
    this.loadMessage()
  }

  loadMessage(){
    this.getAllMessage().then((res)=>{
      this.allMessages = res;
      this.messages = this.allMessages.slice(Math.max(0, this.allMessages.length - this.numberShown),this.allMessages.length);
      this.messagesLoaded = true;
      this.unreadMessages = this.allMessages.filter(message=>!message.isRead).length;
    })
  }

  newMessage(message: Message): void {

    if(message.author === this.globalProvider.getId()){
      let d = setTimeout(()=>{
        let res = {};
        if(message.type==='listmeal' || message.image){

          res['text'] = this.wait1Sentence;
          this.addMessage(res,null);
        }
        else if(!message.mode){
          //res['text'] = this.wait2Sentence;
        }
      },5000);
      this.api.send(message).then(res => {
          if(res && res.partialProfile) {
            Util.setProfile(this.globalProvider.profile, res.partialProfile);
          }

          if(res.error){
            message.previous.answered = false;
            res['text'] = this.errorSentence;
          }
          else if(res.data && res.data.photoId){
            console.log('there is a photo id');
            this.nativeStorage.setItem(res.data.photoId, { photo: this.globalProvider.chatPhoto })
              .then(
                () => {
                  console.log("stored photo!");
                },
                (error) => console.log("Error storing photo", error)
              );
          }
          console.log(message);
          this.addMessage(res,message);
          clearTimeout(d);
        }
      ).catch((e)=> {
          console.log(message);
          clearTimeout(d);
          this.treatPendingMessages();
          //let res = {};
          //res['text'] = this.errorSentence;
          //this.globalProvider.error = true;
          //this.addMessage(res,null)
        }
      );
    }
    if(!this.isQuestionnaire(message)) {
      this.storeMessage(message);
    }
      this.messagesChange.next(message);
  }

  addMessage(res, message, delay=true){

    if(res && res.profile && res.profile.questionnaire){
      this.globalProvider.profile.questionnaire = res.profile.questionnaire; // TODO: method to upload the whole profile
    }
    if(res.mode==='questionnaire' || res.mode==='questionnaireStart' || res.mode==='questionnaireEnd' || res.mode==='questionnaireWaiting'){
      this.newMessage(
         new Message({
          isTyping: true,
          bot: true,
          author: null,
          text: res.text,
          image: res.image,
          data: res.data,
          type:  res.type,
          action: res.action,
          isRead: false,
          choice: res.choice,
          timeBeforeDisplay: 0,
          canWrite: this.canWrite(res),
          discussionEnd: res.discussionEnd,
          mode: res.mode,
          intent: res.intent ? res.intent[0] : null
        })
      );
    }
    else {
      console.log('id');
      console.log(res.id);
      this.lastMessageId=res.id;
      let split = res.text.split('\n\n');
      let time = 0;
      let totaltime = 0;
      let index = 0;
      let index2 = 0;
      //let i = 0;
      for (let m of split) {
        if (index == 0) {
          time = 3000;
        } else {
          let previousMessage = split[index - 1].length * 30;
          time = Math.max(3000, Math.min(previousMessage, 7000));
        }
        console.log('index:' + index + 'time :' + time);
        setTimeout(() => {
          let t = 0;
          if (index2 == 0) {
            t = 3000;
          } else {
            let previousMessage = split[index2 - 1].length * 30;
            t = Math.max(3000, Math.min(previousMessage, 7000));
          }
          console.log('index:' + index2 + 'time :' + t);
          index2++;
          console.log(t);
          let write = this.canWrite(res)
          console.log(write)
          this.newMessage(
             new Message({
              id: res.id,
              index: index2,
              isTyping: true,
              bot: true,
              author: null,
              text: m,
              data: res.data,
              image: res.image,
              type: split[split.length - 1] === m ? res.type : null,
              action: res.action,
              isRead: false,
              choice: split[split.length - 1] === m ? res.choice : [],
              timeBeforeDisplay: delay?t:0,
              canWrite: write,
              discussionEnd: res.discussionEnd,
              mode: res.mode,
              intent: res.intent ? res.intent[0] : null})
          );
          //i++;
        }, totaltime);
        totaltime += time;
        index++;
      }
    }
  }

  storeMessage(message){
    if(!this.allMessages.find((m)=>m.id === message.id && m.index === message.index)){
      this.allMessages.push(message);
      this.save();
    }
  }

  reStore(message){
    let obj = this.allMessages.find((m)=>m.id === message.id);
    if(!obj.isRead) {
      lock.acquire("messages", async () => {
        if (obj) {
          obj = message;
          this.messagesChange.next(obj);
          console.log('message');
          this.save();
        }
      });
    }
  }

  readAll(){
    let unreadMessages = this.allMessages.filter((m)=>!m.isRead);
    for(let message of unreadMessages){
      message.isRead = true;
    }
    this.save();
  }

  getAllMessage() : Promise<any> {
    return this.storage.get(STORAGE_KEY).then(res=>{
      if(res) {
        return res;
      }
      return [];
    });
  }

  keepLastMessage(){
    this.allMessages = this.allMessages.slice(this.allMessages.length-1,this.allMessages.length);
    this.save();
  }

  treatPendingMessages(){
    if(this.globalProvider.profile && this.globalProvider.profile.conversation && this.globalProvider.profile.conversation.app && this.globalProvider.profile.conversation.app.lastMessage){
      if(this.lastMessageId !== this.globalProvider.profile.conversation.app.lastMessage.id) {
        this.addMessage(this.globalProvider.profile.conversation.app.lastMessage, null);
      }
    }
  }

  saveData(data, messageId, messageIndex){
    console.log('save message change');
    console.log(messageId,messageIndex);
    let obj = this.allMessages.find((m)=>m.id === messageId && (messageIndex?m.index === messageIndex:true));
    obj.data = data;
    this.messagesChange.next(obj);
    this.save();
  }

  private save(){
    console.log('save');
    this.storage.set(STORAGE_KEY, this.allMessages).then(()=>{
      this.messages = this.allMessages.slice(Math.max(0,this.allMessages.length-this.numberShown));
      this.unreadMessages = this.allMessages.filter(message => !message.isRead).length;
    })
  }

  public clearMessages(){
    this.messages = [];
    this.allMessages = [];
    this.storage.set(STORAGE_KEY, this.allMessages);
  }

  private canWrite(message): boolean{
    let b = message.type && typeof(message.type)==='string' && (message.type.startsWith('start_') || message.type.startsWith('tips_')) || (!message.choice || message.choice.length == 0) && (!message.type || message.type === 'firstname' || message.type === 'date' || message.type === 'number');
    console.log(b);
    return b;
  }

  private isQuestionnaire(message){
    return message.mode==='questionnaire' || message.mode==='questionnaireStart' || message.mode==='questionnaireEnd' || message.mode==='questionnaireWaiting';
  }

}
