import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Storage} from '@ionic/storage';
import { ENVIRONMENT } from "../../environement.config";
import {IHistory} from "../../models/interfaces";
import {NutritionEvent} from "../../models/nutrition-event";
import {History} from "../../models/history";
import {DateFormatPipe} from "../../providers/helpers";
import {Summary} from "../../models/summary";
import {Threshold} from "../../models/threshold";
import {LogsService} from "../../providers/logsService";

@Injectable()
export class  APIService {
  private URL = ENVIRONMENT.COACH_SERVER;

  constructor(private http: HttpClient, private globalvarProvider: GlobalvarProvider, private storage: Storage,    private _dateFormatPipe: DateFormatPipe, public logs: LogsService) {
  }

  postGame(state){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL+'game', state, this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }

  postLogs(log) {
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL+'logs', log, this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }

  putTestActivity(id, obj){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.put(this.URL+'test-activity/'+id,obj, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response);
    });
  }

  putLang(lang){
    return this.getAuthorizationTokenHeader().then((token)=> {
        return this.http.put(this.URL+'lang',{lang:lang}, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response);
    });
  }

  putParameters(name, value){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.put(this.URL+'parameters', {name:name, value:value}, this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }

  getAPKVersion(){
      return this.http.get(this.URL + 'apk-version').toPromise<any>().then(response => response)
    };

  getTrainingProgram(id){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'training-programs/' + id, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getPossibleSessions(userId){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'possible-sessions/' + userId, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  postStucturedPreferences(userId, obj){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL + 'structured-preferences', {userid: userId, body:obj}, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getPossiblePathways(userId){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'possiblepathways/' + userId, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  postCurrentPathways(userId, choice){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL + 'currentpathway', {userid: userId.toString(), currentpathway:choice}, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getMyProgress(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'my-progress/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getSummaryNutrition(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'summary/nutrients/'+timeframe+'/'+date, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }


  getWaterIntake(date: Date, timeframe){
    let dateText = date.getDate()+"."+(("0" + (date.getMonth() + 1)).slice(-2))+"."+date.getFullYear();
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'water-intake/?date=' + dateText+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  postWaterIntake(date: Date, quantity){
    let dateText = date.getDate()+"."+(("0" + (date.getMonth() + 1)).slice(-2))+"."+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
    let obj = {date:dateText, quantity:quantity};
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL + 'water-intake', obj, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  putProfile(obj){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.put(this.URL+'profile',obj, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getInteractions(date,timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'all/?date='+date+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getCirclesInteractions(date,timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'circle-interactions/?date='+date+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getDimensionalEmotions(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'dimensional-emotions/?date='+date+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getDiscreteEmotions(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'discrete-emotions/?date='+date+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }


  getSleep(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'sleep/?date='+date+'&timeframe='+timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getInteractionsKind(date,timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'interactions-kind/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getLoneliness(date,timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'loneliness/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getStructuredActivities(date, timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'structured-activities/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getUnstructuredActivities(date, timeframe){
    console.log('test');
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'unstructured-activities/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getMemoryFailures(date,timeframe){
    console.log('memory-failure');
    return this.getAuthorizationTokenHeader() .then((token)=> {
      return this.http.get(this.URL + 'memory-failures/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getMemoryAccuracy(date,timeframe){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'memory-accuracy/?date=' + date + '&timeframe=' + timeframe, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  startQuestionnaire(questionnaireId){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'start_questionnaire/' + questionnaireId, this.getAuthorizationHeader(token)).toPromise().then(response => response).catch(() => e => console.log(e));
    });
  }

  postQuestionnaireAnswer(data){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL + 'questionnaire-answer', data, this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }


  startProfileQuestions(userId){
    return this.http.get(this.URL+'start_profile_questions/'+userId, this.getAuthorizationHeader()).toPromise().then(response => response)
  }

  getProposedActivities(){
    this.globalvarProvider.proposedActivitiesLoaded = false;
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'proposed-activities/', this.getAuthorizationHeader(token)).toPromise().then(response => response)
    })
  }

  getProposedActivitiesExt(){
    this.globalvarProvider.proposedActivitiesLoaded = false;
    this.getProposedActivities().then((activities)=> {
      this.globalvarProvider.profile.activities = activities;
      this.globalvarProvider.proposedActivitiesLoaded = true;
      }
    ).catch(()=>{
      this.globalvarProvider.proposedActivitiesLoaded = true;
    });
  }

  getActivities(userId, date){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'activities/?date='+date, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  getUnstructuredWorkouts(){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'workouts', this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }


  getUnavailableTimeSlots(activityId){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL+'unavailable-timeslots/'+activityId, this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  postActivitySchedule(activityId, time, eventId=null){
    return this.getAuthorizationTokenHeader().then((token)=> {
      let obj = {"ce-entry-id":activityId, time:time};
      if(eventId){
        obj['eventId'] = eventId;
      }
      return this.http.post(this.URL+'activity-schedule/', obj , this.getAuthorizationHeader(token)).toPromise<any>().then(response => response)
    });
  }

  putActivity(userId, activityId, data) {
    return this.getAuthorizationTokenHeader().then((token) => {
      return this.http.put(this.URL + 'activities/' + userId + '/' + activityId, data, this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  initUser(){

    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL+'user', {
        id: this.globalvarProvider.getId(),
        token: this.globalvarProvider.getToken(),
        version: this.globalvarProvider.version
      }, this.getAuthorizationHeader(token))
        .toPromise()
        .then(response => response);
    });


  }


  getProfile(): Promise<any>{
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'user-profile', this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  resetOptions(): Promise<any>{
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.delete(this.URL + 'options', this.getAuthorizationHeader(token)).toPromise().then(response => response)
    });
  }

  getLangs(): Promise<any>{
    return this.http.get(this.URL+'translation/langs').toPromise().then(
      response => {
        console.log("getLangs", response);
        return response
      }).catch((e)=>{
        console.log(e);
    });
  }

  getQuestionnaire(){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'questionnaire', this.getAuthorizationHeader(token))
        .toPromise()
        .then(response => response).catch(reason => reason);
    });
  }

  send(message): Promise<any> {
    let obj = {};
    if (message.text!==null) {
      obj['text'] = message.text;
    }
    if (message.image) {
      console.log(message.image);
      obj['image'] = message.image;
    }
    if(message.intent){
      obj['intent'] = message.intent;
    }
    if (message.action) {
      obj['data'] = message.data;
    }
    if( message.imagedate){
      obj['imagedate'] = message.imagedate;
    }
    if (message.action) {
      obj['action'] = message.action;
    }
    if (message.mode) {
      obj['mode'] = message.mode;
    }
    if (message.type) {
      obj['type'] = message.type;
    }
    obj['id']=this.globalvarProvider.getId();
    obj['lang']=this.globalvarProvider.lang;
    obj['token']=this.globalvarProvider.getToken();
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL + 'chatbot?lang=' + this.globalvarProvider.lang, obj, this.getAuthorizationHeader(token))
        .toPromise()
        .then(response => response).catch(reason => reason);
    });
  }

  foodList(): Promise<any> {
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'meals', this.getAuthorizationHeader(token))
        .toPromise()
        .then(response => response);
    });
  }

  groupedFoodList(): Promise<any> {
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'meal-groups', this.getAuthorizationHeaderWithLanguage(token))
      .toPromise()
      .then(response => response);
  });
  }



  uploadPhoto(image) {
    const endpoint = 'photo-dish';
    const query = `${this.URL}${endpoint}`;

    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(
      query,
      {
        image: image,
        type: "breakfast",
      },
      this.getAuthorizationHeaderWithLanguage(token)
    )
      .toPromise()

  });
  }

  alternativeDishList(): Promise<any> {
    return this.http.get(this.URL + 'alternative_dish/' + this.globalvarProvider.getId(), this.getAuthorizationHeader()).toPromise().then(response=> response);
  }

  sendDishFromName(dish, text, meal, size, photoId = ""): Promise<any> {
    return this.getAuthorizationTokenHeader().then((token)=> {
      const obj = {};
      if(meal==='combo') {
        if (dish instanceof Array) {
          obj['listCombo'] = dish;
        }
        else{
          obj['listCombo'] = [dish];
        }
        obj["confirmedDish"] = text;
      }
      else {
        obj["confirmedDish"] = dish;
      }
      obj['size'] = size;
      if (photoId)
        obj["photoid"] = photoId;
      obj["type"] = meal;
      console.log(obj);
      return this.http
        .post(this.URL + "dish", obj, this.getAuthorizationHeaderWithLanguage(token))
        .toPromise();
    });
  }

  putDish(obj){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.put(this.URL + 'dish/', obj,
        this.getAuthorizationHeaderWithLanguage(token)
      )
        .toPromise()

    });
  }

  getDishDetails(dish: string): Promise<any> {
    const endpoint = 'summary-dish';

    const query = `${this.URL}${endpoint}/${dish}`;
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http
        .get(query, this.getAuthorizationHeader(token))
        .toPromise();
    });
  }

  postNutrientQuery(dishs): Promise<any> {
    const endpoint = 'nutrients-query';

    const query = `${this.URL}${endpoint}/`;
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http
        .post(query, dishs, this.getAuthorizationHeader(token))
        .toPromise();
    });
  }

  getHistory(timeframe: string = 'month', dateIn: Date = new Date()): Promise<any> {
    /**
     * There's only week and month endpoints available.
     * For day  => call 'week' and filter out the day
     * For week => call 'week' and filter from monday on
     * For month => call 'month' and filter from first of month
     */

    let date = new Date(dateIn.getTime());
    var offsetDate;

    // get data ahead of now
    switch (timeframe) {
      case "day":
        offsetDate = date;
        break;
      case "week":
        // -6 to get the current day also...
        offsetDate = (new Date).setDate(date.getDate() - 6);
        break;
      case "month":
        offsetDate = (new Date).setMonth(date.getMonth() - 1);
        break;
    }

    offsetDate = this._dateFormatPipe.transform(offsetDate);
    let query = `${this.URL}historyDishes/${
      timeframe == "day" ? "week" : timeframe
      }/${offsetDate}`;

    console.log(query);

    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get<IHistory>(query,this.getAuthorizationHeaderWithLanguage(token))
        .map((hist: IHistory) => {
          let nutritions: NutritionEvent[] = [];

          // first filter based on timeframe
          hist.nutrition.filter(item => {
            let eventDate = new Date(item["timestamp"] * 1000);

            if (timeframe == "day") {
              // only from this exact day
              return eventDate.toDateString() == date.toDateString();

            } else if (timeframe == "week") {
              var temp = date;
              let monday = new Date(temp.setDate(temp.getDate() - temp.getDay()));
              // only dates after monday
              return eventDate >= monday;
            } else if (timeframe == "month") {
              // no idea if we ever need month...

              return eventDate.getMonth() == date.getMonth();
            }

          })
            .map(item => {
              // create NutritionEvents
              let date = new Date(item["timestamp"] * 1000);
              nutritions.push(new NutritionEvent("nutrition",
                date,
                this._dateFormatPipe.transform(date),
                item["id"],
                item["dish"],
                item["meal"],
                item["availablePhoto"]
              ));
            });

          return new History(nutritions);
        })
        .toPromise();
    });
  }

  private getThresholdsRaw(): Promise<any> {
    let endpoint = 'thresholds/';

    let query = this.URL + endpoint;

    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(query, this.getAuthorizationHeader(token)
      ).toPromise();
    });
  }


  getSummary(timeframe = "day", dateIn = new Date()): Promise<Summary[]> {
    let date = new Date(dateIn.getTime());
    var offsetDate;

    // get data ahead of now
    switch (timeframe) {
      case "day":
        offsetDate = date;
        break;
      case "week":
        offsetDate = (new Date).setDate(date.getDate() - 6);
        break;
      case "month":
        offsetDate = (new Date).setMonth(date.getMonth() - 1);
        break;
    }

    let endpoint = 'summary';

    // Format:
    // .../summary/<day|week|month>/<id_user>/<day>
    const query = `${this.URL}${endpoint}/${timeframe}/${this._dateFormatPipe.transform(offsetDate)}`

    console.log("### Summary");
    console.log(query);

    // Format:
    // [[{"Alcohol":0,"Calcium":1167.9, ... ]]
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(query, this.getAuthorizationHeader(token)).toPromise()
        .then((data: any[]) => {
          if (data === undefined || data.length == 0) {
            return [];
          }

          if (timeframe == "day") {
            // remove unnecessary nesting
            const d = data[0][0];
            console.log(d);

            return Object.getOwnPropertyNames(d).map((key: any) =>
              new Summary(
                key,
                d[key]
              )
            )
          } else { // week and month are similar

            let summaries;

            if (timeframe == "week") {
              // we start calculating from monday..
              let monday = new Date(date.setDate(date.getDate() - date.getDay()));
              summaries = data.filter(item => new Date(item[0].day * 1000) >= monday)
                .map(item => item[0]);
            } else if (timeframe == "month") {
              summaries = data.filter(item => new Date(item[0].day * 1000).getMonth() == date.getMonth())
                .map(item => item[0]);
            }

            const nutrients = summaries[0] ?
              Object.getOwnPropertyNames(summaries[0]).filter(i => !["_id", "day"].includes(i)) :
              [];
            console.log("## summary");
            console.log(summaries);

            // for each nutrient ...
            return nutrients.map((key: any) =>
              new Summary(
                key,
                // ... calculate the sum over summaries
                summaries.reduce((prev, item) => prev + item[key], 0)
              )
            )
          }
        })
    })
  }

  /**
   * Use this to get Thesholds with Summary values included
   * Combines the two endpoints
   *
   * @param timeframe "for the summary: day | week | month"
   * @param date "for the summary"
   */
  getThresholdsCombined(timeframe = "day", date = new Date()): Promise<any> {
    // let the other functions deal with timeframe
    return Promise.all([
      this.getThresholdsRaw(),
      this.getSummary(timeframe, date)
    ]).then(resultArray => {
      let thresholds = resultArray[0];
      let summaries = resultArray[1];

      let result = [];
      // filter summaries and match via nutrient
      // there are only 6 thresholds but 24 summaries
      thresholds.nutritional_thresholds.map(t => {
        let summ = summaries.filter(s => s.nutrient == t.nutrient)[0];
        let value = summ ? summ.value : null;

        // multiply thresholds by 7 if it's a week
        // value from summary is already summed up
        switch (timeframe) {
          case "day": {
            result.push(new Threshold(t.nutrient, t.lower, t.upper, value, t.unit));
            break;
          }
          case "week": {
            result.push(new Threshold(t.nutrient, t.lower * 7, t.upper * 7, value, t.unit));
            break;
          }
          case "month": {
            let daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
            result.push(new Threshold(t.nutrient, t.lower * daysInMonth, t.upper * daysInMonth, value, t.unit));
            break;
          }
        }
      });
      console.log(result);
      return result;
    });
  }

  postUnstructuredActivity(activity){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.post(this.URL+'unstructured-activities', activity , this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }

  getGameInfos(game){
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http.get(this.URL + 'game-infos/' + game + '/' + this.globalvarProvider.profile.user_id, this.getAuthorizationHeader(token)).toPromise().then(response => response);
    });
  }

  deletePendingMessages(){
    return this.getAuthorizationTokenHeader().then((token)=>{
      return this.http.delete(this.URL + 'pending-messages/', this.getAuthorizationHeader(token)).toPromise().then(response => response);
    })
  }

  deleteFCMToken(){
    return this.getAuthorizationTokenHeader().then((token)=>{
      return this.http.delete(this.URL + 'FCMToken/', this.getAuthorizationHeader(token)).toPromise().then(response => response);
    })
  }

  getRooms(){
    return this.getAuthorizationTokenHeader().then((token)=>{
      return this.http.get(this.URL + 'rooms', this.getAuthorizationHeader(token)).toPromise().then(response => response);
    })
  }

  getAuthorizationHeader(token?){
    return {headers:{'Authorization':token?token:this.globalvarProvider.getId()}}
  }

  getAuthorizationHeaderWithLanguage(token?){
    let language = '';
    switch (this.globalvarProvider.profile.lang) {
      case 'en': language = 'eng'; break;
      case 'it': language = 'ita'; break;
      case 'es': language = 'esp'; break;
      case 'nl': language = 'nld'; break;
    }
    let c = this.getAuthorizationHeader(token);
    c.headers['Accept-Language'] = language;
    return c;
  }

  getAuthorizationTokenHeader(){
    /*return this.storage.get('userAuth').then(storage => {
        console.log(storage);
        return storage.access_token;
    });*/
    return new Promise(resolve => resolve('eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJWN0FXSW1qT2lRdi13dElwTjk2MWJfY004amgzZ21OblZtMHVNVVUwQldrIn0.eyJqdGkiOiI4ODk0NzFkNi05Mjc4LTQ4NTctOTBmZS05NGY0NzhkZDFlN2MiLCJleHAiOjE1OTA5MDg4MDAsIm5iZiI6MCwiaWF0IjoxNTg4MzE2ODAwLCJpc3MiOiJodHRwczovL2lhbS5uZXN0b3JlLWNvYWNoLmV1L2F1dGgvcmVhbG1zL25lc3RvcmUtY29hY2giLCJhdWQiOiJtb2JpbGV2MSIsInN1YiI6ImI4NzRiYTcxLTA3YTMtNDc1NS1hOWVmLWFlNThkNTBkMTgwZiIsInR5cCI6IkJlYXJlciIsImF6cCI6Im1vYmlsZXYxIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiMDRkOTZlYjktZjMzZC00ZDI4LTlkMDctNmM1Mjg4NTliYmI2IiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJuYW1lIjoiTGVvbmFyZCBBbmdlbGluIiwicHJlZmVycmVkX3VzZXJuYW1lIjoidDE5ODUiLCJnaXZlbl9uYW1lIjoiTGVvbmFyZCIsImxvY2FsZSI6Iml0IiwiZmFtaWx5X25hbWUiOiJBbmdlbGluIiwiZW1haWwiOiJsZW9uYXJkby5hbmdlbGluaUBoZXMtc28uY2gifQ.U2odC65qSpithzUELSV0CH4xFiAhBSbodZ-yxdpa_DD7tTvUdgZJpYeT0OSi2XU4xXLHq6pOM32QC1eRZgi47Tgux9wZG_07ni0YN75PobVSNnGu2y-JErypeAak76-63rmNrQMapCHhcmbu1lvpG8fXh8TphKPYzIn78Jso3Jm0EdyPKuCjsy6W7TKgHDilfO-yldhfNueUGCaF9sVPuD4zaeMeDwAKnRkspIyg8RIRHflrmwUWqWN-uYvkC4cvrs_nhyhZFGy290JgSUcAtR84QW5Vixv6DKp-yO7VXjM1P0U9TMmXJrseuvIJOwwx0TB75N0m3WXlBPC4e7zD7g'));

  }


}
