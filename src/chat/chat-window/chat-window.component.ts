import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {Message} from '../message/message.model';
import {Keyboard} from '@ionic-native/keyboard';
import {Camera} from '@ionic-native/camera';
import {GlobalvarProvider} from '../../app/globalprovider';
import {StatusBar} from '@ionic-native/status-bar';
import {APIService} from '../services/api';
import {Content, Platform} from 'ionic-angular';
import {MessageProvider} from '../services/message-provider';
import {NativeStorage} from '@ionic-native/native-storage';
import {Ng2ImgMaxService} from 'ng2-img-max';

@Component({
  selector: 'chat-window',
  templateUrl: './chat-window.component.html',
})
export class ChatWindowComponent implements OnInit {
  @ViewChild(Content) content: Content;
  @ViewChild('panelfooter') elementView: ElementRef;
  @ViewChild('container') containerEl: ElementRef;

  @Input() isOpen;
  @Output() update = new EventEmitter();

  panelfooterHeight = 0;
  messages;
  draftMessage: Message;
  hasimage = false;

  lastQuestionnaireMessageLoaded = false;
  lastQuestionnaireMessage: Message;
  selectedDate = null;
  choiceOpen = false;
  mealPickerVisible = false;

  fillQuestionnaire = new EventEmitter();
  clearMessages = new EventEmitter();

  lastQuestionnaires;
  options = [];

  constructor(
    public platform: Platform,
    protected messageProvider: MessageProvider,
              private ref: ChangeDetectorRef,
              public translate: TranslateService,
              public el: ElementRef,
              public keyboard: Keyboard,
              private api: APIService,
              private camera: Camera,
              private globalvarProvider:GlobalvarProvider,
              private statusBar: StatusBar,
              private nativeStorage: NativeStorage,
              private ng2ImageMax: Ng2ImgMaxService,
    ) {

    this.fillQuestionnaire.subscribe(()=>{
      this.lastQuestionnaireMessageLoaded = true;
      this.api.getQuestionnaire().then(res=>{
        this.messageProvider.addMessage(res, null);
      })
    });

    this.clearMessages.subscribe(()=>{
      this.messageProvider.keepLastMessage();
    });

    this.globalvarProvider.startQuestionnaireEvent.subscribe(()=>{
      this.startQuestionnaire();
    });

    this.translate.onLangChange.subscribe(()=>{
      this.generateOptions();
    })

  }

  ngOnChanges(change){
    console.log('changesss');
    if(change.isOpen.currentValue===true){
      this.scrollToBottom()
    }
  }


  ngAfterViewChecked() {
    this.ref.detectChanges();
  }

  ngOnInit(): void {

    //this.panelfooterHeight = this.elementView.nativeElement.offsetHeight;
    console.log('init chat');

    this.generateOptions();

    this.draftMessage = new Message();
    console.log('chatWindow');
    this.messageProvider.messagesChange
      .subscribe(
        (lastMessage : Message) => {
          console.log('lastMessage')
          if(lastMessage.bot && (lastMessage.mode === 'questionnaire' || lastMessage.mode==='questionnaireEnd' || lastMessage.mode === 'questionnaireWaiting')){
            this.lastQuestionnaireMessageLoaded = true;
            this.lastQuestionnaireMessage = lastMessage;

            if(lastMessage.mode === 'questionnaireEnd' || lastMessage.mode === 'questionnaireWaiting'){
              setTimeout(()=>{
                this.lastQuestionnaireMessage = null;
                this.lastQuestionnaireMessageLoaded = false
              }, 3000);
            }
            if(lastMessage.mode === 'questionnaireWaiting'){

            }
            if(lastMessage.mode === 'questionnaire'){
              //this.options = [];
            }
          }
          else if(lastMessage.bot && (lastMessage.mode === 'questionnaireStart')){
            if(!this.lastQuestionnaireMessageLoaded){
              this.lastQuestionnaireMessageLoaded = true;
              this.lastQuestionnaireMessage = lastMessage;
            }
          }
          setTimeout(() => {
            if(this.elementView && this.elementView.nativeElement) {
              this.panelfooterHeight = this.elementView.nativeElement.offsetHeight;
            }
            this.update.next();
            setTimeout(()=>{ this.scrollToBottom()},50);
          },50);
            setTimeout(() => {
            this.scrollToBottom();
          });
          console.log(lastMessage);
        });


      this.scrollToBottom();

    this.startQuestionnaire();
  }

  private startQuestionnaire(){
    console.log('start_questionnaire');
    console.log(this.translate);
    if(this.globalvarProvider.profile.questionnaire && this.globalvarProvider.profile.questionnaire.length>0){
      if(this.globalvarProvider.profile.questionnaire[0].question) {
        this.api.getQuestionnaire().then(res => {
          this.messageProvider.addMessage(res, null);
        })
      }
      else if(JSON.stringify(this.globalvarProvider.profile.questionnaire)!==this.lastQuestionnaires){
        let translatePipe = new TranslatePipe(this.translate, this.ref);
        this.messageProvider.addMessage({
          mode: "questionnaireStart",
          text: translatePipe.transform('questionnaire-agree'),
          choice: [{text: translatePipe.transform('yes'), intent: "yes"},{text: translatePipe.transform('no'), intent: "no"}]
        },null);
      }
      this.lastQuestionnaires = JSON.stringify(this.globalvarProvider.profile.questionnaire);
    }
  }

  onLoad(){
    //TODO: find a way to scroll down when keyboard is shown
    setTimeout(() => {
      this.scrollToBottom();
    },0);
  }

  onFocus(){
    //TODO: find a way to scroll down when keyboard is shown
    setTimeout(() => {
      this.scrollToBottom();
    },500);
  }

  onEnter(event: any): void {
    if (this.draftMessage.text || this.draftMessage.image) {
      this.sendMessage();
      event.preventDefault();
    }
  }

  sendMessage(): void {
    const m: Message = this.draftMessage;
    m.canWrite=true;
    m.author = this.globalvarProvider.getId();
    m.isRead = true;
    m.type= this.messageProvider.messages.length>0 ?this.messageProvider.messages[this.messageProvider.messages.length-1].type:null;
    this.messageProvider.newMessage(m);

    this.hasimage = false;
    this.draftMessage = new Message();
  }

  sendTextMessage(text):void {
    const m: Message = new Message();
    m.canWrite=true;
    m.text=text;
    m.author = this.globalvarProvider.getId();
    m.isRead = true;
    m.type= this.messageProvider.messages.length>0 ?this.messageProvider.messages[this.messageProvider.messages.length-1].type:null;
    this.messageProvider.newMessage(m);
    this.hasimage = false;
    this.draftMessage = new Message();
  }

  sendButton(text): void {
    console.log(text);
    this.choiceOpen=false;
    const m: Message = new Message();
    m.action = text;
    m.author = this.globalvarProvider.getId();
    m.isRead = true;
    this.messageProvider.newMessage(m);
    this.hasimage = false;
    this.draftMessage = new Message();
  }

  dateChange(date){
    console.log('date changed');
    console.log(date);
    this.draftMessage.text = date.month+'/'+date.day+"/"+date.year;
  }

  sendText(event, mode=null): void {
    this.choiceOpen=false;
    const m: Message = new Message();
    m.action = event.action;
    m.author = this.globalvarProvider.getId();
    m.isRead = true;
    m.text = event.text;
    m.mode = mode==null?null:'questionnaire';
    m.data = event.data;
    m.previous = event.previous;
    this.messageProvider.newMessage(m);
    this.hasimage = false;
    this.draftMessage = new Message();
  }

  readThis(inputValue: any): void {
    const file: File = inputValue.files[0];
    this.ng2ImageMax.resizeImage(file, 10000, 500).subscribe(
      result => {
        //let uploadedImage = new File([result], result.name);
        const myReader: FileReader = new FileReader();
        myReader.readAsDataURL(result);
        myReader.onload = (e) => {
          console.log(e);
          this.draftMessage.image = myReader.result;
          this.draftMessage.image = this.draftMessage.image.replace('data:application/octet-stream;base64', 'data:'+ file.type +';base64');
          this.draftMessage.imagedate = file.lastModified;
          this.globalvarProvider.chatPhoto = this.draftMessage.image;
          this.hasimage = true;
          this.ref.markForCheck();
          this.update.next();
        };
        myReader.onerror = (e)=>{
          console.log(e)
        }
      },error=>{
        console.log(error);
      });
  }

  handleInputChange(e) {
    console.log(e.target.lastModifiedDate);
    this.readThis(e.target);
    let self = this;
    self.hasimage=true;
    this.panelfooterHeight = this.elementView.nativeElement.offsetHeight;
  }

  scrollToBottom(): void {
    const scrollPane: any = this.el
      .nativeElement.querySelector('.msg-container-base');
    scrollPane.scrollTop = scrollPane.scrollHeight;
  }

  takePicture(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 700,
      targetHeight: 700
    }).then((imageData) => {
      // imageData is a base64 encoded string
      this.draftMessage.image = "data:image/jpeg;base64," + imageData;
      this.globalvarProvider.chatPhoto = this.draftMessage.image;
      this.hasimage = true;
      this.update.next();
      this.ref.markForCheck();
    }, (err) => {
      console.log(err);
    });
  }

  remove(){
    this.hasimage=false;
    this.draftMessage.image = null;
  }

  chatClose(){
    this.globalvarProvider.openChat(false);
    this.statusBar.overlaysWebView(false);
    this.statusBar.backgroundColorByHexString('#ffffff');
    this.statusBar.styleDefault();
  }

  sendProposedSentence(num, textKey){
    this.translate.get(textKey).subscribe(res=> {
      this.draftMessage.text = res;
      this.choiceOpen = false;
      if (num === 1) {
        this.draftMessage.action = 'quit_conversation';
      }
      if (num === 2) {
        this.draftMessage.action = 'howareyou';
      }
      if (num === 3) {
        this.draftMessage.action = 'ask_information';
      }
      this.sendMessage();
    });
  }

  sendOrRecord(){
    if (this.draftMessage.text || this.draftMessage.image) {
      this.sendMessage();
      event.preventDefault();
    }
    else {
      this.choiceOpen=!this.choiceOpen;
    }
  }

  record(){
      let options = {
        language: 'en-US',
        matches: 5,
      };
      /*this.speechRecognition.startListening(options)
        .subscribe(
          (matches: Array<string>) => {console.log(matches), this.platform.zone.run(()=>this.sendTextMessage(matches[0]))},
          (onerror) => console.log('error:', onerror)
        )*/
  }

  sendMeal(mealInfo){
    this.mealPickerVisible = false;
    this.draftMessage.text=mealInfo.meal;
    this.draftMessage.type='Food';
    this.sendOrRecord();
  }

  selectMeal(){
    this.mealPickerVisible=true;
  }

  validMeal(e){
    console.log(e);
    this.mealPickerVisible = false;
    if(e.meal instanceof Array){
      this.draftMessage.data={meals:e.meal.map(x=>x.id), type:e.type};
      this.draftMessage.text=e.meal.map(x=>x.text).join(', ');
    }
    else{
      this.draftMessage.data={meals:e.meal.id, type:e.type};
      this.draftMessage.text=e.meal.text;
    }

    this.draftMessage.action = 'selectedMeal';
    this.draftMessage.type='Food';
    this.sendOrRecord();
  }

  generateOptions(){
    let translatePipe = new TranslatePipe(this.translate, this.ref);
    this.options = [];
  }

  isSameDay(date1, date2){
      return date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
  }

}
