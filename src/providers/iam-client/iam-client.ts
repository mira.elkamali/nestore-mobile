import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { Storage } from '@ionic/storage';
import { ENVIRONMENT } from "../../environement.config";
import {HttpClient} from "@angular/common/http";


@Injectable()
export class IamClientProvider {

  public userAuth: any;
  public user: any;

  public keycloakConfig: any;

  private headerOptions: any = { "Content-Type": "application/x-www-form-urlencoded" };
  private headers = new Headers(this.headerOptions);
  private requestOptions = new RequestOptions({ headers: this.headers });

  constructor(public http: Http, public storage: Storage, public httpClient: HttpClient) {

    this.http.get('assets/keycloak.json').map(res => res.json()).subscribe(data => {
      this.keycloakConfig = data;
      this.keycloakConfig.realm = ENVIRONMENT.REALM;
    });

  }

  load(userCredentials) {

    //if (this.userAuth) {
    //  return Promise.resolve(this.userAuth);
    //}

    return new Promise((resolve, reject) => {

      if (userCredentials) {

        let data = {
          username: userCredentials.email,
          password: userCredentials.password,
          grant_type: this.keycloakConfig.grantType,
          client_id: this.keycloakConfig.clientId,
          // client_secret: this.keycloakConfig.clientSecret
        };

        this.http.post(
          this.getEndpoint('token'),
          this.prepareData(data),
          this.requestOptions
        ).map(res => res.json()).subscribe(authData => {

          this.storage.set('userAuth', authData);
          this.setAuthData(authData);

          resolve(this.userAuth);

        }, error => {
          reject(error);
        });

      }

    });

  }

  public checkAuth() {

    return new Promise(resolve => {

      this.storage.get('userAuth').then(storage => {

        if (storage) {

          this.setAuthData(storage);
          resolve(true);

        } else {

          resolve(false);

        }

      });

    });

  }

  public login(credentials) {

    return Observable.create(observer => {

      this.load({
        email: credentials.email,
        password: credentials.password
      }).then(() => {

        observer.next(true);
        observer.complete();

      }).catch(error => {
        observer.error(error);
      });

    });

  }

  public logout() {

    console.log('');
    this.storage.remove('userAuth');

    return Observable.create(observer => {

      let data = {
        client_id: this.keycloakConfig.clientId,
        // client_secret: this.keycloakConfig.clientSecret,
        refresh_token: this.userAuth.refresh_token
      };

      this.http.post(
        this.getEndpoint('logout'),
        this.prepareData(data),
        this.requestOptions
      ).subscribe(() => {

        this.setAuthData(null);
        this.storage.remove('userAuth');

        this.user = null;

        observer.next(true);
        observer.complete();

      }, error => {
        observer.error(error);
      });

    });

  }

  public getUser(refresh = false) {

    if (this.user && !refresh) {
      return Promise.resolve(this.user);
    }

    return new Promise((resolve, reject) => {

      let headers = new Headers();

      headers.append('Accept', 'application/json');
      headers.append('Authorization', `Bearer ${this.userAuth.access_token}`);

      let options = new RequestOptions({ headers: headers });

      this.httpClient.get(
        this.getEndpoint('userinfo'),
        {headers:{ 'Accept': 'application/json', 'Authorization': `Bearer ${this.userAuth.access_token}` }}

      ).map(res => {console.log(res);return res}).subscribe(userData => {

        this.user = userData;
        resolve(this.user);

      }, error => {
        reject('reload');
      });

    });

  }

  public updateToken() {

    return new Promise((resolve, reject) => {

      let data = {
        grant_type: 'refresh_token',
        client_id: this.keycloakConfig.clientId,
        // client_secret: this.keycloakConfig.clientSecret,
        refresh_token: this.userAuth.refresh_token
      };

      this.http.post(
        this.getEndpoint('token'),
        this.prepareData(data),
        this.requestOptions
      ).map(res => res.json()).subscribe(authData => {

        this.storage.set('userAuth', authData);
        this.storage.set('lastRefresh', this.getLastRefreshDate());

        this.setAuthData(authData);

        resolve(this.userAuth);


      }, error => {
        reject(error);
      });

    });

  }

  private setAuthData(data) {
    this.userAuth = data;

    if (data != null && !this.userAuth.expireDate) {
      this.userAuth.expireDate = this.getExpireDate(data.expires_in);
      this.userAuth.refreshExpireDate = this.getExpireDate(data.refresh_expires_in);
    }
  }

  private getExpireDate(timestampInSeconds) {

    let expire = new Date();

    expire.setTime(expire.getTime() + timestampInSeconds * 1000);

    return expire;

  }

  private getLastRefreshDate() {

    let refresh = new Date();

    refresh.setHours(0, 0, 0, 0);

    return refresh;

  }

  private getEndpoint(endpoint) {

    return `${this.keycloakConfig.url}/realms/${this.keycloakConfig.realm}/protocol/openid-connect/${endpoint}`;

  }

  private prepareData(data) {

    let formBody = [];

    for (let property in data) {

      let encodedKey = encodeURIComponent(property),
        encodedValue = encodeURIComponent(data[property]);

      formBody.push(`${encodedKey}=${encodedValue}`);

    }

    return formBody.join("&");

  }

  googleLoad(idToken) {
    if (this.userAuth) {
      return Promise.resolve(this.userAuth);
    }

    return new Promise((resolve, reject) => {

      if (idToken) {

        let data = {
          client_id: this.keycloakConfig.clientId,
          grant_type: "urn:ietf:params:oauth:grant-type:token-exchange",
          subject_token: idToken,
          subject_issuer: "google",
          subject_token_type: "urn:ietf:params:oauth:token-type:access_token",
          requested_token_type: "urn:ietf:params:oauth:token-type:refresh_token"
        };

        this.http.post(
          this.getEndpoint('token'),
          this.prepareData(data),
          this.requestOptions
        ).map(res => res.json()).subscribe(authData => {

          this.storage.set('userAuth', authData);
          this.setAuthData(authData);

          resolve(this.userAuth);

        }, error => {
          reject(error);
        });

      }

    });

  }

  public googleLogin(idToken) {
    return Observable.create(observer => {
      this.googleLoad(idToken).then(() => {
        observer.next(true);
        observer.complete();
      }).catch(error => {
        observer.error(error);
      });
    });
  }
}
