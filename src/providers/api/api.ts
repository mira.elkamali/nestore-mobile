import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { GlobalvarProvider } from "../../app/globalprovider";
import { DateFormatPipe } from "../helpers";
import { NutritionEvent } from "../../models/nutrition-event";
import { History } from "../../models/history";
import { IHistory, IThreshold } from "../../models/interfaces";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

// import { HistoryEvent } from "../../app/models/history-event";
import { Threshold } from "../../models/threshold";
import { Summary } from "../../models/summary";
import { ENVIRONMENT } from "../../environement.config";

import {lang} from 'moment';

@Injectable()
export class ApiProvider {
  private URL = ENVIRONMENT.COACH_SERVER;

  constructor(
    public http: HttpClient,
    private globalVars: GlobalvarProvider,
    private _dateFormatPipe: DateFormatPipe,
  ) {
  }

  initUser(firstname) {
    this.http
      .post(
        this.URL + "user",
        {
          id: this.globalVars.getId(),
          token: this.globalVars.getToken(),
          firstname: firstname,
          lang: this.globalVars.lang
        },
        this.getAuthorizationHeader()
      )
      .toPromise()
      .then(response => response);
  }

  getProfile(): Promise<any> {
    return this.http
      .get(this.URL + "profile", this.getAuthorizationHeader())
      .toPromise()
      .then(response => response);
  }

  send(message): Promise<any> {
    const obj = {};
    if (message.text) {
      obj["text"] = message.text;
    }
    if (message.image) {
      console.log(message.image);
      obj["image"] = message.image;
    }
    if (message.imagedate) {
      obj["imagedate"] = message.imagedate;
    }
    if (message.action) {
      obj["action"] = message.action;
    }
    if (message.type) {
      obj["type"] = message.type;
    }
    obj["id"] = this.globalVars.getId();
    obj["lang"] = this.globalVars.lang;
    obj["token"] = this.globalVars.getToken();
    return this.http
      .post(
        this.URL + "chatbot?lang=" + this.globalVars.lang,
        obj,
        this.getAuthorizationHeader()
      )
      .toPromise()
      .then(response => response)
      .catch(reason => reason);
  }

  foodList(): Promise<any> {
    return this.http
      .get(this.URL + "meals", this.getAuthorizationHeader())
      .toPromise()
      .then(response => response);
  }



  alternativeDishList(): Promise<any> {
    return this.http
      .get(
        this.URL + "alternative_dish/" + this.globalVars.getId(),
        this.getAuthorizationHeader()
      )
      .toPromise()
      .then(response => response);
  }


  // private getThresholds(): Promise<Threshold[]> {
  //   let endpoint = 'thresholds/';
  //   let userid = this.globalVars.getId();

  //   let query = this.URL + endpoint + userid;

  //   return this.http.get(query,
  //     // this.getAuthorizationHeader()
  //   ).pipe(
  //     map((data: IThreshold) =>
  //       data.nutritional_thresholds.map((item: any) =>
  //         new Threshold(
  //           item.nutrient,
  //           item.lower,
  //           item.upper
  //         )
  //       )
  //     )
  //   ).toPromise();
  // }


  getAuthorizationHeaderWithLanguage(){
    let language = '';
    switch (this.globalVars.profile.lang) {
      case "en": language = 'eng'; break;
      case "it": language = 'ita'; break;
      case "es": language = 'esp'; break;
      case "nl": language = 'nld'; break;
    }
    let c = this.getAuthorizationHeader();
    c.headers['Accept-Language'] = language;
    return c;
  }

  getAuthorizationHeader() {
    return { headers: { Authorization: this.globalVars.getId() } };
  }
}
