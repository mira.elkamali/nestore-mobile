import { Injectable } from "@angular/core";
import {ENVIRONMENT} from "../environement.config";
import {Subject} from "rxjs";

@Injectable()
export class WOTService {

  public WotMessage = new Subject<any>();

  ping() {
    if ((<any>window).plugins) {
      let WOTId = ENVIRONMENT.WOT_ID;
      let extras = {};
      extras[WOTId + '.extra.FROM'] = 'institute.humantech.nestore';
      (<any>window).plugins.intentShim.sendBroadcast({
          flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
          package: WOTId,
          action: WOTId + ".action.PING",
          extras: extras
        }, function (r) {
          console.log(r);
        },
        function () {

        });
    }

    if((<any>window).plugins){ // useful when we test without deploying
      (<any>window).plugins.intentShim.startActivity({
          //https://github.com/darryncampbell/plugin-intent-api-exerciser/blob/master/www/js/index.js#L120
          component:
            {
              "package": "com.neosperience.nestoreagent",
              "class": "com.neosperience.nestoreagent.WakeupWorkaroundActivity"
            }
        },
        function() {},
        function() {}
      );
    }
  }
}
