import {Injectable} from "@angular/core";
import {ENVIRONMENT} from "../environement.config";
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";

@Injectable()
export class  LogsService {
  private URL = ENVIRONMENT.COACH_SERVER;

  constructor(private http: HttpClient, private storage: Storage) {

  }

  postLogs(logs): Promise<any> {
    return this.getAuthorizationTokenHeader().then((token)=> {
      return this.http
        .post(this.URL + "logs", logs, this.getAuthorizationHeader(token))
        .toPromise()
        .then(response => response);
    });
  }

  start(resource){
    this.storage.get('logs_'+resource).then((res) => {
      if (!res) {
        res = [];
      }
      res.push({type: resource, start: new Date().toISOString()});
      this.storage.set('logs_'+resource, res);
    });
  }

  stopAndSend(resource){
    this.storage.get('logs_'+resource).then((res) => {
      if(!res){
        res = [];
      }
      if(res.length>0) {
        res[res.length - 1].stop = new Date().toISOString()
      }
      this.storage.set('logs_'+resource, res).then(()=>
        this.postLogs(res).then(()=>
          this.storage.remove('logs_'+resource)
        )
      )
    });
  }

  getAuthorizationHeader(token){
    return {headers:{'Authorization': token}}
  }

  getAuthorizationTokenHeader(){
    return this.storage.get('userAuth').then(storage => {
      console.log(storage);
      return storage.access_token;
    });
  }

}
