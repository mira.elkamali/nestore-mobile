import { Injectable } from "@angular/core";
import {Observable} from "rxjs";
import {LocalCircleInteractions} from "../../models/local-circle-interactions";
import {HttpClient} from "@angular/common/http";
import {SocialIntegrationLoneliness} from "../../models/social-integration-loneliness";
import {MemoryFailure} from "../../models/memory-failure";
import {Loneliness} from "../../models/loneliness";

@Injectable()
export class DataProvider {
  private mainFileFolderPath: string = 'assets/data/';
  private localCircleShortTermFile: string = 'short-term.json';
  private localCircleLongTermFile: string = 'long-term.json';
  private dailySocialIntegrationLonelinessFile: string = 'daily-social-integration-loneliness.json';

  constructor(private http: HttpClient) {

  }

  getLocalCircleWeekly(weekNumber: number): Observable<any> {
    /*return Observable.create(observer => {
      this.file.readAsText(this.mainFileFolderPath, this.shortTermFile).then(
        text => {
          observer.next(JSON.parse(text));
          observer.complete();
        });
    });*/
    //return this.http.get<LocalCircleInteractions>(this.mainFileFolderPath + this.localCircleShortTermFile);
    return Observable.of(this.getShortTerm());
  }


  getLocalCircleMonthly(month: number): Observable<any> {
    /*return Observable.create(observer => {
      this.file.readAsText(this.mainFileFolderPath, this.longTermFile).then(
        text => {
          if(text != null && text.length > 0) {
            observer.next(JSON.parse(text));
          }
          observer.complete();
        });
    });*/
    return Observable.of(this.getLongTerm());
  }

  getLocalCircleData(timeframe: string): Observable<LocalCircleInteractions> {
    if(timeframe == 'month') {
      return this.getLocalCircleMonthly(0);
    } else if(timeframe == 'week') {
      return this.getLocalCircleWeekly(0);
    }
    return null;
  }

  getDailySocialIntergration(){
    return {
      "number_of_interaction" : 5,
      "in_person" : 70,
      "other_type" : 30
    }
  }

  getDailySocialIntegrationLoneliness(): Observable<SocialIntegrationLoneliness> {
    /*return this.http.get<SocialIntegrationLoneliness>(this.mainFileFolderPath +
      this.dailySocialIntegrationLonelinessFile);*/
    return Observable.of(this.getDailySocialIntergration());
  }

  getWeekMemoryFailures(): MemoryFailure[][] {
    return [[new MemoryFailure(0,2), new MemoryFailure(3, 8), new MemoryFailure(6, 3)]];
  }

  getMonthMemoryFailures(): MemoryFailure[][] {
    return [
      [new MemoryFailure(0,2), new MemoryFailure(3, 8), new MemoryFailure(6, 3)],
      [new MemoryFailure(2,0), new MemoryFailure(5, 3)],
      [new MemoryFailure(1,0), new MemoryFailure(4, 1)],
      [new MemoryFailure(0,2), new MemoryFailure(3, 4), new MemoryFailure(6, 2)]];
  }

  getMemoryFailures(timeframe: string): Observable<MemoryFailure[][]> {
    if(timeframe == 'month') {
      return Observable.of(this.getMonthMemoryFailures());
    } else if(timeframe == 'week') {
      return Observable.of(this.getWeekMemoryFailures());
    }
    return null;
  }



  getWeekLoneliness(): Loneliness[][] {
    return [[{dayOfWeek: 0, value: 0}, {dayOfWeek: 1, value: 0}, {dayOfWeek: 2, value: 0}, {dayOfWeek: 3, value: 2},
      {dayOfWeek: 4, value: 0}, {dayOfWeek: 5, value: 1}, {dayOfWeek: 6, value: 0}]];
  }

  getMonthLoneliness(): Loneliness[][] {
    return [
      [{dayOfWeek: 0, value: 0}, {dayOfWeek: 1, value: 0}, {dayOfWeek: 2, value: 0}, {dayOfWeek: 3, value: 2},
        {dayOfWeek: 4, value: 0}, {dayOfWeek: 5, value: 1}, {dayOfWeek: 6, value: 1}],
      [{dayOfWeek: 0, value: 0}, {dayOfWeek: 1, value: 0}, {dayOfWeek: 2, value: 0}, {dayOfWeek: 3, value: 2},
        {dayOfWeek: 4, value: 0}, {dayOfWeek: 5, value: 1}, {dayOfWeek: 6, value: 1}],
      [{dayOfWeek: 0, value: 1}, {dayOfWeek: 1, value: 1}, {dayOfWeek: 2, value: 0}, {dayOfWeek: 3, value: 0},
        {dayOfWeek: 4, value: 0}, {dayOfWeek: 5, value: 0}, {dayOfWeek: 6, value: 1}],
      [{dayOfWeek: 0, value: 0}, {dayOfWeek: 1, value: 0}, {dayOfWeek: 2, value: 0}, {dayOfWeek: 3, value: 0},
        {dayOfWeek: 4, value: 0}, {dayOfWeek: 5, value: 2}, {dayOfWeek: 6, value: 1}]
    ];
  }

  getLongTerm(){
    return {
      id: "UVWX",
      startDate: "2019-03-01",
      endDate: "2019-03-31",
      totalInteractionsNumber: 100,
      totalInteractionsDuration: 35000,
      localCircleMembers: [
      {
        user: "Joe",
        interactionsNumber: 40,
        interactionsProportion: 0.55
      },
      {
        user: "Mark",
        interactionsNumber: 20,
        interactionsProportion: 0.30
      },
      {
        user: "Tom",
        interactionsNumber: 15,
        interactionsProportion: 0.15
      }
    ],
      localCircleInteractionsNumber: 75,
      localCircleInteractionsDuration: 21500,
      periods: [
      {
        startDate: "2019-02-25",
        endDate: "2019-03-03",
        totalInteractionsNumber: 22,
        totalInteractionsDuration: 5200
      },
      {
        startDate: "2019-03-04",
        endDate: "2019-03-10",
        totalInteractionsNumber: 20,
        totalInteractionsDuration: 4600
      },
      {
        startDate: "2019-03-11",
        endDate: "2019-03-17",
        totalInteractionsNumber: 16,
        totalInteractionsDuration: 5600
      },
      {
        startDate: "2019-03-18",
        endDate: "2019-03-24",
        totalInteractionsNumber: 18,
        totalInteractionsDuration: 6600
      },
      {
        startDate: "2019-03-25",
        endDate: "2019-03-31",
        totalInteractionsNumber: 19,
        totalInteractionsDuration: 7600
      }
    ]
    }
  }

  getShortTerm(){
    return {
      id: "ABCD",
      startDate: "2019-03-04",
      endDate: "2019-03-10",
      totalInteractionsNumber: 25,
      totalInteractionsDuration: 5000,
      localCircleMembers: [
      {
        user: "Joe",
        interactionsNumber: 11,
        interactionsProportion: 0.5
      },
      {
        user: "Mark",
        interactionsNumber: 6,
        interactionsProportion: 0.25
      },
      {
        user: "Tom",
        interactionsNumber: 3,
        interactionsProportion: 0.25
      }
    ],
      localCircleInteractionsNumber: 20,
      localCircleInteractionsDuration: 4800
    }
  }

  getLoneliness(timeframe: string): Observable<MemoryFailure[][]> {
    if(timeframe == 'month') {
      return Observable.of(this.getMonthLoneliness());
    } else if(timeframe == 'week') {
      return Observable.of(this.getWeekLoneliness());
    }
    return null;
  }

}
