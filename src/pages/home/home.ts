import {Component, OnInit} from '@angular/core';
import {GlobalvarProvider} from '../../app/globalprovider';
import {LocalNotifications} from '@ionic-native/local-notifications';
import {TranslateService} from '@ngx-translate/core';
import {StatusBar} from '@ionic-native/status-bar';
import {APIService} from '../../chat/services/api';
import {Storage} from '@ionic/storage';
import {Subject} from "rxjs";
import {MessageProvider} from "../../chat/services/message-provider";
import {EmailComposer} from "@ionic-native/email-composer";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  visible=false;
  info;
  lang;
  display;

  process;
  activitiesStatus;

  tuto = 0;
  bottom = 0;
  top=0;

  openActivitySubject = new Subject();

  constructor(
              private localNotifications: LocalNotifications,
              private storage: Storage,
              public api: APIService,
              public globalvarProvider: GlobalvarProvider,
              public messageProvider: MessageProvider,
              public emailComposer: EmailComposer,
              ) {

    this.openActivitySubject.subscribe(()=>{
      globalvarProvider.isActivityOpen=true;
    })

  }

ngOnInit() {
    console.log(this.globalvarProvider.profile.pathways)
  this.initProgress();

}

nextStep(){
    if(this.tuto) {
      let d = document.getElementById('home');
      d.children[1].remove();
      this.storage.set('tutorial_home',false);
      this.globalvarProvider.tuto = false;
    }
  if(this.tuto===1) {
    this.clickwelcome2()
  }
  else if(this.tuto==2){
    this.tuto=0;
    this.storage.set('tutorial_home',false);
  }
}

clickwelcome(){
    this.tuto=1;
    let c = document.getElementById('activity_button');

    let cln = <HTMLElement>c.cloneNode(true);

    let values = c.getBoundingClientRect();

  let text = document.getElementById('text');
  let values2 = text.getBoundingClientRect();



  let h =  ()=> {
      this.globalvarProvider.isActivityOpen=true
    };
     let d = document.getElementById('home');
    values2 = d.getBoundingClientRect();


     cln.style.position = 'absolute';
     console.log(values);
     console.log(values2);
     this.bottom = values2.height-values.top;
     console.log(this.bottom);
     cln.style.top = values.top+"px";
     cln.style.height = values.height+"px";
     //cln.addEventListener('click', h);
    d.appendChild(cln)

}

  clickwelcome2(){
    if(!this.globalvarProvider.tuto)
      return;
    this.tuto=2;
    let c = document.getElementById('progress');

    let values3 = c.getElementsByClassName('tutoIcon')[0].getBoundingClientRect();
    console.log(values3);

    let cln = <HTMLElement>c.cloneNode(true);

    let values = c.getBoundingClientRect();

    let d = document.getElementById('home');



    let h =  ()=> {
      this.globalvarProvider.isActivityOpen=true
    };
    cln.style.position = 'absolute';
    this.top = values3.top+30;
    cln.style.top = values.top+"px";
    cln.style.height = values.height+"px";
    //cln.addEventListener('click', h);
    d.appendChild(cln)
  }

removeTuto(){
  let d = document.getElementById('home');
  this.tuto = 0;
  this.storage.set('tutorial_home',false);
  this.globalvarProvider.tuto = false;
}

initProgress(){
    this.updateProgress(this.globalvarProvider.profile.progress)
}

updateProgress(res){
  this.process = res;
  this.activitiesStatus = Array.from(Array(res.total), (d, i) => false);
  for(let i = 0; i<res.score; i++){
    this.activitiesStatus[i] = true;
  }
}


  openChat(){
    this.globalvarProvider.openChat(true);
    this.localNotifications.cancelAll();
  }

  validd(d){
    this.info=JSON.stringify(d);
  }

  startiQuestionnaire(questionnaireId){
    this.api.startQuestionnaire(questionnaireId).then().catch();
  }

  startQuestionnaire(questionnaireId) {
    let name = '';
    if(questionnaireId === 'SOC-LO') {
      name = 'de_jong_gierveld_loneliness_scales';
    }
    else if (questionnaireId==='SOC-SI'){
      name = 'lubben_social_network_scale'
    }
    else if(questionnaireId==='CFQ'){
      name = 'cognitive_failure_questionnaire'
    }
    if (this.globalvarProvider.profile.progress.tasks[name] < 1) {
      this.globalvarProvider.currentQuestionnaire = questionnaireId;
      this.api.startQuestionnaire(questionnaireId).then((message) => {
        this.globalvarProvider.message = message;
        this.globalvarProvider.message.bot = true;
      }).catch(()=>{
        this.globalvarProvider.error = true;
      })
    }
  }

  selectPathway(pathway, event){
    let domainName2 = '';
    switch (pathway['domain-name']) {
      case 'PHY' : domainName2='physical';break;
      case 'NUT' : domainName2= 'nutritional';break;
      case 'SOC' : domainName2= 'social';break;
      case 'COG' : domainName2= 'cognitive';break;
    }
    pathway['domain-fullname'] = domainName2;
    console.log(pathway);
    this.globalvarProvider.selectedPathway=pathway;
    event.stopPropagation();
  }

  sendEmail(test){
    this.storage.get(test).then((res) => {
      if(res) {
        res.map((x)=>{
          x.user_code = this.globalvarProvider.profile.user_id;
          delete x.sequence_id;
          delete x.sent;
          x.timestamp = new Date().toISOString();
          if(!x.time) {
            x.time = "0";
          }
        });
      }
      else{
        res = [];
      }
      if(test=='updating'){
        test = 'numerical_updating_task';
      }

      let email = {
        to: 'api@nestore-coach.eu',
        subject: test+', ' + this.globalvarProvider.profile.user_id,
        attachments: ['base64:'+test+'_'+this.globalvarProvider.profile.user_id+'.json//'+window.btoa(JSON.stringify(res))],
        isHtml: false
      };

      this.emailComposer.open(email);
    });
  }

  timer;
  down(gameId){
    this.timer = setTimeout(()=>{
      this.sendEmail(gameId)
    },3000)
  }

  up(){
    console.log('up')
    clearTimeout(this.timer)
  }

}
