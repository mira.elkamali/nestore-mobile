import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';

import { SubmitDishPage } from '../submit-dish/submit-dish';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from '../../providers/api/api';
import { NutritionSelectPage } from '../nutrition-select/nutrition-select';
import { NativeStorage } from '@ionic-native/native-storage';
import {APIService} from '../../chat/services/api';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Subject} from "rxjs";


@Component({
  selector: 'water-intake',
  templateUrl: 'water-intake.html',
})
export class WaterIntakePage {
  @Output() onClose = new EventEmitter();
  @Output() canBack = new EventEmitter();
  glassQuantity = 200;
  clicked = false;
  glasses = [false, false, false, false, false, false, false, false];
  initQuantity = 0;
  quantity = 0;
  loaded = false;
  lastIntakeDate=null;

  achieved = false;

  selectFirst = new Subject();
  selectSecond = new Subject();
  save = new Subject();

  constructor(private api: APIService, private globalProvider: GlobalvarProvider) {
    this.load(true);

    this.selectFirst.subscribe((fromTuto)=>{
      if(fromTuto){
        this.glasses[0] = true;
      }
      else {
        this.glasses[0] = !this.glasses[0];
      }
      this.computeScore()
    });

    this.selectSecond.subscribe((fromTuto)=>{
      if(fromTuto){
        this.glasses[1] = true;
      }
      else {
        this.glasses[1] = !this.glasses[1];
      }
      this.computeScore()
    });

    this.save.subscribe(()=>{
      this.loaded = false;
      this.api.postWaterIntake(new Date(), this.quantity-this.initQuantity).then(res => {
        this.load();
        this.displayAchievedGoalScreenIfNecessary();
      }).catch(()=> {
          this.globalProvider.internalError = true;
          this.loaded = true;
        }
      )
    })
  }

  private displayAchievedGoalScreenIfNecessary(){
    console.log(this.glasses.filter(x=>x).length);
    if(this.glasses.filter(x=>x).length === 8 ){
      this.achieved = true;
    }
    setTimeout(()=>{
      this.achieved = false
    },1500)
  }

  private load(first=false){
    this.loaded = false;
    this.api.getWaterIntake(new Date(),'day').then(res=>
      {
        this.loaded = true;
        this.setGlasses(res);
        if(first){
          this.displayAchievedGoalScreenIfNecessary();
        }
      }
    );
  }

  private setGlasses(intakes){
    let quantity=0;
    for(let intake of intakes){
      quantity+=intake.quantity;
    }
    this.initQuantity = quantity;
    this.globalProvider.waterUpdate.next(this.initQuantity);
    for(let i=0;i<this.glasses.length;i++){
      this.glasses[i]=i<this.initQuantity/this.glassQuantity;
    }
    this.setLastHour(intakes);
    this.computeScore();
  }

  close() {
    this.onClose.emit()
  }

  computeScore(){
    this.quantity = this.glasses.filter(x=>x).length * this.glassQuantity;
  }

  private setLastHour(intakes){
    if(intakes.length>0) {
      this.lastIntakeDate = intakes[intakes.length - 1].date.split(' ')[1];
    }
  }

}
