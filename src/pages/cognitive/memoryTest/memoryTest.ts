import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {CLimits} from './CLimits';
import {Datas} from './Datas';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {APIService} from '../../../chat/services/api';
import {GlobalvarProvider} from "../../../app/globalprovider";
import { Insomnia } from '@ionic-native/insomnia';
import {Storage} from "@ionic/storage";

@Component({
  selector: 'memoryTest',
  templateUrl: 'memoryTest.html',
})
export class MemoryTest {

  @Input() options;
  @Output() finish = new EventEmitter();

  readonly DIFFICULTIES = [
    {level:1, initialInterval:6000, operationsInterval: 3500,outISI:500, inISI:500},
    {level:2, initialInterval:4000, operationsInterval: 1250,outISI:500, inISI:250}];

  readonly minSessionTime = 900000; // time in milliseconds // (15 minutes)

  private difficulty;
  private loaded = false;
  private smallLoaded = true;
  private nbTiles = 3;
  private tiles;
  private mathOpNum = 8;
  private minNum = 0;
  private maxNum = 9;
  private minMathOp = -8;
  private maxMathOp = 8;
  private limits : CLimits;
  private tuto = false;
  private trial = false;
  private finalScore;
  private trialStep = 1;
  private help = true;
  //private totalTime;

  private displayMathOperIndex = 0;//which math operation to display, [0,btnTiles)
  data : Datas;

  displayedValues = [];
  chooseAns = null;
  allAns = false;
  currentAns = null;
  enableKeyboard = false;
  done = false;
  mode;
  scoreText;

  gameState : any;
  translatePipe;

  startTime;
  endTime;
  quitConfirmation = false;
  isSessionFinish = false;


  ngOnChanges(change){
    if(change.options && this.options) {

      if (this.options !== 'error') {
        this.restart();
      }
      this.loaded = true;
    }
  }

  constructor(public api: APIService, public globalvarProvider: GlobalvarProvider, public alertController: AlertController, public translate: TranslateService, private cdRef:ChangeDetectorRef, private insomnia: Insomnia,private storage: Storage){
    this.translatePipe = new TranslatePipe(this.translate,this.cdRef);

    this.globalvarProvider.back.subscribe(()=>{
      if(this.options.mode === 'intervention') {
        if (!this.isSessionFinished()) {
          this.quitConfirmation = true
        } else {
          this.close();
        }
      }
    });
  }

  ngOnInit(){
  this.insomnia.keepAwake().then(
      () => console.log('keep awake success'),
      () => console.log('keep awake error')
    );
  }

  async presentAlert() {
    let pressText = this.translatePipe.transform('press_ok_to_start');
    let okText = this.translatePipe.transform('ok');

    const alert = await this.alertController.create({
      message: pressText,
      enableBackdropDismiss: false,

      buttons: [{
        text: okText, handler: (blah) => {
          this.start();
        }
      }],
    });
    await alert.present();

  }

  start(){
    this.startTime = new Date();
    this.displayedValues = this.data.getInitValues();
    setTimeout(() => {
      this.runISIdelay(this)
    }, this.difficulty.initialInterval);
  }

  doOperation(){
    setTimeout(() => {
      this.runMathOperation(this)
    }, (this.displayMathOperIndex == 0) || (this.displayMathOperIndex >= this.mathOpNum) ? this.difficulty.outISI : this.difficulty.inISI);
  }

  async scoreAlert() {

    let okText =await this.translate.get('ok').toPromise().then((res)=> {return res});
    let foundText = await this.translate.get('you_found').toPromise().then((res)=> {return res});

    const alert = await this.alertController.create({
      title: foundText,
      message: this.data.getSuccessRateString(),
      enableBackdropDismiss: false, // <- Here! :)
      buttons: [{text:okText, handler: (blah) => {
          this.finish.emit();
        }}],
    });

    await alert.present();
  }

  runISIdelay(self, isLast=false){
    self.clearAllValues();
    if(!this.tuto || isLast) {
      this.doOperation();
    }
    else if(!isLast){
      this.help=true;
    }
  }

  runMathOperation(self){
    if (this.displayMathOperIndex < this.mathOpNum) {
      //display the math operation
      self.displayedValues[this.data.getMathOpBtn(this.displayMathOperIndex)]= this.data.getMathOperationString(this.displayMathOperIndex);
      let isLast = this.displayMathOperIndex+1===this.mathOpNum;
      setTimeout(()=>{self.runISIdelay(self,isLast)}, this.difficulty.operationsInterval);
    }
    else if (this.displayMathOperIndex >= this.mathOpNum){
      this.chooseAns = 0;
      this.enableAns();
      if(this.tuto) {
        this.help = true;
      }
      //get answers --> in order...
      //setAllButtonText(getString(R.string.strTouchAnswer),numSize); //changed to just a big '?'
      //disable buttons to use isEnabled later

    }

    this.displayMathOperIndex++;
  }

  clearAllValues() : void{
    for(let i = 0; i<this.displayedValues.length; i++){
      this.displayedValues[i] = "";
    }
  }

  selectAns(num){
    this.currentAns = num;
    if(!this.done && this.chooseAns!=null && this.chooseAns>=num){
      this.enableKeyboard = true;
    }
  }

  giveAns(num){
    if(this.tuto && this.displayedValues[this.currentAns]==='?') {
      this.help = true;
    }

    this.data.storeAns(num, this.currentAns);


    this.displayedValues[this.currentAns] = ""+num;
    if(this.currentAns==this.chooseAns) {
      this.chooseAns++;
    }
    this.currentAns = null;
    this.enableAns();
    this.enableKeyboard = false;
    this.allAns = this.data.checkAllAns();
  }

  enableAns(){
    this.mode = 'select';
    this.displayedValues[this.chooseAns] = '?'
  }

  displayAnswers(){
    this.help=true;
    if(this.data.getScore()===this.nbTiles){
      this.finalScore=true;
    }
    this.done = true;
    this.enableKeyboard = false;
    for(let i = 0; i < this.nbTiles;i++) {
      this.displayedValues[i] = this.data.getPresentationString(i);
    }

    //this.totalTime = this.convertMillisecondsToDigitalClock(new Date().getTime()-this.options.sessionStartDate.getTime()).clock;


    this.gameState = {};
    this.gameState.task_name = 'NU';
    this.gameState.task_version =  this.options['task-version'];
    this.gameState.difficulty_version = this.options['difficulty-version'];
    this.gameState.session_id = 0;
    this.gameState.sequence_id = this.options['sequence-id'];
    this.gameState.module_name = this.options['module-name'];
    this.gameState.time = (new Date().getTime()-this.startTime.getTime())/1000;

    if(this.options.mode === 'intervention' && this.isSessionFinished()) {
      this.isSessionFinish = true;
      this.gameState.taskId = this.options.taskId;
    }

    this.gameState.cells = [];
    if(this.tuto){
      this.gameState.tuto = true;
    }
    if(this.trial){
      this.gameState.trial = true;
    }
    this.scoreText = this.data.getSuccessRateString();

    let points = 0;
    for(let i = 0; i < this.nbTiles; i++){
      if(this.data.correctAns[i] === this.data.userAns[i]){
        points++;
      }
    }

    this.gameState.score = (points/this.nbTiles) * 100;

    for(let i = 0; i < this.nbTiles; i++){
      this.gameState.cells[i] = {block_number: i+1,cell:i+1, correct_response: this.data.correctAns[i], given_response: this.data.userAns[i], points: this.data.correctAns[i]===this.data.userAns[i]?1:0, outcome:'FINISHED'};
    }

    if(!this.trial && !this.tuto){
      this.smallLoaded = false;
      this.api.postGame(this.gameState).then(() => {
        this.gameState.sent = true;
        this.storeLocal(this.gameState);
        this.api.initUser().then((res)=>{
          this.globalvarProvider.profile = res;
          this.smallLoaded = true;
        });
      }).catch(()=> {
          this.storeLocal(this.gameState);
          this.globalvarProvider.internalError = true;
          this.smallLoaded = true;
        }
      );



    }
  }


  trialNext(){
    this.trialStep++;
    if(this.trialStep==2 && (this.trial || !this.tuto)){
      this.help = false;
      this.start();
    }
    if(this.trialStep==3 && (!this.trial && !this.tuto)){
      this.help = false;
      this.quit(false);
    }
    if(this.trialStep==3){
      this.help = false;
      this.start();
    }
    if(this.trialStep==4){
      this.help = false;
      this.doOperation();
    }
    if(this.trialStep==5){
      this.help = false;
      this.doOperation();
    }
    if(this.trialStep==6){
      this.help = false;
      this.doOperation();
    }
    if(this.trialStep==7){
      this.help = false;
      this.doOperation();
    }
    if(this.trialStep==8){
      this.help = false;
    }
    if(this.trialStep==9){
      this.help = false;
    }
    if(this.trialStep==10){
      this.help = false;
    }
    if(this.trialStep==11){
      this.help = false;
      this.finish.emit();
    }
  }

  quit(save){
    if(save){
      this.smallLoaded = false;
      this.api.postGame(this.gameState).then(()=>{
        this.finish.emit();
        this.smallLoaded = true;
      }).catch(()=> {
        this.globalvarProvider.internalError = true;
        this.smallLoaded = true;
      });
    }
    else{
      if(this.options.mode === 'intervention'){
        this.restart();
      }
      else {
        this.finish.emit(false);
      }
    }
  }

  close(){
    this.finish.emit(false);
  }

  convertMillisecondsToDigitalClock(ms) {
    let hours = Math.floor(ms / 3600000), // 1 Hour = 36000 Milliseconds
      minutes = Math.floor((ms % 3600000) / 60000), // 1 Minutes = 60000 Milliseconds
      seconds = Math.floor(((ms % 360000) % 60000) / 1000) // 1 Second = 1000 Milliseconds
    return {
      hours : hours,
      minutes : minutes,
      seconds : seconds,
      clock : Math.max(15 - minutes)
    };
  }

  restart(){
    this.done = false;
    this.trialStep = 1;
    this.nbTiles = this.options['task-version'];
    this.difficulty=this.DIFFICULTIES.find(x=>x.level===this.options['difficulty-version']);
    this.tuto = this.options.tuto===true;
    this.trial = this.options.trial===true;
    this.tiles = Array.apply(null, {length: this.nbTiles}).map(Number.call, Number);
    this.mathOpNum = this.nbTiles * 2;
    this.limits = new CLimits(this.minNum, this.maxNum, this.minMathOp, this.maxMathOp);
    this.data = new Datas(this.nbTiles, this.mathOpNum, this.limits);
    this.help = true;
    this.displayedValues = [];
    this.chooseAns = null;
    this.allAns = false;
    this.currentAns = null;
    this.displayMathOperIndex = 0;
  }


ngOnDestroy(){
    this.insomnia.allowSleepAgain().then(
      () => console.log('allow sleep again success'),
      () => console.log('allow sleep again error')
    );
  }

  private isSessionFinished(){
    return (new Date().getTime() - this.options.sessionStartDate.getTime()) > this.minSessionTime;
  }

  private storeLocal(gameState){
    this.storage.get('updating').then((res) => {
      if (!res) {
        res = [];
      }
      res.push(gameState);
      this.storage.set('updating', res);
    });
  }

}
