import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';
import {NBackGenerator} from './NBackList';
import {APIService} from '../../../chat/services/api';
import {CLimits} from '../memoryTest/CLimits';
import {Datas} from '../memoryTest/Datas';
import {GlobalvarProvider} from "../../../app/globalprovider";
import {Insomnia} from "@ionic-native/insomnia";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'nback-game',
  templateUrl: 'nback.html',
})
export class NBackGameComponent {

  help=true;
  trialStep=1;
  tuto=false;
  trial=false;
  @Input() options;
  back = 2;
  @Output() finish = new EventEmitter();
  private nback;
  private randomList;
  private currentPosition = null;
  private hideNumber = false;
  private canAnswer = false;
  private points = 0;
  private initialInterval = 2500; //milliseconds
  private state = [];
  private loaded;
  private smallLoaded = true;



  constructor(public api: APIService, public globalvarProvider: GlobalvarProvider, public alertController: AlertController, public translate: TranslateService, private insomnia: Insomnia,private storage: Storage){
    this.nback = new NBackGenerator(this.back);
    this.randomList = this.nback.getList();
  }

  ngOnInit(){
    this.insomnia.keepAwake().then(
      () => console.log('keep awake success'),
      () => console.log('keep awake error')
    );
  }


  ngOnChanges(change){
    if(change.options && this.options) {
      if (this.options !== 'error') {
        this.back = this.options['task-version']
      }
      this.loaded = true;
    }
  }

  start(){
    setTimeout(() => {
      this.hide()
    }, this.initialInterval);
  }


  save() {

    let gameState : any = {};
    gameState['task_name'] = 'N-back';
    gameState['task_version'] = this.back+'-back';
    gameState['replies'] = this.state;
    gameState['score'] = this.points;
    gameState['sequence_id'] = this.options['sequence-id'];
    gameState['module_name'] = this.options['module-name'];
    gameState['trial']= this.options.trial;

    this.smallLoaded = false;

    if(!gameState.trial) {
      this.storeLocal(gameState);
    }

    return this.api.postGame(gameState).then(() => {
      this.api.initUser().then((res)=>{
        this.globalvarProvider.profile = res;
        this.smallLoaded = true;
      });
    }).catch(()=> {
        this.globalvarProvider.internalError = true;
        this.smallLoaded = true;
      }
    );

  }

  runISIdelay() {
    if(this.canAnswer){
      this.giveAnswer(null)
    }
    if(this.currentPosition===null){
      this.currentPosition = 0;
    }
    else{
      this.currentPosition++;
    }
    this.hideNumber = false;
    this.canAnswer = true;
    setTimeout(() => {
      this.hide()
    }, 500);
  }

  hide() {
    this.hideNumber = true;
    if(this.currentPosition===this.randomList.length-1){
      setTimeout(() => {
        this.help=true;
        if(!this.options.trial) {
          this.save().then();
        }
      },this.initialInterval);
    }
    else {
      setTimeout(() => {
        this.runISIdelay()
      }, this.initialInterval);
    }
  }

  giveAnswer(b){
    if(this.canAnswer) {
      let correctResponse = this.randomList[this.currentPosition] === this.randomList[this.currentPosition-this.back];
      let hasPoint = correctResponse === b;
      if (hasPoint) {
        this.points++
      }
      this.state.push({'givenResponse' : b === null ? 0 : +b ,'correctResponse' : +correctResponse , 'points' : +hasPoint, 'outcome': 'FINISHED', 'stimulusType':this.nback.getLure(this.currentPosition)});
    }
    this.canAnswer = false;


  }

  isCorrectAnswer(b){
    return (this.randomList[this.currentPosition] === this.randomList[this.currentPosition-this.back]) === b;
  }

  trialNext() {
    this.trialStep++;
    if (this.trialStep == 2 && (this.trial || !this.tuto)) {
      this.help = false;
      this.start();
    }
    if(this.trialStep==3 && (!this.trial && !this.tuto)){
      this.help = false;
      this.finish.emit();
    }
  }

  quit(save){
    if(save){
     this.save().then(()=>{
        this.finish.emit();
      });
    }
    else{
      this.finish.emit();

    }
  }

  private storeLocal(gameState){
    this.storage.get('nback').then((res) => {
      if (!res) {
        res = [];
      }
      res.push(gameState);
      this.storage.set('nback', res);
    });
  }

  ngOnDestroy(){
    this.insomnia.allowSleepAgain().then(
      () => console.log('allow sleep again success'),
      () => console.log('allow sleep again error')
    );
  }



}
