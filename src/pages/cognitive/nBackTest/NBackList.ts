export class NBackGenerator {
  private L;
  private T = 12;
  private list_lures_steps;
  private R = 3;
  private b;
  private values_number;
  private values_code;

  constructor(nbBack: number = 2) {
    this.b = nbBack;
    if (this.b === 2) {
      this.list_lures_steps = [1, 3, 4, 5];
    } else if (this.b === 3) {
      this.list_lures_steps = [2, 4, 5, 6];
    }
    this.L = this.R * this.T + this.b;

    this.values_number = this.obtainSequence();
    console.log(this.values_number);
  }

  public getList(){
    return this.values_number;
  }

  public getLure(pos){
    return this.values_code[pos];
  }

  private updateScore(values_score,values_code,lure_step,lure_pos){
    /* python: values_score[lure_pos] = lure_step + values_score[lure_pos - lure_step]*/
    values_score[lure_pos] = lure_step + values_score[lure_pos + lure_step];

    /* python: for i in range(lure_pos + 1, L):*/
    for (let i = lure_pos + 1; i < this.L; i++) {
      if(values_score[i] === 'T'){
        values_score[i] = values_score[i - this.b] + this.b;
      }
      else if(values_code[i][0] == 'L'){
        let lure_step_ = parseInt(values_code[i][1]);
        values_score[i] = lure_step_ + values_score[i - lure_step_]
      }
    }

    return values_score

  }

  private existsViolation(values_score,values_code){
    /* python: 	for i in range(b, L):*/
    for (let i = this.b; i < this.L; i++) {
      /* python: 	if values_code[i] not in ['T', 'R'] and values_score[i - b] + b == values_score[i]:*/
      if(values_code[i]!=='T' && values_code[i]!=='R' && values_score[i-this.b]+this.b === values_score[i]){

        /* python: return True*/
        return true;
      }
    }
    /* python: return False*/
    return false;
  }

  private positionsLeadingToViolation(values_score, values_code, lure_step) {
    /* python: 	violations = []*/
    let violations = [];

    /* python: 	for i in range(lure_step, L):*/
    for (let i = lure_step; i < this.L; i++) {

      /* python: if values_score[i] == 0:  # is position available*/
      if (values_score[i] === 0) {

        /* python: values_score_ = update_score(values_score=values_score[:], values_code=values_code[:], lure_step=lure_step, lure_pos=i)*/
        let values_score_ = this.updateScore([...values_score], [...values_code], lure_step, i);

        /* python: values_code_ = values_code[:]*/
        let values_code_ = [...values_code];

        /* python: values_code_[i] = 'L' + str(lure_step)*/
        values_code_[i] = 'L' + lure_step;

        /* python: if exists_violation(values_score_, values_code_):*/
        if(this.existsViolation(values_score_, values_code_)){

          /* python: violations += [i]*/
          violations.push(i)
        }

      }
    }
    /* python: return violations*/
    return violations;
  }

  private obtainSequence(){
    let success = false;
    let values_number;
    while(!success){
      this.values_code = this.assignPosition();
      if(this.values_code){
        success = true;
        values_number = this.assignNumbers(this.values_code)
      }
    }
    return values_number
  }

  private assignPosition() {

    /*  python: list_target = random.sample(range(b, L), T)*/
    let list_target = Array.from({length: this.L - this.b}, (v, k) => k + this.b);
    let shuffled = list_target.sort(function () {
      return .5 - Math.random()
    });
    list_target = shuffled.slice(0, this.T);

    /* python: list_lures = [[]] * len(list_lures_steps)*/
    let list_lures = Array.from({length: this.list_lures_steps.length}, () => []);

    /* python: 	positions_used = list_target[:]*/
    let positionsUsed = [...list_target];

    /* python: values_score = [0] * L  # to look for violations (e.g., too few/many targets)*/
    let values_score = Array.from({length: this.L}, () => 0);

    /* python: list_lures = [[]] * len(list_lures_steps)*/
    let values_code = Array.from({length: this.L}, () => 'R');

    /*python: for t in np.sort(list_target):*/
    for (let t of [...list_target].sort((a,b)=>a-b)){
      /*python: values_score[t] = values_score[t - b] + b*/
      values_score[t] = values_score[t - this.b] + this.b;

      /*python: values_code[t] = 'T'*/
      values_code[t] = 'T'
    }

    /*python:  for i, type_ in enumerate(list_lures_steps):*/
    for (let i = 0; i < this.list_lures_steps.length; i++) {

      /* python: list_pos = []*/
      let list_pos = [];

      /* python: for r in range(R):*/
      for(let r = 0; r<this.R; r++){


        /* python : positions_used_and_cannot = positions_used[:] + list(range(type_)) + list(range(b)) + positions_leading_to_violation(values_score, values_code, type_) */
        let positions_used_and_cannot = positionsUsed.concat(Array.from({length: this.list_lures_steps[i] + 1}, (v, k) => k)).concat(Array.from({length: this.b}, (v, k) => k)).concat(this.positionsLeadingToViolation(values_score, values_code, this.list_lures_steps[i] ));

        /* python : positions_not_used = [item for item in range(b-1, L) if item not in used_and_cannot]*/
        let positions_not_used = Array.from({length: this.L - (this.b - 1)}, (v, k) => k + this.b - 1).filter((x) => !positions_used_and_cannot.includes(x));

        if(positions_not_used.length==0){
          return;
        }

        /* python : list_lures[i] = random.sample(not_used, R)*/
        let pos_ = [...positions_not_used].sort(function () {
          return .5 - Math.random()
        })[0];

        positionsUsed = positionsUsed.concat([pos_]);
        list_pos = list_pos.concat([pos_]);

        values_code[pos_] = 'L' + this.list_lures_steps[i];


        /*python : used += list_lures[i]*/
        values_score = this.updateScore(values_score, values_code, this.list_lures_steps[i], pos_)
      }

      /*python: list_lures[i] = list_pos*/
      list_lures[i] = list_pos;
    }

    /* python: return(values_code)*/
    return values_code;

  }

  private assignNumbers(values_codes){
    /* python: values_number = []*/
    let values_number = [];

    /* python: for i in range(L):*/
    for (let i =  0; i<this.L; i++){

      let x;
      /* python: if (values_code[i] in ['R']):*/
      if(values_codes[i] === 'R'){
        /* python: x = get_constraints_random(values_number[max(0,i-max(list_lures_steps)):i])*/
         x = this.get_contraints_random(values_number.slice(Math.max(i-this.list_lures_steps[this.list_lures_steps.length-1],0),i+this.list_lures_steps[this.list_lures_steps.length-1]));
      }
      /*python: elif (values_code[i] in ['T']):*/
      else if(values_codes[i] === 'T'){

        /* python: x = values_number[i-b]*/
        x = values_number[i-this.b];
      }

      /* python: else:*/
      else {

        /* python: x = values_number[i-int(list(values_code[i])[-1])]*/
        x = values_number[i-parseInt(values_codes[i].slice(-1))];

      }

      /* python: values_number.append(x)*/
      values_number.push(x);

    }

    /* python: return(values_number)*/
    return values_number;
  }

  private get_contraints_random(values_number){
    /* python: let options_ = [item for item in range(10) if item not in values_number]*/
    let options = Array.from({length: 10}, (v, k) => k).filter((x) => !values_number.includes(x));
    let shuffled = options.sort(function () {
      return .5 - Math.random()
    });
    return shuffled[0];
  }

}
