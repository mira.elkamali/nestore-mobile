import {Component, EventEmitter, Input, Output} from '@angular/core';
import { APIService } from '../../chat/services/api';
import {GlobalvarProvider} from "../../app/globalprovider";
import {Subject} from "rxjs";

@Component({
  selector: 'page-nutrition-select',
  templateUrl: 'nutrition-select.html',
})
export class NutritionSelectPage {
  @Input() suggestions;
  @Output() onBack = new EventEmitter();
  @Output() valid = new EventEmitter();

  loaded = false;
  mode = 'normal';

  private all_items: any[];
  private search_items: any[];

  confirmClick = new Subject();
  removeClick = new Subject();

  constructor(
              public api: APIService, public globalvarProvider : GlobalvarProvider) {
    this.confirmClick.subscribe(()=>{
      this.confirm();
    })

    this.removeClick.subscribe((fromTuto)=>{
      if(fromTuto!==true){
        this.removeFood(fromTuto)
      }
    })

  }

  ngOnChanges(changes){
    if(this.suggestions){
      console.log(this.suggestions);
      this.mode = 'combo';
    }
  }
  
  initializeGroupedItems() {
    //TODO: cache results, move to GlobalVarProvider
    this.api.groupedFoodList().then(
      (success) => {
        console.log(success);
        this.all_items = success.foods.sort();
        this.search_items = this.all_items;
      },
      (error) => {
        console.log(error);
        this.all_items = [];
      }
    );
  }

  getItems(event) {
    // Reset items back to all of the items
    this.search_items = this.all_items;
    console.log(this.search_items);

    // set val to the value of the ev target
    var val = event.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.search_items = this.search_items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  itemSelected(item) {
   /* this.navCtrl.setRoot(NutritionPage, {
      selectedDish: item
    });*/
    // this.navCtrl.push(MealDetailPage, {
    //   item: item
    // });
  }

  validate(e){
    console.log(e);
    if(e.type==='combo'){
      this.mode = 'combo';
      if(!this.suggestions){
        this.suggestions=[];
      }
    }

    if(this.suggestions) {
      if(!this.suggestions.find(e => e === e.meal)){
        this.suggestions.unshift(e.meal)

      }
    }
    else{
      this.valid.emit(e);
      console.log(e);
    }
  }

  confirm(){
    this.valid.emit({ meal: this.suggestions,  type:this.mode==='combo'?'combo':''});
  }

  removeFood(name){
    this.suggestions = this.suggestions.filter(e => e !== name);
    if(this.suggestions.length==0){
     this.mode = 'normal';
     this.suggestions = null;
    }
  }
}
