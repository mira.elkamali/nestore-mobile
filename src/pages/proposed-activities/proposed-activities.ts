import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import {APIService} from '../../chat/services/api';
import {GlobalvarProvider} from '../../app/globalprovider';
import {sp} from "@angular/core/src/render3";


@Component({
  selector: 'proposed-activities',
  templateUrl: 'proposed-activities.html',
})
export class ProposedActivitiesPage {
  @Output() onClose = new EventEmitter();
  @Output() canBack = new EventEmitter();
  @Output() action = new EventEmitter();
  clicked = false;
  selectedActivity = null;
  scheduledActivity = null;
  loaded = false;

  constructor(private api: APIService, private globalvarProvider: GlobalvarProvider) {
    this.load();
  }

  reload(){
    this.globalvarProvider.reloadActivities();
    this.load();
  }

  load(){
    this.loaded = false;
    this.api.getProposedActivitiesExt();
  }

  close() {
    this.onClose.emit();
  }


  startActivity(activity){
    let domain = activity.domain;
    let id = activity.idce;
    if(activity.task && activity.task.startsWith("game")) {
      this.action.emit({'action':'open-cognitive', 'task':activity.task.substring(5,activity.task.length)});
    }
    else {
      this.api.putActivity(this.globalvarProvider.id, id, {validate: true}).then(() => {
        activity.planned = true;
      });
    }
  }

  scheduleActivity(activity){
    this.scheduledActivity = activity;
    //let date = new Date();
    //let dateText = date.getDate()+"."+(("0" + (date.getMonth() + 1)).slice(-2))+"."+date.getFullYear();
    /*this.api.putActivity(this.globalvarProvider.profile.user_id, activity.id, {scheduleDate: dateText}).then(
      ()=>{
        console.log('test');
      }
    )*/
  }

  showDetails(activity){
    this.selectedActivity = activity;
    console.log(this.selectedActivity)
  }

}
