import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Storage } from '@ionic/storage';

import { SubmitDishPage } from '../submit-dish/submit-dish';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from '../../providers/api/api';
import { NutritionSelectPage } from '../nutrition-select/nutrition-select';
import { NativeStorage } from '@ionic-native/native-storage';
import {APIService} from '../../chat/services/api';
import {GlobalvarProvider} from '../../app/globalprovider';
import {ENVIRONMENT} from "../../environement.config";

@Component({
  selector: 'page-physical',
  templateUrl: 'physical.html',
})
export class PhysicalPage {
  @Output() onClose = new EventEmitter();
  @Output() canBack = new EventEmitter();
  clicked = false;

  constructor(private api: APIService, private globalvarProvider: GlobalvarProvider) {
  }

  close() {
    this.onClose.emit()
  }

  startActivity(activity){
    this.globalvarProvider.startedActivity = {mode:'test',activity:activity};
  }

  sendBroadcast(){
    let WOTId = ENVIRONMENT.WOT_ID;
    let extras = {};
    extras[WOTId+'.extra.FROM'] = 'institute.humantech.nestore';
    if((<any>window).plugins) { // useful when we test without deploying
      (<any>window).plugins.intentShim.sendBroadcast({
          flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
          package: WOTId,
          action: WOTId+".action.ENABLE_TEST_6MWT",
          extras: extras
        }, function (r) {
          console.log(r);
        },
        function () {

        });
    }
  }

}
