import {ChangeDetectorRef, Component} from '@angular/core';
import {App, NavController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';
import {APIService} from '../../chat/services/api';
import {IamClientProvider} from '../../providers/iam-client/iam-client';
import {LoginPage} from '../login/login';
import {Camera} from '@ionic-native/camera';
import {NativeStorage} from '@ionic-native/native-storage';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Storage} from '@ionic/storage';
import {AppVersion} from "@ionic-native/app-version";
import {EmailComposer} from "@ionic-native/email-composer/";

@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
/**
 * Contains properties of the User Page.
 */
export class UserPage {
  selectedCard = null;
profile: any;
option = null;
settings = false;
photo = null;
loaded = false;
langs = null;

valueWeight;
valueHeight;
version;
notificationEnable;
notificationDisable;

  constructor(public appVersion: AppVersion,public cdref : ChangeDetectorRef, private app: App, public iamClient: IamClientProvider, public apiService: APIService, public translate: TranslateService, public alertCtrl: AlertController, public navCtrl: NavController, public camera: Camera,
  public nativeStorage: NativeStorage, public api : APIService, public globalvarProvider : GlobalvarProvider, private storage: Storage, private emailComposer: EmailComposer) {
    this.alertCtrl = alertCtrl;
    this.navCtrl = navCtrl;
    this.profile = {};



    console.log('version');

    this.appVersion.getVersionNumber().then((version)=>{
      console.log('version');
      this.version=version;
    });

    this.notificationEnable = globalvarProvider.profile.dss_profile && globalvarProvider.profile.dss_profile.preferences && globalvarProvider.profile.dss_profile.preferences.nutritional && globalvarProvider.profile.dss_profile.preferences.nutritional.notification;

  }

  changeParam(e){
    this.notificationDisable = true;
    this.api.putParameters('notification', e).then(()=>{
      this.notificationDisable = false;
    }).catch(()=>{
      this.notificationDisable = false;
      this.notificationEnable = !this.notificationEnable;
      this.globalvarProvider.error = true;
    })
  }



  /**
   * Call the loadUserProfile() funnction when the page has fully entered and is now the active page.
   */
  ionViewDidEnter(): void {
    this.option=false;
    setTimeout(()=>{ // loading of the page take time if not in setTimeout
      Promise.all([
        this.getPhoto('photo-profile')]).then(()=>{this.loaded = true})
    },0)
  }


  /**
  * Check Auth state before rendering the view to allow/deny access for rendering this view
  */
  ionViewCanEnter(): boolean {
    return true;
  }

  takePictureIfNoPhoto(){
    if(!this.photo){
      this.takePicture()
    }
  }

  takePicture() {
    console.log("taking picture");
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 700,
      targetHeight: 700
    }).then(
      (imageData) => {
        this.photo = "data:image/jpeg;base64," + imageData;
        this.storeInNativeStorage(this.photo)
      }, (err) => {
        console.log(err);
      });
  }

  storeInNativeStorage(photo){
    this.nativeStorage.setItem('photo-profile', { photo: photo })
      .then(
        () => {
          console.log("stored photo!");
        },
        (error) => console.log("Error storing photo", error)
      );
  }

  getPhoto(id) {
    return this.nativeStorage.getItem(id).then(
      (success) => {
        console.log("got photo: " + id, success);
        this.photo =  success.photo;
      },
      (error) => {
        console.log("couldn't get photo: " + id, error);
      });
  }


  onSelectChange(value){
    let lang = this.langs.find((x)=> x.code === value);

    this.storage.set('lang',lang.id);

    console.log('change_lang');
    this.apiService.putLang(lang.code).then(()=>{
      console.log('change lang2');
      this.globalvarProvider.profile.lang = lang.code;
    }).catch((e)=>{
      console.log(e);
    });

    this.translate.use(lang.code);
  }

  test1(e, option){
    console.log('test');
      this.selectedCard=option;
      e.stopPropagation();
  }

  clickOutside(){
    this.selectedCard=null;
  }

  openLanguage(){
    this.loaded = false;
    this.api.getLangs().then((langs)=>{
      this.loaded = true;
      this.option='lang';
      this.langs = langs
    }).catch(()=>{
      this.loaded = true;
      this.globalvarProvider.internalError = true;
    })
  }

}
