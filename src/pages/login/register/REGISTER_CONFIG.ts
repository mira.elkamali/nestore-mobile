import { ENVIRONMENT } from "../../../environement.config";

export const REGISTRATION_FORM_ACTIVE = {
  birthday: false,
  country: false,
  gender: false,
  timezone: false,
  description: false,
  language: false,
  title: false,
  middleName: false,
  suffix: false,
  job: false,
  screenName: true,
  pilotSite: false
};

// *
// Google Credentials
// *

export const GOOGLE_CREDENTIALS = {
  webClientId: "793800156735-q9pl0nst1dguecqbl57imiv2363l94vj.apps.googleusercontent.com",
  offline: true
};

export const GOOGLE_CAPTCHA_KEY = "6LdDwJ8UAAAAAIuKMpuH6YmA7aAdvUIwmtqAQa8p";

// *
// register activation switches
// *

// *
// Development API_KEY
// *
//export const API_KEY: string = "105_3g8b2yiyuneo04ggc40kw4gggogowok048ogcs4gooccoo08sk";

export const APIS = {
  nestoreRegister: {
    url: "https://api.nestore-coach.eu",
    endpoints: {
      register: "/ropardo/mw/register"
    },
    headers: {
      "Content-Type": "x-www-form-urlencoded",
      apiKey: ENVIRONMENT.API_KEY
    }
  }
};
