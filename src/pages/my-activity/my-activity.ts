import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { NutritionEvent } from '../../models/nutrition-event';
import { History } from '../../models/history';
import {TranslateService} from '@ngx-translate/core';
import {APIService} from '../../chat/services/api';
import {GlobalvarProvider} from '../../app/globalprovider';

const IMAGES = {
  social: {
    on: "../../assets/icon/Social_Charts_on.svg",
    off: "../../assets/icon/Social_Charts_off.svg"
  },
  physical: {
    on: "../../assets/icon/Physical_Charts_on.svg",
    off: "../../assets/icon/Physical_Charts_off.svg"
  },
  nutrition: {
    on: "../../assets/icon/Food_charts_on.svg",
    off: "../../assets/icon/Food_charts_off.svg"
  },
  cognitive: {
    on: "../../assets/icon/Cognitive_charts_on.svg",
    off: "../../assets/icon/Cognitive_charts_off.svg"
  }
}

@Component({
  selector: 'page-my-activity',
  templateUrl: 'my-activity.html',
})
export class MyActivityPage {


  history: History;
  waterIntake;
  activeDomain: string = "nutrition"; // nutrition per default

  images = {
    social: "../../assets/icon/Social_Charts_off.svg",
    physical: "../../assets/icon/Physical_Charts_off.svg",
    nutrition: "../../assets/icon/Food_charts_on.svg",
    cognitive: "../../assets/icon/Cognitive_charts_off.svg"
  };
  timeframe = "day";
  activities;
  loaded = true;
  filtredActivities = [];

  private date: Date = new Date();
  private scheduledActivity;
  private startedActivity;

  currentPromise;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public apiService: APIService,
    private translate: TranslateService,
    private globalProvider: GlobalvarProvider,
) {

    this.activities = [];
    translate.onLangChange.subscribe(()=>{
      this.date = new Date(this.date);
    });
    if(globalProvider && globalProvider.profile && globalProvider.profile.dss_profile && globalProvider.profile.dss_profile.working_memory && globalProvider.profile.stage==='intervention'){
      this.activeDomain = 'pathway';
    }
    else{
      this.activeDomain = 'social';
    }

    this.globalProvider.reloadActivitiesSubject.subscribe(()=>{
      this.dataChanged()
    })

  }

  dataChanged() {
    console.log('changed');
    this.getData();
  }

  getData() {
    console.log('test');
    this.loaded = false;
    let requests = [
      {promise : (resolve)=>this.apiService.getWaterIntake(this.date, 'day').then((x)=>resolve(x)), action: (x)=>{this.waterIntake = x}, domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getHistory(this.timeframe, this.date).then((x)=>resolve(x)), action: (x)=>{this.history = x}, domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getActivities(this.globalProvider.profile.user_id, MyActivityPage.getDate(this.date)).then((x)=>resolve(x)), action: (x)=>{
        console.log(this.date);
        this.filtredActivities = [];
          x.map((c)=>{
          if (c['ce_id'].startsWith('ce_phy')) {
            c.domain = 'physical';
          }
          else if (c['ce_id'].startsWith('ce_cog')) {
            c.domain = 'cognitive';
          }
          else if (c['ce_id'].startsWith('ce_soc')) {
            c.domain = 'social';
          }
          else if (c['ce_id'].startsWith('ce_nut')) {
            c.domain = 'nutrition';
          }
          this.activities = x;
          console.log(this.activities);
          this.getActivities(this.activeDomain);
          })}, stages:['intervention'],domains:['nutrition','social','cognitive','physical','pathway']},
     ];

    requests = requests.filter((x)=>(!x.stages || x.stages.includes(this.globalProvider.profile.stage)) && (!x.domains || x.domains.includes(this.activeDomain)));

    if(requests.length===0){
      this.loaded = true;
      }
    else {
      let p = requests.map(x => new Promise(x.promise));
      console.log(this.date);
      let a = Promise.all(p).then((x) => {
        console.log(this.date);
        let r = Promise.all(x).then(y => {
          if(this.currentPromise === a){
            for (const [index, result] of y.entries()) {
              requests[index].action(result);
            }
            this.loaded = true;
          }
        });
      });
      this.currentPromise = a;
    }

    /*if(this.activeDomain==='nutrition') {



      let a = Promise.all([this.apiService.getWaterIntake(this.date), this.apiService.getHistory(this.timeframe, this.date)]).then(
        values => {
          this.waterIntake = values[0];
          this.history = <History>values[1];
          if(this.currentPromise === a){
            this.loaded = true;
          }
        }

      );
      this.currentPromise = a;

    }

    console.log(this.history);
    console.log("# this.date", this.date);
    this.loaded = false;
    this.apiService.getActivities(this.globalProvider.profile.user_id).then((res)=> {
      res.map((c)=>{
        if (c['ce_id'].startsWith('ce_phy')) {
          c.domain = 'physical';
        }
        else if (c['ce_id'].startsWith('ce_cog')) {
          c.domain = 'cognitive';
        }
        else if (c['ce_id'].startsWith('ce_soc')) {
          c.domain = 'social';
        }
        else if (c['ce_id'].startsWith('ce_nut')) {
          c.domain = 'nutrition';
        }
      });
       this.activities = res;
       this.getActivities(this.activeDomain);
       this.loaded = true;
    }

  );*/


  }

  getActivities(domain){
    console.log('getactivities');
    if(this.activeDomain==='pathway'){
      this.filtredActivities = this.activities.filter((x) => (MyActivityPage.today(this.date) && x['scheduled-time'] === null) || (x['scheduled-time'] != null && MyActivityPage.sameDay(this.date, new Date(x['scheduled-time']))));
      if(MyActivityPage.today(this.date)){
        this.globalProvider.profile.progress = {score:this.filtredActivities.filter((x)=>x.review && x.review.done).length, total:this.filtredActivities.length};
      }
    }
    else {
      this.filtredActivities = this.activities.filter((x) => x.domain === domain && ((MyActivityPage.today(this.date) && x['scheduled-time'] === null) || (x['scheduled-time'] != null && MyActivityPage.sameDay(this.date, new Date(x['scheduled-time'])))));
    }
    console.log(this.filtredActivities);
  }

  checkActivity(activity){
    this.apiService.putActivity(this.globalProvider.id, activity.id, {domain:activity.domain,done:true}).then(()=>{
      activity.loading = false;
      activity.review = {done:true};
    });
  }

  scheduleActivity(activity){
    this.scheduledActivity = activity;
  }

  startActivity(activity){
    if(activity.title === "ce_cog_nut"){
      if(activity.task.status === 'pending') {
        activity.loading = true;
        this.apiService.putTestActivity(activity.task.user_task_uid, {status: 'accepted'}).then(() => {
          this.globalProvider.beginGame('updating', activity.task.user_task_uid);
          activity.task.status = 'accepted';
        }).catch(() => {
          activity.loading = false;
          this.globalProvider.internalError = true;
        })
      }
      else{
        this.globalProvider.beginGame('updating', activity.task.user_task_uid);
      }
    }
    else {
      this.globalProvider.startedActivity = activity;
    }
  }

  static today(td) {
    var d = new Date();
    return td.getDate() === d.getDate() && td.getMonth() === d.getMonth() && td.getFullYear() === d.getFullYear();
  }

  static sameDay(date1, date2) {
    return date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
  }

  static getDate(date) {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
  }

}
