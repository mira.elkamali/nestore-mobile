import {Component, ElementRef, Renderer2, ViewChild} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Threshold } from '../../models/threshold';
import { Summary } from '../../models/summary';
import {TranslateService} from '@ngx-translate/core';
import {DataProvider} from "../../providers/data/data";
import {LocalCircleInteractions} from "../../models/local-circle-interactions";
import {SocialIntegrationLoneliness} from "../../models/social-integration-loneliness";
import {Loneliness} from "../../models/loneliness";
import {APIService} from '../../chat/services/api';
import {History} from '../../models/history';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Subject} from "rxjs";

@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html',
})
export class ChartsPage {
  @ViewChild('progressChart') progress;
  @ViewChild('waterChart') water;
  @ViewChild('nutrientsChart') nutrients;

  activeDomain;
  timeframe = 'day';
  isCalendarEnabled = false;


  waterValue;
  myProgress;
  loaded = false;

  progressTab = 0;

  hasNutritionData = false;


  date = new Date();

  thresholds: Threshold[];
  localCircleData: any = null;
    socialIntegrationLoneliness: SocialIntegrationLoneliness = null;
  memoryFailures: any;
  memoryAccuracy: any;
  memoryLevel: any;
  energy: any;
  energy2: any;
  omega3: any;
  loneliness: any = null;
  interactions: any = null;
  structuredActivities = null;
  summaryActivities = null;
  socialTypes: any = null;
  nutritionalOrder = [];
  nutritionalPathway = null; // TODO GET FROM INPUT, CHECK WHEN CHANGE TO UPDATE ORDER
  currentPromise;
  emotionsData;
  discreteEmotionsData;
  sleep;
  strenghtScore;
  myScore;

  enabled = [];

  scrollToProgress = new Subject();
  scrollToNutrients = new Subject();
  scrollToWater = new Subject();

  constructor(
    public navCtrl: NavController,
    public api: ApiProvider,
    public apiService: APIService,
    private el: ElementRef,
    private translate: TranslateService,
    private dataProvider: DataProvider,
    private globalProvider: GlobalvarProvider) {

    this.globalProvider.reloadChartSubject.subscribe((options:any)=>{
      this.date = new Date();
      this.timeframe = options.timeframe;
      this.activeDomain = options.domain;
      this.getData();
    });

    if(this.globalProvider.profile.stage === 'intervention'){
      //todo. get the nutritional pathway for orders
    }

    this.scrollToProgress.subscribe((fromTuto)=>{
      if(fromTuto) {
        this.progress.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'})
      }
    });

    this.scrollToNutrients.subscribe((fromTuto)=>{
      if(fromTuto) {
        this.nutrients.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'})
      }
    });

    this.scrollToWater.subscribe((fromTuto)=>{
      if(fromTuto) {
        this.water.nativeElement.scrollIntoView({behavior: 'smooth', block: 'start'})
      }
    });

    let orders = [];
    orders['NUT_DEC_BW'] = [5,2,1,3];
    orders['NUT_INC_BW'] = [3,1,2,0];
    if(this.nutritionalPathway!==null){
      this.nutritionalOrder = orders[this.nutritionalPathway];
    }
    else{
      this.nutritionalOrder = [1,2,3,4,5];
    }

    translate.onLangChange.subscribe(()=>{
      this.date = new Date(this.date);
    });
    this.globalProvider.waterUpdate.subscribe((res)=>{
      this.waterValue = res;
    });
    console.log(this.globalProvider);
    if(globalProvider && globalProvider.profile && globalProvider.profile.stage==='intervention'){
      this.activeDomain = 'pathway';
    }
    else{
      this.activeDomain = 'social';
    }

  }

  dataChanged() {
    this.getData();
    console.log('changed');
  }

  private getData() {
    // todo: are these declarations useful ?
    this.waterValue = null;
    this.socialTypes = null;
    this.socialIntegrationLoneliness = null;
    this.localCircleData = null;


    this.loaded = false;
    let requests = [
      {promise : (resolve)=>this.apiService.getMemoryFailures(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.memoryFailures = x}, timeFrames:['week', 'month'], domains:['cognitive']},
      {promise : (resolve)=>this.apiService.getMemoryAccuracy(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{
        if(this.timeframe==='week'){
          this.memoryAccuracy = x.map((y)=>y.score===null?null:Math.round(y.score))
          this.memoryLevel = x.map(y=>y.level);
        }
        else{
          this.memoryAccuracy = x.map(y=>y<0?null:Math.round(y));
        }
        }, stages:['intervention'], pathways:'COG_IMP_ME', timeFrames:['week', 'month'],domains:['cognitive']},
      {promise : (resolve)=>this.apiService.getWaterIntake(this.date, this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.waterValue = x.reduce(function (acc, obj) {return acc + obj.quantity;}, 0);}, timeFrames:['day'], domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getThresholdsCombined(this.timeframe, this.date).then((x)=>resolve(x)), action: (x)=>{this.thresholds = x; console.log(x); this.hasNutritionData = this.thresholds[0].hasData;},domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getMyProgress(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.myProgress = x},domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getSummaryNutrition(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{console.log(console.log(x)); this.energy = x.energy; this.energy2=x.energy2; this.omega3=x.omega3},domains:['nutrition']},
      {promise : (resolve)=>this.apiService.getStructuredActivities(this.getDate(),this.timeframe).then((x)=>resolve(x)), action: (x)=>{
        if(this.globalProvider.profile.stage==='intervention'){
          this.structuredActivities = x;
        }
        if(this.timeframe==='day') {
          this.energy2 = x;
        }
        },domains:['nutrition','physical'],stages:['2weeks', 'intervention']},
      {promise : (resolve)=>this.apiService.getLoneliness(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.loneliness = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getInteractionsKind(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.socialTypes = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getInteractions(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.interactions = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getCirclesInteractions(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.localCircleData = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getDimensionalEmotions(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.emotionsData = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getDiscreteEmotions(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.discreteEmotionsData = x},domains:['social'], timeFrames:['week', 'month']},
      {promise : (resolve)=>this.apiService.getUnstructuredActivities(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{
        if(this.globalProvider.profile.stage==='intervention'){
          this.strenghtScore = x;
          this.myScore = x;
        }
        this.summaryActivities = x;
        },domains:['physical']
      },
      {promise : (resolve)=>this.apiService.getSleep(this.getDate(), this.timeframe).then((x)=>resolve(x)), action: (x)=>{this.sleep = x},domains:['physical']},
    ];

    requests = requests.filter((x)=>(!x.stages || x.stages.includes(this.globalProvider.profile.stage)) && (!x.timeFrames || x.timeFrames.includes(this.timeframe)) && (!x.domains || x.domains.includes(this.activeDomain)));
    requests = requests.filter((x)=>(!x.pathways || this.globalProvider.profile.dss_profile.working_memory.pathways.map((y)=> y['pathway-name']).includes(x.pathways))); // todo: x.pathways can be an array

    if(requests.length===0){
      this.loaded = true;
    }
    else {
      let p = requests.map(x => new Promise(x.promise));
      let a = Promise.all(p).then((x) => {
        let r = Promise.all(x).then(y => {
          for (const [index, result] of y.entries()) {
            requests[index].action(result);
          }
          if(this.currentPromise === a){
            this.loaded = true;
          }
        });
      });
      this.currentPromise = a;
    }
  }

  private getIdForTranslation(str){ // TODO: ASK SIVLIA TO USE KEY FOR nutrients
    return (str.charAt(0).toLowerCase() + str.slice(1)).replace(/\s+/g, '');
  }

  private getDate() {
    return this.date.getFullYear() + "-" + (this.date.getMonth() + 1) + "-" + this.date.getDate()
  }

}
