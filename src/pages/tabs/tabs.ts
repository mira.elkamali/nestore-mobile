import {Component, ViewChild} from '@angular/core';

import {HomePage} from '../home/home';
import {UserPage} from '../user/user';
import {NavParams, Tabs} from 'ionic-angular';
import {GlobalvarProvider} from '../../app/globalprovider';
import {Message} from '../../chat/message/message.model';
import { MyActivityPage } from '../my-activity/my-activity';
import { ChartsPage } from '../charts/charts';
import {APIService} from '../../chat/services/api';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from "@ionic/storage";
import {Subject} from "rxjs";
import {animate, style, transition, trigger} from "@angular/animations";
import {PathwayForDomain} from "../../pipes/pathwayForDomain";
import {MessageProvider} from "../../chat/services/message-provider";

@Component({
  templateUrl: 'tabs.html',
  animations:[
    trigger('myInsertRemoveTrigger', [
    transition(':enter', [
      style({ bottom: '-100%', opacity:0 }),
      animate('0.4s', style({ bottom: 0, opacity:1 })),
    ])
  ]),]
})
export class TabsPage {
  openGlass = new Subject();
  openNutrition = new Subject();

  tabsIndex = 0;

  @ViewChild('myTabs') tabRef: Tabs;

  public selectedTab: number;
  numberNotRead = 0;

  message;

  clickButton = new Subject();
  changeTab1 = new Subject();
  changeTab2 = new Subject();

  tab1Root = HomePage;
  tab2Root = MyActivityPage;
  tab3Root = ChartsPage;
  tab5Root = UserPage;
  // tab5Root = NutritionPage;

  constructor(public storage:Storage, public translate: TranslateService, private api: APIService, private messageProvider: MessageProvider, private navParams: NavParams, private globalvarProvider: GlobalvarProvider, /*public localNotifications: LocalNotifications*/){
    console.log(this.globalvarProvider.profile);
    console.log(this.tabRef);
    this.globalvarProvider.selectedTab = this.navParams.get('index') || 0;
    this.globalvarProvider.setActiveTab(navParams.get('index')  || 0);

    this.messageProvider.messagesChange.subscribe(res=>{
      this.numberNotRead= this.messageProvider.messages.filter(message => !message.isRead).length});
      console.log(this.numberNotRead);

    this.globalvarProvider.tutorials = {};

    this.clickButton.subscribe(()=>{
      this.globalvarProvider.blur = !this.globalvarProvider.blur;
    });

    this.changeTab1.subscribe(()=>{
      this.changeTab(1, false);
    });

    this.changeTab2.subscribe(()=>{
      this.changeTab(2, false);
    });

    this.openGlass.subscribe(()=>{
        this.changeSelectedDomain('water', false)
    });

    this.openNutrition.subscribe(()=>{
      this.changeSelectedDomain('nutrition', false)
    });

    globalvarProvider.changeTab.subscribe((i)=>{
      this.tabRef.select(i)
    });

    globalvarProvider.reloadProposedActivitiesSubject.subscribe(()=>{
      this.api.getProposedActivitiesExt();
    })

  };

  switchTab(n: number){

    this.globalvarProvider.setActiveTab(n);
  }

  setLanguage(lang){
    this.translate.use(lang);
  }

  ionViewWillEnter() {
    if(this.globalvarProvider.selectedTab) {
      this.tabRef.select(this.globalvarProvider.selectedTab);
    }
  }

  changeTab(i, enabledIfBaseline){
    if(enabledIfBaseline || this.globalvarProvider.profile.stage!=='baseline') {
      this.globalvarProvider.selectedTab = i;
      this.tabRef.select(i);
    }
  }

  private changeSelectedDomain(domain, disabled){
    let pathwayPipe = new PathwayForDomain();
    if(domain === 'cognitive' && (this.globalvarProvider.profile.stage !== 'intervention' || (this.globalvarProvider && this.globalvarProvider.profile && this.globalvarProvider.profile.dss_profile && this.globalvarProvider.profile.dss_profile.working_memory && this.globalvarProvider.profile.dss_profile.working_memory.pathways && domain === 'cognitive' && pathwayPipe.transform(this.globalvarProvider.profile.dss_profile.working_memory.pathways, 'COG') !== 'COG_IMP_ME'))){
      return;
    }

    if(!disabled && (this.globalvarProvider.profile.stage!=='baseline')) {
      this.globalvarProvider.selectedDomain = domain;
      this.globalvarProvider.blur = false;
    }
  }

  sendText(event, questionnaireId): void {
    console.log(event);
    this.api.postQuestionnaireAnswer({
      questionnaireId: questionnaireId,
      message: {text:event.action?event.action:event.text}
    }).then((message)=>{
      console.log('d');
      console.log(message);
      this.globalvarProvider.message=message;
      this.globalvarProvider.message.bot = true;
      if(this.globalvarProvider.message.discussionEnd){
        setTimeout(()=>{
          this.api.initUser().then(
            (res) => {
              console.log(res);
              this.globalvarProvider.profile = res;
              this.globalvarProvider.currentQuestionnaire = null;
              this.globalvarProvider.message=null;
            });
        },2000)
      }
    }).catch((e)=>{
      this.globalvarProvider.currentQuestionnaire = null;
      this.globalvarProvider.internalError = true;
    });
  }

  openLink(link){
    console.log('link');
    window.open(link,'_system', 'location=yes');
  }

  changeStage(){
    this.globalvarProvider.click2weeks=false;
    if(!this.globalvarProvider.processLoading) {
      this.globalvarProvider.processLoading = true;
      this.api.putProfile({stage:'2weeks'}).then((res) => {
        this.api.initUser().then(
          (profile) => {
            this.globalvarProvider.profile = profile;
          })
      }).catch((r)=>{
        this.globalvarProvider.error = true;
      });
    }
  }

  transitionLoaded = false;

  transitionEnd(){
    this.transitionLoaded = this.globalvarProvider.blur;
  }

}
