import {Directive, Input, Output, EventEmitter} from '@angular/core';

@Directive({
  selector: '[ngInit]'
})
export class LoadDirective {

  constructor(){
  }

  @Input() isLast: boolean = true;

  @Output('ngInit') initEvent: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
      setTimeout(() => this.initEvent.emit(), 0);
  }
}
