import {Pipe} from "@angular/core";
import {PipeTransform} from "../../node_modules_old/@angular/core";

@Pipe({name: 'numberToArray'})
export class NumberToArrayPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    let res = [];
    for (let i = 0; i < value; i++) {
      res.push(i);
    }
    console.log(res);
    return res;
  }
}
