import {Directive, Input, Output, EventEmitter, ElementRef} from '@angular/core';

@Directive({
  selector: '[transitionEnd]'
})
export class TransitionEndDirective {

  constructor(el: ElementRef){
    el.nativeElement.addEventListener("transitionstart", (event) => {
    }, false);

    el.nativeElement.addEventListener("transitionend", (event) => {
      setTimeout(() => this.initEvent.emit(true), 0);
    }, false);
  }

  @Output('transitionEnd') initEvent: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
  }
}
