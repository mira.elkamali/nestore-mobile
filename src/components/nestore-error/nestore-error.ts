import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import * as d3 from 'd3';


@Component({
  selector: 'nestore-error',
  templateUrl: 'nestore-error.html'
})
export class NestoreErrorComponent {
  @Input() text = 'Cannot connect to server';
  @Input() button = 'Retry';

  @Output() onRetry = new EventEmitter();
  mode = 'retry';

  constructor() {
  }

  ngOnInit() {

  }

  retry(){
    this.onRetry.emit();
  }
}
