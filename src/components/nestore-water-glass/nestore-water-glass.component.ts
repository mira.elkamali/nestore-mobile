import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'nestore-water-glass',
  templateUrl: 'nestore-water-glass.component.html',
})
export class NestoreWaterGlassComponent {
  @Input() canRemove = true;
  @Input() selected = false;
  @Output() selectedChange = new EventEmitter();

  select(){
    if(this.canRemove) {
      this.selected = !this.selected;
      this.selectedChange.emit(this.selected)
    }
  }

}
