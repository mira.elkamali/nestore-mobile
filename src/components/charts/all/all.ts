import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {SocialIntegrationLoneliness} from "../../../models/social-integration-loneliness";
import {APIService} from '../../../chat/services/api';

/**
 * Generated class for the AllComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-all',
  templateUrl: 'all.html'
})
export class AllComponent {
  @Input() date = new Date();
  @Input() timeframe;

  showdetails = false;

  chartColors: string[] = [
    '#0B6789',
    '#76C1DB',
  ];

  chartDataSets: ChartDataSets[];
  chartLegend: boolean = false;
  chartType: ChartType = 'doughnut';
  chartOptions: ChartOptions = {
    animation: {duration:0},
    responsive: true,
    segmentShowStroke: false,
    cutoutPercentage: 70,
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        maintainAspectRatio: true,
      }],
      yAxes: [{
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
      }],
    }
  };

  @Input() interactions;

  constructor(private api: APIService){
    /**/
    console.log('constructor', this.interactions);
    //this.computeChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    /*if(changes.date || changes.timeframe){
      this.getData();
    }*/
    if(changes.interactions){
      this.computeChart();
    }
  }

  getData(){
    this.api.getInteractions(this.date.toISOString().substring(0,10),this.timeframe).then(data =>
      {
        this.interactions = data;
        this.computeChart();
      }
    )
  }

  computeChart() {
    let data = [this.interactions.in_person, this.interactions.other_type];
    this.chartDataSets = [
      {data: data, label: 'data1', fill: false, backgroundColor: this.chartColors},
    ];
  }

}
