import {ChangeDetectorRef, Component, Input, SimpleChanges, ViewChild} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-discrete-emotions-chart',
  templateUrl: 'discrete-emotions-chart.html'
})
export class DiscreteEmotionsChart {
  @ViewChild('tabs') tabsElement;
  @Input() data;
  showdetails = false;


  strongest=null;
  public tab = 0;
  width = 0;
  left;
  animate = false;
  emotions =
    [{textId:"chart-type-discrete-emotions-anger", color:'#FF6375'},
    {textId:"chart-type-discrete-emotions-disgust", color:'#887FCE'},
    {textId:"chart-type-discrete-emotions-fear", color:'#FDB2A4'},
    {textId:"chart-type-discrete-emotions-happiness", color:'#19A595'},
    {textId:"chart-type-discrete-emotions-sadness", color:'#76C1DB'},
    {textId:"chart-type-discrete-emotions-surprise", color:'#FCE11B'}];

  chartColors: string[] = [
    '#FF6375',
    '#887FCE',
    '#FDB2A4',
    '#19A595',
    '#76C1DB',
    '#FCE11B',
  ];

  chartDataSets: ChartDataSets[];
  chartLegend: boolean = false;
  chartType: ChartType = 'doughnut';
  chartOptions: ChartOptions = {
    tooltips: {
      enabled: false
    },
    animation: {duration:0},
    responsive: true,
    segmentShowStroke: false,
    cutoutPercentage: 70,
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        maintainAspectRatio: true,
      }],
      yAxes: [{
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
      }],
    }
  };


  @Input() timeframe;
  @Input() value = 0;
  @Input() total = 1600;

  constructor(private cdRef:ChangeDetectorRef){

  }

  ngOnChanges(changes) {
    if (changes.data) {
      if (!this.data.error) {
        let d = this.data.map(x => {
          return x.value
        });
        let indexOfMaxValue = d.indexOf(Math.max(...d));
        this.strongest = 'chart-type-discrete-emotions-' + this.data[indexOfMaxValue].emotion;
        this.chartDataSets = [
          {data: d, label: 'data1', fill: false, backgroundColor: this.chartColors},
        ];
      }
    }
  }


}
