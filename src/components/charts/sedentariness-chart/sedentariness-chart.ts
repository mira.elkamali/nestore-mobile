import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-sedentariness-chart',
  templateUrl: 'sedentariness-chart.html'
})
export class SedentarinessChartComponent {
  @Input() timeframe;
  @Input() value;

  constructor() {
  }

}
