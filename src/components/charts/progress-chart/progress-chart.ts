import {Component, EventEmitter, Input, Output, SimpleChanges, ViewChild} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { ChangeDetectorRef } from '@angular/core';

import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'progress-chart',
  templateUrl: 'progress-chart.html'
})

export class ProgressChartComponent {
  @Input() data;
  @Input() date;
  @Input() timeframe;

  @ViewChild('tabs') tabsElement;
  private weight;
  private height;
  private lastweight;
  private fatMass;
  private lastFatMass;
  private bmi = 0;
  private unit = 'kg';
  @Input() tab = 0;
  @Output() tabChange = new EventEmitter();
  width = 0;
  left;
  animate = false;

  constructor(private cdRef:ChangeDetectorRef){

  }

  ngOnChanges(change){
    if(change.data) {
      console.log(this.data);
      if (this.data.height && this.data.height.length>0)
        this.height = this.data.height[0];
      }
      if (this.data.weight && this.data.weight.length>0){
        this.weight = this.data.weight[0];
      }
      if(this.data.weight && this.data.weight.length>1) {
        this.lastweight = this.data.weight[1];
      }
      if(this.data.fat_mass && this.data.fat_mass.length>0) {
      this.fatMass = this.data.fat_mass[0];
      }
      if(this.data.fat_mass && this.data.fat_mass.length>1) {
      this.lastFatMass = this.data.fat_mass[1];
    }
      /*if(this.weight && this.height) {
        this.bmi = this.weight.value / (this.height.value * this.height.value / 10000);
      }*/
      if(this.timeframe==='day' || this.timeframe==='week') {
        if (this.data.bmi) {
          this.bmi = this.data.bmi[0].value;
        }
      }
  }

  ngAfterViewChecked(){
    this.computeWidth(false);
  }

  changeTab(value){
    this.tab=value;
    this.tabChange.emit(value);
  }

  computeWidth(animate){
    if(animate) {
      this.animate = true;
    }
    let span = this.tabsElement.nativeElement.children[this.tab].children[0];
    this.width = span.offsetWidth;
    this.left = span.getBoundingClientRect().x - this.tabsElement.nativeElement.getBoundingClientRect().x;
    this.cdRef.detectChanges();
  }
}
