import {ChangeDetectorRef, Component, Input, SimpleChanges, ViewChild} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

@Component({
  selector: 'nestore-dimensional-emotions-chart',
  templateUrl: 'dimensional-emotions-chart.html'
})
export class DimensionalEmotionsChart {
  @ViewChild('tabs') tabsElement;
  showdetails = false;
  public tab = 0;
  width = 0;
  left;
  animate = false;

  @Input() timeframe;
  @Input() data = {};


  constructor(private cdRef:ChangeDetectorRef){

  }

  ngAfterViewChecked(){
    this.computeWidth(false);
  }

  computeWidth(animate){
    if(animate) {
      this.animate = true;
    }
    let span = this.tabsElement.nativeElement.children[this.tab].children[0];
    this.width = span.offsetWidth;
    this.left = span.getBoundingClientRect().x - this.tabsElement.nativeElement.getBoundingClientRect().x;
    this.cdRef.detectChanges();
  }
}
