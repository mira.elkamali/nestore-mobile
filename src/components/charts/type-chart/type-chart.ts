import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {APIService} from '../../../chat/services/api';

/**
 * Generated class for the TypeChartComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-type-chart',
  templateUrl: 'type-chart.html'
})
export class TypeChartComponent {

  showdetails = false;

  chartColors: string[] = [
    '#0B6789',
    '#2f4b7c',
    '#58508d',
    '#bc5090',
    '#FF6375',
    '#FDB2A4',
    '#ff7c43',
    '#ffa600',
  ];

  chartDataSets: ChartDataSets[];
  chartLegend: boolean = false;
  chartLabels: string[][];
  chartType: ChartType = 'pie';
  chartOptions: ChartOptions = {
    animation: {duration:0},
    responsive: true,
    segmentShowStroke: false,
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        maintainAspectRatio: true,
      }],
      yAxes: [{
        ticks: {
          display: false,
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
      }],
    }
  };

  @Input() socialTypes;

  ngOnChanges(changes: SimpleChanges) {
    /*if(changes.date || changes.timeframe){
      this.getData();
    }*/
    if(changes.socialTypes){
      this.computeChart()
    }
  }

  /*getData(){
    this.api.getInteractionsKind(this.date.toISOString().substring(0,10),this.timeframe).then(data =>
      {
        this.socialTypes = data;
        console.log(this.socialTypes);
        this.computeChart();
      }
    )
  }*/

  constructor(private api: APIService){
   this.socialTypes = [];
  }

  computeChart(){
    let data = [];
    let total = this.socialTypes.reduce(function (acc, obj) { return acc + obj.percentage; }, 0);
    for(let i = 0; i < this.socialTypes.length; i++) {
      data.push(this.socialTypes[i].percentage);
      this.socialTypes[i].percentage /= total;
    }


    this.chartDataSets = [
      {data: data, label: 'data1', fill: false, backgroundColor: this.chartColors},
    ];
  }

}
