import {Component, Input, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import {Globalization} from "@ionic-native/globalization/ngx";

/**
 * Generated class for the MemoryAccuracyComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'nestore-memory-accuracy',
  templateUrl: 'memory-accuracy.html'
})
export class MemoryAccuracyComponent {
  chartDataSets: ChartDataSets[];
  chartColors: string[] = [];
  chartLegend: boolean = false;
  chartLabels: string[][] = [];
  chartType: ChartType = 'bar';
  chartOptions: ChartOptions = {
    animation: {duration:0},
    responsive: true,
    scales: {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false
        },
        barThickness: 20,
        maintainAspectRatio: true,
        stacked: true,
        ticks: {
          fontFamily: "'Lato'",
          fontWeight: 900,
          fontStyle: 'bold',
          fontSize: 16,
          fontColor: '#333333'
        }
      }],
      yAxes: [{
        gridLines: {
          drawBorder: false
        },
        stacked: false,
        ticks: {
          beginAtZero: true,
          stepSize: 5,
          callback: function(item, index, items) {
            if(item == 100 || item == 50 || item == 75){
              let el = document.createElement('img');
              el.setAttribute('src', 'assets/icon/up.svg');
              //return el;
              return item;
            }
          }
        }
      }],
    }
  };

  @Input() data: number[] = [

  ];

  @Input() levels;
  @Input() memoryAccuracyStatsMonth: number[] = [
    80, 65, 45, 80
  ];
  @Input() memoryAccuracyStatsWeek: number[] = [
    80, -1, -1, 65, -1, -1, 30
  ];
  @Input() timeframe: string;
  @Input() colorThresholds: object[] = [
    {'from': 0, 'to': 50, 'color': '#fd6e5b'},
    {'from': 50, 'to': 75, 'color': '#ffd938'},
    {'from': 75, 'to': 101, 'color': '#96e24b'},
  ];

  private memoryAccuracyBarBG: number[] = [];

  private weekdays: string[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

  constructor(private globalization: Globalization) {
    if(this.timeframe == 'day' || this.timeframe == undefined) {
      return;
    }
    this.computeChart();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.timeframe == 'month' || this.timeframe == 'week') {
      this.computeChart();
    }
  }

  private computeChart() {
    this.chartLabels.length = 0;
    let memAccuracy = [];
    if(this.timeframe == 'month') {
      for(let i = 0; i < this.memoryAccuracyStatsMonth.length; i++) {
        this.memoryAccuracyBarBG[i] = 100;
        this.chartLabels.push([this.memoryAccuracyStatsMonth[i] + '%', 'Wk' + (i+1)]);
        memAccuracy.push(this.memoryAccuracyStatsMonth[i]);
      }
    } else if(this.timeframe == 'week') {
      for(let i = 0; i < this.memoryAccuracyStatsWeek.length; i++) {
        if(this.memoryAccuracyStatsWeek[i] != -1) {
          this.memoryAccuracyBarBG[i] = 100;
          this.chartLabels.push([this.memoryAccuracyStatsWeek[i] + '%', this.getWeekday(i)]);
        } else {
          this.memoryAccuracyBarBG[i] = 0;
          this.chartLabels.push(['','•'])
        }
        memAccuracy.push(this.memoryAccuracyStatsWeek[i]);
      }
    }

    this.chartColors.length = 0;
    for(let i = 0; i < memAccuracy.length; i++) {
      this.chartColors.push(this.getValueColor(memAccuracy[i]));
    }

    this.chartDataSets = [
      {data: memAccuracy, label: 'data1', fill: false, backgroundColor: this.chartColors},
      {data: this.memoryAccuracyBarBG, label: 'data2', fill: false, backgroundColor: ['#f0f0f0', '#f0f0f0', '#f0f0f0', '#f0f0f0']}
    ];
  }

  private getWeekday(index: number) {
    return this.weekdays[index];
  }

  private getValueColor(value: number): string {
    let color: string;
    for(let i = 0; i < this.colorThresholds.length; i++) {
      let e = this.colorThresholds[i];
      if(value >= e['from'] && value < e['to']) {
        color = e['color'];
        break;
      }
    }
    return color;
  }


}
