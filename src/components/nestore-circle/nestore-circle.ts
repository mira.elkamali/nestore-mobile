import {Component, Input, ViewChild} from '@angular/core';
import * as d3 from 'd3';


@Component({
  selector: 'nestore-circle',
  templateUrl: 'nestore-circle.html'
})
export class NestoreCircleComponent {
  @Input() color = 'black';
  @Input() text = null;


  constructor() {
  }

  ngOnInit() {

  }
}
