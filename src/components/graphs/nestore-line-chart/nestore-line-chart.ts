import {ChangeDetectorRef, Component, Input, OnChanges, Output, ViewChild} from '@angular/core';
import * as d3 from 'd3';
import {CoordinatesChart} from '../coordinatesChart';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'nestore-line-chart',
  templateUrl: 'nestore-line-chart.html'
})
export class NestoreLineChartComponent extends CoordinatesChart implements OnChanges {
  @Input() label = '';
  @Input() pointSize = 5;
  @Input() showPoints = true;
  @Input() clickable = false;
  @Input() unit;

  displayValue;
  line1;
  line2;
  line3;
  line;

  maxVar;

  static getLineSVG(data ) {
    const lineGenerator = d3.line()
      .x((d) => d[0])
      .y((d) => d[1])
      .curve(d3.curveCardinal);

    return lineGenerator(data);
  }


  constructor(protected cdRef: ChangeDetectorRef, protected translate: TranslateService) {
    super(cdRef, translate, 'line');

  }

ngOnChanges(changes) {
    if (!this.max) {
      this.maxVar = null;
    } else {
      this.maxVar = this.max;
    }
}

  initChart() {
    if (this.line) {
      this.line.remove();
    }
    if (this.line2) {
      this.line2.remove();
    }
    if (this.line3) {
      this.line3.remove();
    }
    if (this.line1) {
      this.line1.remove();
    }
    super.initChart();

    const svgData = this.svgData[0].filter((coord) => {return coord[1] !== null});
    console.log(svgData);
    let lineData = "";
    svgData.map(( coordinates, i )=>{
        let command = i === 0 ? "M" : "L";
        lineData = lineData
          + " "
          + command
          + " "
          + coordinates[0]
          + ","
          + coordinates[1]
    });


    this.line3 = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "path"
    );

    this.line = document.createElementNS(
      'http://www.w3.org/2000/svg',
      'path'
    );
    this.line.setAttribute('d', NestoreLineChartComponent.getLineSVG(svgData));
    this.line.setAttribute('fill', 'none');
    this.line.setAttribute('stroke', this.color);
    this.line.setAttribute('stroke-width', '5');
    this.g.appendChild(this.line);

    this.svgChart.nativeElement.appendChild(this.g);

    svgData.map(( coordinates, index ) => {
      if (this.showPoints) {
        let point = document.createElementNS(
          "http://www.w3.org/2000/svg",
          "circle"
        );
        point.setAttribute("cx",coordinates[0]);
        point.setAttribute("cy",coordinates[1]);
        point.setAttribute("r", ''+this.pointSize);
        point.setAttribute("fill", this.color);
        point.addEventListener('click', this.onClick.bind(this,coordinates[2]));
        //point.setAttribute("stroke", "#fff");
        //point.setAttribute("stroke-width", "2");
        this.g.appendChild(point);
      }});
  }

  getValueArray( graphData ) {
    return graphData.map(( item ) => {
      return item.value;
    });
  }

  calculateSVGData( graphData, width, height ) {
    const values = this.getValueArray( graphData );
    return this.getCoordinates( values, width, height );
  }


  getCoordinates( values, width, height ) {
    // let min = Math.min.apply( null, values )-1;
    console.log(this.maxVar);
    let min = this.maxVar ? 0 : Math.min.apply( null, values.filter(x => x !== null ) ); // 0;
    let max = this.maxVar ? this.max : Math.max.apply( null, values );

    const diff = max - min;

    if (!this.maxVar) {
      min = min - diff * 0.3;
      max = max + diff * 0.3;
    }



    if(max==0){
      max=3;
    }

    let min2 =  0;//Math.min.apply( null, values.filter(x=>x!==null) );//(this.showPoints?0:0); //point size
    let max2 = height - (this.showPoints?0:0); //point size

    let yRatio = height / ( max - min );
    let xRatio = width / ( values.length );


    return values.map( ( value, i ) => {
      // let y = height - (yRatio * ( value - min )) ;
      let x = ( xRatio * i ) + ( xRatio / 2 );

      // rescale in order than point stay in view

      //let y2 = min2 + (y/height) * (max2-min2);
      let y2 = this.graphHeight - (this.graphHeight) * ((value - this.min) / (this.max - this.min));

      if(value==null){
        return [x,null, value];
      }
      return [x,y2, value]
    })
  }

  onClick(index, e) {
    this.displayValue = index
    console.log(e)
    console.log(this.data[index]);
  }

}
