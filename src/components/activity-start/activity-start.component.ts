import {Component, EventEmitter, Input, NgZone, Output} from '@angular/core';
import {APIService} from "../../chat/services/api";
import {TranslateService} from "@ngx-translate/core";
import {GlobalvarProvider} from "../../app/globalprovider";
import {finished} from "stream";
import {Http} from "@angular/http";
import {Storage} from "@ionic/storage";
import {ENVIRONMENT} from "../../environement.config";
import {WOTService} from "../../providers/WOTService";


@Component({
  selector: 'activity-start',
  templateUrl: 'activity-start.component.html',
})


export class ActivityStartComponent {
  @Input() activity;
  @Output() cancel = new EventEmitter();
  loaded = true;
  isCalendarEnabled = false;
  date = new Date();
  availableTime = [];
  selectedDateTime = null;
  searchDevice = false;
  finish = false;
  explanation = false;
  error = null;
  smallLoaded = false;

  time;
  interval;


  constructor(public api: APIService, private globalProvider: GlobalvarProvider, private zone: NgZone, public storage: Storage, public wot: WOTService) {
    wot.WotMessage.subscribe((c)=> {
      let WOTId = ENVIRONMENT.WOT_ID;

      this.zone.run(() => {
        console.log('1');

        if(c.action==='institute.humantech.nestore.action.WOT_ERROR'){
          this.error = c.extras['institute.humantech.nestore.extra.REASON'];
          this.searchDevice = false;
          this.finish = false;
        }
        else if(c.action==='institute.humantech.nestore.action.WOT_SUCCESS'){
          console.log(c);
          if(c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.ENABLE_STRUCTURE_ACTIVITY' || c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.ENABLE_TEST_30SCRT' || c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.ENABLE_TEST_6MWT'){
            console.log(c);
            if(this.activity.mode==='test'){
              this.api.putTestActivity(this.activity.activity.userTaskId, {status:'accepted'}).then();
            }
            this.activity.status = 'running';
            this.storage.set('currentPhysicalActivity', this.activity).then();
          }
          console.log(c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']);
          console.log(WOTId+'action.DISABLE_TEST_6MWT');
          if((c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.DISABLE_STRUCTURE_ACTIVITY' || c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.DISABLE_TEST_30SCRT'  || c.extras['institute.humantech.nestore.extra.REQUESTED_ACTION']===WOTId+'.action.DISABLE_TEST_6MWT')){
            console.log(c);
            this.clickCancel();
          }
          this.finish = false;
          this.searchDevice = false;
          this.explanation = true;
        }
        console.log(c);
      })
    });
  }

  ngOnChanges(change) {
    if(change.activity) {
      console.log(this.activity);
      if(this.activity.mode!=='test') {
        this.api.getTrainingProgram(this.activity['workout-id']).then(res => {
          this.activity.program = res;
          console.log(this.activity);

        });
      }
      if(this.activity.status && this.activity.status==='running' && this.activity.type==='unstructured'){
        this.startTimer();
      }
      if (!this.activity.status) {
        this.activity.status = 'sent';
      }
    }
  }

  clickOK() {

    if(this.activity.status==='sent' && this.activity.type!=='unstructured') {
      if ((<any>window).plugins) {
        this.searchDevice = true;// useful when we test without deploying
        if (!this.activity.mode) {
          let WOTId = ENVIRONMENT.WOT_ID;
          let extras = {};
          extras[WOTId+'.extra.FROM'] = 'institute.humantech.nestore';
          extras[WOTId+'.extra.STRUCTURE_ID'] =  this.activity['workout-id'];
          extras[WOTId+'.extra.COACHING_EVENT_ID'] = this.activity.id;
          (<any>window).plugins.intentShim.sendBroadcast({
              flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
              package: WOTId,
              action: WOTId+".action.ENABLE_STRUCTURE_ACTIVITY",
              extras: extras
            }, function (r) {
              console.log(r);
            },
            function () {

            });
        }
        else{
          let WOTId = ENVIRONMENT.WOT_ID;
          let extras = {};
          extras[WOTId+'.extra.FROM'] = 'institute.humantech.nestore';
          console.log({
            flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
            package: WOTId,
            action: WOTId+".action.ENABLE_TEST_"+this.activity.activity.activity,
            extras: extras
          });

          (<any>window).plugins.intentShim.sendBroadcast({
              flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
              package: WOTId,
              action: WOTId+".action.ENABLE_TEST_"+this.activity.activity.activity,
              extras: extras
            }, function (r) {
              console.log(r);
            },
            function () {

            });
        }
      }
      else{
        this.error='Bluetooth seems not working';
      }
    }
    else if(this.activity.status==='sent' && this.activity.type==='unstructured') {
      this.activity.status = 'running';
      this.activity.startDate = new Date();

      this.storage.set('currentPhysicalActivity', this.activity).then();

      this.startTimer();
    }
    else if(this.activity.status==='running' && this.activity.type==='unstructured') {
      this.globalProvider.startedActivity=null;
    }
    else{
      if(!this.activity.mode) {
        this.api.putActivity(this.globalProvider.profile.user_id, this.activity.id, {finished: true}).then(()=>
          this.globalProvider.reloadActivities()
        );
        this.storage.remove('currentPhysicalActivity');
        this.clickCancel();
      }
      if(this.activity.mode==='test'){
        this.globalProvider.lastTestDate = new Date();
        this.smallLoaded = true;
        this.api.putTestActivity(this.activity.activity.userTaskId, {status:'done'}).then(()=> {
          this.smallLoaded = false;
          this.cancel.next();
          this.storage.remove('currentPhysicalActivity');
          }
        ).catch(()=> this.smallLoaded=false);
      }

    }
  }

  startTimer(){
    this.time=this.convertHour((new Date().getTime() - this.activity.startDate.getTime())/1000);
    this.interval = setInterval(() => {
      this.time = this.convertHour((new Date().getTime() - this.activity.startDate.getTime())/1000);
    },1000);
  }

  clickStop(){
    if(this.activity.type==='unstructured'){
      this.smallLoaded = true;
      this.api.postUnstructuredActivity({start_time: this.activity.startDate.toISOString(), end_time:new Date().toISOString(), log_type:this.activity.data.tag, type:this.activity.data.name, intensity_target:this.activity.data.intensity, user_code:this.activity.data.user_code, timezone:Intl.DateTimeFormat().resolvedOptions().timeZone}).then(()=>{
        this.globalProvider.startedActivity = null;
        this.globalProvider.selectedDomain = null;
        this.smallLoaded = false;
        this.storage.remove('currentPhysicalActivity');
      }).catch(()=>{
        this.smallLoaded=false;
        }
      );

    }
    else {
      this.finish = true;
      if ((<any>window).plugins) { // useful when we test without deploying
        if (!this.activity.mode) {
          let WOTId = ENVIRONMENT.WOT_ID;
          let extras = {};
          extras[WOTId + '.extra.FROM'] = 'institute.humantech.nestore';
          (<any>window).plugins.intentShim.sendBroadcast({
              flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
              package: WOTId,
              action: WOTId + ".action.DISABLE_STRUCTURE_ACTIVITY",
              extras: extras
            }, function (r) {
              console.log(r);
            },
            function () {

            });
        } else {
          let WOTId = ENVIRONMENT.WOT_ID;
          let extras = {};
          extras[WOTId + '.extra.FROM'] = 'institute.humantech.nestore';
          (<any>window).plugins.intentShim.sendBroadcast({
              flags: [32], //https://github.com/d arryncampbell/darryncampbell-cordova-plugin-intent/issues/29
              package: WOTId,
              action: WOTId + ".action.DISABLE_TEST_" + this.activity.activity.activity,
              extras: extras
            }, function (r) {
              console.log(r);
            },
            function () {

            });
        }
      }
    }
  }

  clickCancel() {
    if(this.activity.type==='unstructured'){
      this.globalProvider.startedActivity = null;
      this.globalProvider.selectedDomain = null;
      this.cancel.next();
    }
    if(this.activity.type==='structured'){
      this.storage.remove('currentPhysicalActivity');
      this.cancel.next();
    }
    if(this.activity.mode==='test'){
      this.smallLoaded = true;
      this.api.putTestActivity(this.activity.activity.userTaskId, {status:'canceled'}).then(()=> {
        this.smallLoaded = false;
        this.storage.remove('currentPhysicalActivity');
          this.cancel.next();
        }
      ).catch(()=>{
        this.smallLoaded = false;
      });
    }
  }

  clickBack() {
    this.cancel.next();
  }

  convertHour(seconds: number): string {
    let hours =  Math.floor((seconds / 3600));
    return (('0'+Math.floor(seconds / 3600)).slice(-2)) + ':' + ('0'+Math.floor((seconds - hours*3600)/60)).slice(-2) + ':' + ('0'+(Math.floor(seconds % 60))).slice(-2);
  }

  close(){
    this.searchDevice=false;
  }

}
