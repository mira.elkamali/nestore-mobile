import {Component, EventEmitter, Input, Output} from '@angular/core';
import {APIService} from "../../chat/services/api";
import {Keyboard} from "ionic-angular";

@Component({
  selector: 'meal-picker',
  templateUrl: './meal-picker.component.html',
})
export class MealPickerComponent {
  @Output() valid = new EventEmitter();
  @Output() close = new EventEmitter();

  constructor(public keyboard: Keyboard) {
  }

  info = null;

  protected validMealName(info){
      if(!info.isInList) {
        this.info = info;
      }
      else{
        this.valid.emit({meal:info.meal});
      }
  }

  protected validMealGroup(info){
    this.valid.emit(info);
  }

  protected clickOutside(){
    this.close.emit();
  }

}
