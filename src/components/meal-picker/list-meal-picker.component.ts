import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {Keyboard} from 'ionic-angular';
import {APIService} from '../../chat/services/api';

@Component({
  selector: 'list-meal-picker',
  templateUrl: './list-meal-picker.component.html',
})
export class ListMealPickerComponent {
@ViewChild('input1') input1;
@Output() valid = new EventEmitter();
@Output() cancel = new EventEmitter();

  tags = [];
  list = [];
  found = [];
  text = "";
  selected = false;
  send = false;

  constructor(public apiService: APIService, public keyboard: Keyboard) {
    apiService.groupedFoodList().then(res=> {
      this.list=res.foods.name;
      this.found=this.list.slice(0,4);
    });

  }
  affectText(t){
    this.text=t;

    if(document.hasFocus())
    setTimeout(()=> {
      this.input1.nativeElement.blur();
      this.selected=true;
    },1)
  }

  validate(meal){
    console.log('validate1');
    this.valid.emit(meal);
    this.selected=false;
  }

  sort(){
    let filteredList = this.list.filter(res=>
    {
      let condition = true;
      for(let searchword of this.text.split(' ')) {
        let condition2 = false;
        for (let word of res.split(' ')) {
          if (word.toUpperCase().startsWith(searchword.toUpperCase())) {
            condition2 = true;
            break;
          }
        }
        condition = condition && condition2;
        if(!condition){
          return false;
        }
      }
      return true;});

    filteredList=filteredList.sort((a,b) =>{
      if(a.toUpperCase().startsWith(this.text.toUpperCase()) || !b.toUpperCase().startsWith(this.text.toUpperCase())){
        return -1;
      }
      return 0;
    });

    this.found=filteredList.slice(0,4);
  }

  isInList(value){
    return this.list.includes(value)
  }
}
