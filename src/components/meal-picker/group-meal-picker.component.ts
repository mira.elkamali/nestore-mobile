import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Keyboard} from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'group-meal-picker',
  templateUrl: './group-meal-picker.component.html',

})
//TODO: REMOVE ?
export class GroupMealPickerComponent {
@Output() valid = new EventEmitter();
@Output() cancel = new EventEmitter();
@Input() mealInfo;

foodList = [];

groups = [
    {click:false, group:'foodgroup_noodles'},
    {click:false, group:'foodgroup_soup'},
    {click:false, group:'foodgroup_vegetable'},
    {click:false, group:'foodgroup_egg'},
    {click:false, group:'foodgroup_diary'},
    {click:false, group:'foodgroup_rice'},
    {click:false, group:'foodgroup_dessert'},
    {click:false, group:'foodgroup_fried'},
    {click:false, group:'foodgroup_meat'},
    {click:false, group:'foodgroup_seafood'},
    {click:false, group:'foodgroup_bread'},
];

  constructor(public translate: TranslateService, public keyboard: Keyboard) {

  }

  validate(){
    this.valid.emit({meal:this.mealInfo.meal,groups:this.groups.filter((group)=>group.click).map((group)=>group.group)});
  }


}
