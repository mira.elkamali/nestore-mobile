import { NgModule } from '@angular/core';
import {MealPickerComponent} from './meal-picker.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MealPickerSearchComponent} from './meal-picker-search.component';
import {ListMealPickerComponent} from './list-meal-picker.component';
import {GroupMealPickerComponent} from './group-meal-picker.component';
import {TranslateModule} from '@ngx-translate/core';
import {APIService} from '../../chat/services/api';
import {MealPickerSearchCustomComponent} from '../meal-picker-search-custom/meal-picker-search-custom';
import {SpinnerComponent} from '../spinner/spinner.component';
import {Ng2ImgMaxModule} from 'ng2-img-max';
import {IonicModule} from "ionic-angular";

@NgModule({
  declarations: [
    MealPickerComponent,
    MealPickerSearchComponent,
    ListMealPickerComponent,
    GroupMealPickerComponent,
    MealPickerSearchCustomComponent,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    TranslateModule,
    Ng2ImgMaxModule,
    IonicModule
  ],
  providers: [APIService],
  exports:[
    MealPickerComponent,
    MealPickerSearchComponent,
    MealPickerSearchCustomComponent,
    ListMealPickerComponent,
    GroupMealPickerComponent,
    SpinnerComponent,
  ]
})
export class MealPickerModule { }
