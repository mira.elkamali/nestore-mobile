import {Component, Input} from '@angular/core';

@Component({
  selector: 'nestore-grid-circles',
  templateUrl: 'nestore-grid-circles.html'
})
export class NestoreGridCirclesComponent {
  @Input() data ;
  @Input() showValues = true ;
  @Input() mode = 'week';

  dayNameArray = [];

  constructor() {

    let baseDate = new Date(Date.UTC(2017,0, 1)); // just a Monday
    this.dayNameArray = [];
    for(let i = 0; i < 7; i++)
    {
      this.dayNameArray.push(baseDate);
      baseDate = new Date(baseDate.setDate(baseDate.getDate() + 1));
    }
  }

}
