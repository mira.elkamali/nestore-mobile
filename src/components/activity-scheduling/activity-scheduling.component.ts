import {Component, EventEmitter, Input, Output} from '@angular/core';
import {APIService} from "../../chat/services/api";
import {TranslateService} from "@ngx-translate/core";


@Component({
  selector: 'activity-scheduling',
  templateUrl: 'activity-scheduling.component.html',
})


export class ActivitySchedulingComponent {
  @Input() activity;
  @Output() cancel = new EventEmitter();
  @Output() scheduled = new EventEmitter();

  loaded = true;
  loadedHours = true;
  isCalendarEnabled = false;
  date = new Date();
  availableTime;
  availableHours;
  unavailableTime;
  selectedDateTime = null;
  dateArray;
  selected;


  constructor(public api: APIService) {

    let date = new Date();
    date.setDate(date.getDate() - 1);
    this.dateArray = [];
    for(let i = 0; i < 7; i++)
    {
      this.dateArray.push(date);
      date = new Date(date.setDate(date.getDate() + 1));
    }
  }

  ngOnChanges(change) {
    if (change.activity.currentValue) {
      console.log(this.activity)
    }
  }

  clickOK() {
    console.log(this.activity);
    this.loaded = false;
    this.api.postActivitySchedule(this.activity.id,this.selectedDateTime, this.activity.eventId).then(
      ()=>{
        this.loaded=true;
        this.scheduled.next()});
  }

  clickCancel() {
    this.cancel.next();
  }

  isToday(td) {
    let d = new Date();
    return td.getDate() === d.getDate() && td.getMonth() === d.getMonth() && td.getFullYear() === d.getFullYear();
  }

  changeDate(){
    console.log('changeDate');
    if(!this.unavailableTime) {
      this.loadedHours = false;
      this.api.getUnavailableTimeSlots(this.activity.id).then((res) => {
          let times = [];
          for (let a of res) {
            if(!a.endsWith('Z')){ // todo: remove once Silvia the date correctly
              a+='Z';
            }
            times.push(+new Date(a));
          }
          this.unavailableTime = times;
          this.s();
          this.loadedHours = true;
        }
      );
    }
    else{
      this.s()
    }
  }

  s(){
    this.availableHours = [];
    for (let i of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]) {
      let d = new Date(this.selected);
      d.setHours(i);
      d.setMinutes(0);
      d.setSeconds(0);
      d.setMilliseconds(0);
      this.availableHours.push(d);
    }

    console.log(this.unavailableTime);

    this.availableHours = this.availableHours.filter((time) => {
      return !this.unavailableTime.includes(+new Date(time)) && +new Date() < +new Date(time);
    });

  }

  select(d){
    this.availableHours = null;
    this.selectedDateTime = null;
    this.selected = d;
    this.changeDate();
  }

  quit(){
    this.cancel.emit();
  }
}
