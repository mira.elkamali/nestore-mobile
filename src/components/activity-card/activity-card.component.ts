import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import { GlobalvarProvider } from '../../app/globalprovider';
import {LoginPage} from '../../pages/login/login';
import {IamClientProvider} from '../../providers/iam-client/iam-client';
import {App} from 'ionic-angular';
import {TranslatePipe, TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'nestore-activity-card',
  templateUrl: 'activity-card.component.html',
})
export class ActivityCardComponent {
  @Input() image;
  @Input() domain = 'physical';
  @Input() data;
  @Output() dataChange = new EventEmitter();

  @Input() hasValidation = true;
  @Output() validate = new EventEmitter();
  @Output() schedule = new EventEmitter();
  @Output() start = new EventEmitter();
  @Output() showDetails = new EventEmitter();

  structuredText = [];


  timeToWait;


  translatePipe = new TranslatePipe(this.translate, this.cdRef);

  constructor(private cdRef:ChangeDetectorRef, private translate: TranslateService, private globalprovider : GlobalvarProvider) {
    this.translatePipe = new TranslatePipe(this.translate, this.cdRef);

    setInterval(() => {
      this.timeToWait =  (new Date(this.data['scheduled-time']).getTime() - new Date().getTime())/1000;
    },1000);
  }


  ngOnChanges(changes){
    console.log(this.data);

    if(changes.data){
      if(this.data.ce_id) {
        let text = this.translatePipe.transform(this.data.ce_id);
        let links = text.match(/\bhttps?:\/\/\S+/gi);

        let endMessage = text;
        if (links) {
          for (let link of links) {
            let split = endMessage.split(link);
            this.structuredText.push({type: 'text', data: split[0]});
            this.structuredText.push({type: 'link', data: link});
            endMessage = split[1];
            console.log(this.structuredText);

            //this.message.text = this.message.text.replace(link,'<a href='+link+'>'+link+'</a>');
          }
        }
      }
    }
  }

  valid(){
    if(this.data.review && this.data.review.done){
      //nothing
    }
    else if(this.data.scheduled==true && this.data.title==='ce_cog_nut' && this.data.task){
      this.startActivity()
    }
    else if(this.data['scheduled-time']){
      this.validate.emit();
      this.data.loading = true;
    }
    else if(this.data && !this.data.planned && !(this.data.review && this.data.review.done)){
      this.schedule.emit();
    }
    else if(this.data.task) {
      this.validate.emit(this.data.task);
    }
    else {
      this.validate.emit();
    }
  }

  scheduleActivity(){
   this.schedule.emit();
  }

  startActivity(){
    console.log(this,this.data.task);
    this.start.emit({game:'NUT', task:this.data.task.id});
  }

  details(){
    console.log(this.data);
    this.showDetails.emit();
  }

  openLink(link){
    console.log('link');
    window.open(link,'_system', 'location=yes');
  }
}
