import { Component, Input, Renderer2, ElementRef } from '@angular/core';
import { Threshold } from '../../models/threshold';


@Component({
  selector: 'nestore-double-pie-chart',
  templateUrl: 'nestore-double-pie-chart.html'
})
export class NestoreDoublePieChartComponent {

  @Input() value = 0;
  @Input() color = '#FF6375';
  @Input() text = false;

  @Input() value2 = 30;
  @Input() color2 = '#FFDBDF';
  @Input() text2 = false;

  @Input() offset = 0;
  @Input() offset2 = 0;

  constructor() {
  }

}
