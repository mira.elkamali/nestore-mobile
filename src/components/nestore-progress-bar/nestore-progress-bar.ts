import { Component, Input, Renderer2, ElementRef } from '@angular/core';
import { Threshold } from '../../models/threshold';


@Component({
  selector: 'nestore-progress-bar',
  templateUrl: 'nestore-progress-bar.html'
})
export class NestoreProgressBarComponent {

  @Input() threshold: Threshold;
  styles: Object;
  imgSrc;

  constructor(private renderer: Renderer2, private el: ElementRef) {
  }

  ngAfterViewInit() {
    this.drawThresholds();
    this.moveSlider();
    this.moveLabels();
  }

  drawThresholds() {
    let seg1 = this.el.nativeElement.querySelector("#seg1");
    let seg2 = this.el.nativeElement.querySelector("#seg2");
    let seg3 = this.el.nativeElement.querySelector("#seg3");

    this.renderer.setStyle(seg1, "width", this.percString("lower"));
    this.renderer.setStyle(seg2, "left", this.percString("lower"));
    if(this.threshold.hasUpper) {
      this.renderer.setStyle(seg2, "width", this.percString("goodWidth"));
      this.renderer.setStyle(seg3, "left", this.percString("upper"));
      this.renderer.setStyle(seg3, "width", "20%");
    } else {
      this.renderer.setStyle(seg2, "width", String(100 - this.threshold.percentages["lower"]) + "%");
      this.renderer.setStyle(seg2, "border-radius", "0 12px 12px 0");
      this.renderer.setStyle(seg3, "display", "none");
    }
  }

  moveLabels() {
    let lower = this.el.nativeElement.querySelector("#lower");
    let upper = this.el.nativeElement.querySelector("#upper");

    this.renderer.setStyle(lower, "left", `calc(${this.percString("lower")} - ${lower.clientWidth}px / 2)`);
    if(this.threshold.hasUpper) {
      this.renderer.setStyle(upper, "left", `calc(${this.percString("upper")} - ${upper.clientWidth}px / 2)`);
    } else {
      this.renderer.setStyle(upper, "display", "none");
    }

  }

  moveSlider() {
    let element = this.el.nativeElement.querySelector("#slider");
    // subtract half of slider width from the position -> center it
    this.renderer.setStyle(element, "left", `calc(${this.percString("value")} - 15px)`);
  }

  getImage() {
    if(this.threshold.inRange) {
      return "../../assets/icon/Notification_bar_green.svg";
    } else {
      return "../../assets/icon/Notification_bar_red.svg";
    }
  }

  private percString(name) {
    return String(this.threshold.percentages[name]) + "%";

  }
}
