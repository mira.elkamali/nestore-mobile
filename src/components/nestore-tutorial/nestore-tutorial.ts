import {Component} from "@angular/core";
import {GlobalvarProvider} from "../../app/globalprovider";

@Component({
  selector: 'nestore-tutorial',
  templateUrl: 'nestore-tutorial.html'
})
export class NestoreTutorialComponent {
  constructor(public globalvarProvider: GlobalvarProvider){

  }

  clickTutorialText(){
    this.globalvarProvider.nextTip();
  }
}
