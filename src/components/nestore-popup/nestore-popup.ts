import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import * as d3 from 'd3';


@Component({
  selector: 'nestore-popup',
  templateUrl: 'nestore-popup.html'
})
export class NestorePopupComponent {

  @Output() valid = new EventEmitter();
  @Output() unvalid = new EventEmitter();

  @Input() text;
  @Input() validText;
  @Input() unvalidText;

  validation(b){
    b?this.valid.emit():this.unvalid.emit();
  }
}
