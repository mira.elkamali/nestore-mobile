import {Component, EventEmitter, Input, Output} from '@angular/core';


@Component({
  selector: 'chat-questionnaire',
  templateUrl: './chat-questionnaire.component.html',
})

export class ChatQuestionnaireComponent{
  @Input() message = null;
  @Output() sendAnswer = new EventEmitter();

  constructor() {

  }

  send(e){
    this.sendAnswer.emit(e);
  }

}
