import {ChangeDetectorRef, Component, Input, ViewChild} from '@angular/core';
import * as d3 from 'd3';
import {formatDate} from "@angular/common";
import {DatePipeProxy} from "../../pipes/DatePipeProxy";
import {TranslatePipe, TranslateService} from "@ngx-translate/core";
import {CoordinatesChartOld} from "../coordinatesChart";


@Component({
  selector: 'nestore-bar-chart-old',
  templateUrl: 'nestore-bar-chart.html'
})
export class NestoreBarChartOldComponent extends CoordinatesChartOld {
  @Input() thresold = 3;
  @Input() label;
  @Input() data = [70,65,60,55,4,0];
  @Input() mode ;
  @Input() rounded = false;

  line1;
  line2;
  line3;
  line;
  labelPosition = 0;

  constructor(cdRef:ChangeDetectorRef, private translate: TranslateService) {
    super(cdRef);
    let baseDate = new Date(Date.UTC(2017,0, 1)); // just a Monday
    this.dayNameArray = [];
    for(let i = 0; i < 7; i++)
    {
      this.dayNameArray.push(baseDate);
      baseDate = new Date(baseDate.setDate(baseDate.getDate() + 1));
    }
  }

  initChart(){
    if(this.g){
      this.g.remove();
    }
    if(this.line){
      this.line.remove();
    }
    if(this.line2){
      this.line2.remove();
    }
    if(this.line3){
      this.line3.remove();
    }
    if(this.line1){
      this.line1.remove();
    }
    super.initChart();

    let width = this.svgChart.nativeElement.clientWidth;
    let height = width / 2;

    this.labelPosition = height*(3/5);

    if(this.thresold){
      this.labelPosition = height-(height)*(this.thresold/this.max);

    }

    let svgData = this.calculateSVGData( this.data.map((v)=>{
      if(this.rounded && v!==null){
        return {value: v[0], offset: v[1]}
      }
      return {value: v}}), width-50, height );


    this.line3 = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "path"
    );

    if(this.thresold) {
      this.line3.setAttribute("d", "M30," + this.labelPosition + " L" + (width - 30) + ", " + this.labelPosition);
      this.line3.setAttribute("fill", "none");
      this.line3.setAttribute("stroke", "black");
      this.line3.setAttribute("stroke-width", "2");
      this.svgChart.nativeElement.prepend(this.line3);


      this.line1 = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "path"
      );
      this.line1.setAttribute("d", "M0," + this.labelPosition + " L" + width + ", " + this.labelPosition);
      this.line1.setAttribute("fill", "none");
      this.line1.setAttribute("stroke", "#F0F0F0");
      this.line1.setAttribute("stroke-width", "2");
      this.svgChart.nativeElement.prepend(this.line1);
    }




    this.svgChart.nativeElement.appendChild(this.g);

    let translateDate;
    if (this.mode === 'week') {
      translateDate = new DatePipeProxy(this.translate);
    }
    else{
      translateDate = new TranslatePipe(this.translate, this.cdRef)
    }

    svgData.map(( coordinates, index )=>{
      let g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");

      let w = width/20;
      if(this.rounded) {
        g2.setAttribute('clip-path', 'inset(0 0 0 0 round 80)');
        g2.setAttribute('transform', 'translate(0,'+coordinates[2]+')');
        w = width/30;

      }

      let point = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "rect"
      );
      point.setAttribute("x",""+(-(w)/2+coordinates[0]));
      point.setAttribute("y",""+((height)-(height-coordinates[1])));
      point.setAttribute("width", ""+w);
      point.setAttribute("height", ""+(height-coordinates[1]));
      point.setAttribute("fill", this.color);
      //point.setAttribute("stroke", "#fff");
      //point.setAttribute("stroke-width", "2");
      g2.appendChild(point);
      this.g.appendChild(g2);

      let text = document.createElementNS(
        "http://www.w3.org/2000/svg",
        "text"
      );
      text.setAttribute("x",coordinates[0]);
      text.setAttribute("y",height+20+ " ");
      text.setAttribute("text-anchor",'middle');
      text.setAttribute('font-family',"Lato-Black, Lato");
      text.setAttribute('font-weight',"800");
      text.setAttribute('fill', '#A8AAAC');
      if(this.mode==='week') {
        text.innerHTML = translateDate.transform(this.dayNameArray[index], 'EEEEE');
      }
      else{
        text.innerHTML = translateDate.transform('week_abbreviation') + (index+1);
      }
      this.g.appendChild(text)
    })
  }

  getValueArray( graphData ) {

  }

  calculateSVGData( graphData, width, height ) {
    return this.getCoordinates( graphData, width, height )
  }


  getCoordinates( graphData, width, height ) {
    let v =  graphData.map(( item ) => {
        return item.value;
    });
    //let min = Math.min.apply( null, values )-1;
    let min = 0;
    let max = this.max ? this.max : Math.max.apply( null, v );
    if(max==0){
      max=3;
    }

    let min2 = 0; //point size
    let max2 = height - 0; //point size

    let yRatio = height / ( max - min );
    let xRatio = width / ( v.length );

    return graphData.map( ( value, i ) => {
      let v = value.value;
      let off = 0;
      if(this.rounded && value.value!==null){
        v = value.value;
        off=value.offset;
      }
      let y = height - (yRatio * ( v - min )) ;
      let x = ( xRatio * i ) + ( xRatio / 2 );

      // rescale in order than point stay in view
      let y2 = min2 + (y/height) * (max2-min2);

      let offset = - (yRatio * ( off - min )) ;
      let offset2 = min2 + (offset/height) * (max2-min2);

      return [x,y2,this.rounded && off?offset2:0]
    })
  }

  getLineSVG( data ) {
    let lineGenerator = d3.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .curve(d3.curveCardinal);

    return lineGenerator(data)
  }

  getSimpleLineSVG( data ) {
    let lineGenerator = d3.line()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]; })
      .curve(d3.curveCardinal);

    return lineGenerator(data)
  }


}
