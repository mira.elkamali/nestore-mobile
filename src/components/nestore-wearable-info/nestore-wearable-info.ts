import {Component, EventEmitter, Output} from "@angular/core";

@Component({
  selector: 'nestore-wearable-info',
  templateUrl: 'nestore-wearable-info.html'
})
export class NestoreWearableInfoComponent {
  @Output() close = new EventEmitter();
  selectPage = 0;
  pages = 3;
  page = 0;

  quit(){
    this.close.next();
  }
}
