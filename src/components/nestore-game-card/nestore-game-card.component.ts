import {Component, EventEmitter, Input, Output} from '@angular/core';
import { GlobalvarProvider } from '../../app/globalprovider';
import {LoginPage} from '../../pages/login/login';
import {IamClientProvider} from '../../providers/iam-client/iam-client';
import {App} from 'ionic-angular';

@Component({
  selector: 'nestore-game-card',
  templateUrl: 'nestore-game-card.component.html',
})
export class NestoreGameCardComponent {
  @Input() disabled = false;
  @Input() value = null;
  @Input() showBack = false;
  @Input() canSelect = false;
  @Input() legend;
  @Input() success = null;
  @Input() legendPosition = 'bottom';

  @Input() color;
  @Output() click  = new EventEmitter();

  onClick(){
    if(this.canSelect) {
      this.click.emit();
    }
  }
}
