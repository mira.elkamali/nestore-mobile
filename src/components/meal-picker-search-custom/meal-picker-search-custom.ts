import {Component, EventEmitter, Input, Output, ViewChild, ChangeDetectorRef, NgZone} from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { TranslateService } from '@ngx-translate/core';
import { ApiProvider } from '../../providers/api/api';
import { Camera } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {APIService} from "../../chat/services/api";


@Component({
  selector: 'meal-picker-search-custom',
  templateUrl: 'meal-picker-search-custom.html'
})
export class MealPickerSearchCustomComponent {
  @ViewChild('input1') input1;

  @Input() placeholder;
  @Input() types;

  @Input() photoOption = true;

  @Input() loaded;
  @Output() loadedChange = new EventEmitter();
  @Output() isSearching = new EventEmitter();
  @Output() resultsChanges = new EventEmitter();
  @Output() valid = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() photo = new EventEmitter();
  @Output() suggestions = new EventEmitter();
  @Output() startUploadPhoto = new EventEmitter();
  @Input() selectedType = 'food';

  list = [];
  found = [];
  text = '';
  meal = '';
  selected = false;
  hasKeyboard = false;
  image = null;
  hasPhoto = false;
  suggestionsList = [];
  type;
  globalList;

  loading = true;

  constructor(public api: APIService,
    public keyboard: Keyboard,
    public translate: TranslateService,
    public camera: Camera,
    public ref: ChangeDetectorRef,
    private nativeStorage: NativeStorage,
    private ng2ImgMax: Ng2ImgMaxService) {

    console.log('test');

    keyboard.onKeyboardShow().subscribe(() => { this.hasKeyboard = true; this.isSearching.emit(this.hasKeyboard) });
    keyboard.onKeyboardHide().subscribe(() => { this.hasKeyboard = false; this.isSearching.emit(this.hasKeyboard) });

    api.groupedFoodList().then(res => {
      this.globalList = res;
      this.uploadSearch();
      this.loading = false;
      this.loaded = true;
      this.loadedChange.emit(true);
    });
  }

  ngOnChanges(changes){
    if(changes.types && this.types){
      this.type = this.types;
      this.selectedType='combo';
    }
  }

  uploadSearch(){
    this.list = this.globalList[this.selectedType];
    this.found = this.list.slice(0, 4);
    this.sort();

  }

  affectText(t) {
    console.log(t);
    this.text = t.text;
    this.meal = t;

    if (document.hasFocus())
      setTimeout(() => {
        this.input1.nativeElement.blur();
        this.selected = true;
        this.validate();
      }, 1)
  }

  validate() {
    this.valid.emit({ meal: this.meal, type:this.selectedType});
    this.selected = false;
  }

  sort() {
    console.log("calling sort()");
    let filteredList = this.list.filter(res => {
      let condition = true;
      for (let searchword of this.text.split(' ')) {
        let condition2 = false;
        for (let word of res.text.split(' ')) {
          if (word.toUpperCase().startsWith(searchword.toUpperCase())) {
            condition2 = true;
            break;
          }
        }
        condition = condition && condition2;
        if (!condition) {
          return false;
        }
      }
      return true;
    });

    filteredList = filteredList.sort((a, b) => {
      if (a.text.toUpperCase().startsWith(this.text.toUpperCase()) || !b.text.toUpperCase().startsWith(this.text.toUpperCase())) {
        return -1;
      }
      return 0;
    });

    this.found = filteredList.slice(0, 4);
    this.resultsChanges.emit({ text: this.text, items: this.found });
  }


  isInList(value) {
    return this.list.includes(value)
  }

  readFile(inputValue: any): void {
    const file: File = inputValue.files[0];
   // const myReader: FileReader = new FileReader();
    this.ng2ImgMax.resizeImage(file, 10000, 500).subscribe(
      result => {
        //let uploadedImage = new File([result], result.name);
        //this.uploadAndStorePhoto(uploadedImage);
        const myReader: FileReader = new FileReader();
        myReader.onload = (e) => {
          console.log(e);
          this.image = myReader.result;
          this.hasPhoto = true;
          this.image = this.image.replace('data:application/octet-stream;base64', 'data:'+ result.type +';base64');

          this.uploadAndStorePhoto(this.image);
        };
        myReader.readAsDataURL(result);
      },
      error => {
        console.log('😢 Oh no!', error);
      }
    );
    /*myReader.onload = (e) => {
        console.log(e);
        this.image = myReader.result;
        this.hasPhoto = true;
        this.uploadAndStorePhoto(this.image);
    };*/
    //myReader.readAsDataURL(file);
  }

  takePicture() {
    console.log("taking picture");
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 700,
      targetHeight: 700
    }).then(
      (imageData) => {
        this.startUploadPhoto.emit();
        this.uploadAndStorePhoto("data:image/jpeg;base64," + imageData)
      }, (err) => {
        console.log(err);
      });
  }

  openPicture(e) {
    this.startUploadPhoto.emit();
    console.log(e.target.lastModifiedDate);
    this.readFile(e.target);

  }

  uploadAndStorePhoto(photo) {
    console.log("uploading photo");

    this.api.uploadPhoto(photo).then(
      (success: any) => {
        console.log(success);
        let photoId = success.id;
        this.type = success.type;

        this.suggestionsList = this.catchSuggestions(success);
        this.suggestions.emit(this.suggestionsList);
        this.text = this.suggestionsList[0];
        this.found = this.suggestionsList.slice(1, 5);
        this.hasPhoto = true;
        this.image = photo;
        // store photo with id
        this.nativeStorage.setItem(success.id, { photo: photo })
          .then(
            () => {
              this.photo.emit({ photo: photo, photoId: photoId, type:this.type });
              console.log("stored photo!");
            },
            (error) => console.log("Error storing photo", error)
          );
      },
      (error) => {
        this.photo.emit(null)
      }
    )
  }

  catchSuggestions(success) {
    var suggestions = [];

    // always the first element of drink/food is the dish
    // second is the probability
    switch (success.type) {
      case "drinks": {
        if(success.drink[0]) {
          suggestions.push(success.drink[0]);
        }
        break;
      }
      case "food": {
        if(success.dish[0]) {
          suggestions.push(success.dish[0][0]);
        }
        break;
      }
    }

    // TODO: this is just a best guess, not sure if it always works
    // see if there are alternative suggesions and grab the names
    let alternativeKey = Object.getOwnPropertyNames(success).filter(i => i.includes("alternative"));
    if (alternativeKey.length > 0) {
      success[alternativeKey[0]].forEach(element => {
        suggestions.push(element[0])
      });
    }

    return suggestions;
  }
}
