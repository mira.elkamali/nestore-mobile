import {ChangeDetectorRef, HostListener, Input, ViewChild} from "@angular/core";

export abstract class ChartOld {
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.initChart();
  }

  @ViewChild('svg') svgChart;
  @Input() data ;
  @Input() color = '#76C1DB';

  constructor(protected cdRef:ChangeDetectorRef) {
    this.cdRef=cdRef
  }

  ngOnInit(){
    this.initChart();
  }

  public abstract initChart();

  ngAfterViewInit(){
    this.ngOnInit();
    this.cdRef.detectChanges();
  }

}
