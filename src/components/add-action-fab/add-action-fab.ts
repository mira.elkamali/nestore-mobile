import {Component, EventEmitter, Output} from '@angular/core';
import { ModalController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { NutritionPage } from '../../pages/nutrition/nutrition';
import {TranslateService} from '@ngx-translate/core';

/**
 * Generated class for the AddActionFabComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'add-action-fab',
  template: `
    <ion-fab  bottom right>
      <button (click)="fabToggled(fab)" tappable ion-fab><ion-icon name="add"></ion-icon></button>
      <ion-fab-list #fab side="top" style="right: 0; align-items: flex-end">
        <div (click)="openPage('social')" style="display: inline-flex">
          <div class="domainName">{{'domain-social'|translate}}</div>
          <button ion-fab class="social"><img src="../assets/icon/Social_small.svg" /></button>
        </div>
        <div (click)="openPage('physical')"  style="display: inline-flex">
          <div class="domainName">{{'domain-physical'|translate}}</div>
          <button ion-fab class="physical"><img src="../assets/icon/Physical_small.svg" /></button>
        </div>
        <div  (click)="openPage('nutrition')" style="display: inline-flex">
          <div class="domainName">{{'domain-nutrition'|translate}}</div>
          <button ion-fab class="nutrition"><img src="../assets/icon/Nutrition_small.svg" /></button>
        </div>
        <div  (click)="openPage('cognitive')" style="display: inline-flex">
          <div class="domainName">{{'domain-cognitive'|translate}}</div>       
          <button ion-fab class="cognitive"><img src="../assets/icon/Cognitive_small.svg" /></button>
        </div>
      </ion-fab-list>
    </ion-fab>
  `,
  styles: [`
    :host { right: 15px; bottom: 15px; }
    :host ion-fab { position: static; z-index: 3 }
    .domainName{
      font-size: 22px;
      font-weight: 700;
      display: flex;
      align-items: center;
      margin-right: 20px;
    }
  `]
})
export class AddActionFabComponent {
  @Output() onStateChange = new EventEmitter();
  @Output() onDomainSelected = new EventEmitter();

  text: string;
  private isOpened: boolean = false;

  public fabToggled(fab): void {
    console.log(fab)
    this.isOpened = !this.isOpened;

    if(this.isOpened) {
      this.onStateChange.emit(true);
    } else {
      this.onStateChange.emit(false);
    }
  }

  constructor(public translate: TranslateService) {
    console.log('Hello AddActionFabComponent Component');
  }

  openPage(domain) {
    this.onDomainSelected.emit(domain);
    console.log(domain);
  }

}
