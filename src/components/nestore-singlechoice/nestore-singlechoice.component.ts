import {Component, EventEmitter, Input, Output} from '@angular/core';
import {$$} from "@angular/compiler/src/chars";
import {GlobalvarProvider} from "../../app/globalprovider";
import {Subject} from "rxjs";

@Component({
  selector: 'nestore-singlechoice',
  templateUrl: 'nestore-singlechoice.component.html',
})
export class NestoreSingleChoiceComponent {
  @Input() choices = [];
  @Input() canEdit = true;
  @Output() validated = new EventEmitter();
  @Input() validation = false;
  @Input() selected;
  @Output() selectedChange = new EventEmitter();
  optionSelected;

  selectedIntent;
  choiceClick = new Subject();

  constructor(public globalProvider: GlobalvarProvider){
    this.choiceClick.subscribe(()=>{
      this.change(this.choices[0]);
    })
  }

  ngOnChanges(changes){
    console.log(changes);
    if(changes.selected && this.selected) {
      this.selectedIntent = this.selected ? this.selected.intent : null;
    }
  }

  validate() {
    this.validated.emit({text:this.optionSelected.text, intent: this.optionSelected.intent});
  }

  change(e){
    this.selected=e;
    this.optionSelected = e;
    this.selectedChange.emit(e);
    this.selectedIntent = this.selected ? this.selected.text : null;
  }


}
