export class MemoryFailure {
  constructor(public dayOfWeek: number,
              public value: number) {}
}
