import {LocalCircleMember} from "./local-circle-member";

export class LocalCircleInteractions {
  constructor(
    public id: string,
    public startDate: Date,
    public endDate: Date,
    public totalInteractionsNumber: number,
    public totalInteractionsDuration: number,
    public localCircleMembers: LocalCircleMember[],
    public localCircleInteractionsNumber: number,
    public localCircleInteractionsDuration: number,
    public periods: LocalCircleInteractions[]
  ) {}
}
