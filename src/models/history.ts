import { NutritionEvent } from "./nutrition-event";

export class History {
    constructor(
        public nutrition: Array<NutritionEvent>
    ) { }
}